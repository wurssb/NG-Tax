### NG-Tax 2.0 SPARQL queries

A list of commonly used SPARQL queries

## Simple queries



### Samples

To obtain all samples from an endpoint analysed by NG-Tax 2.0

```bash
PREFIX gbol: <http://gbol.life/0.1/>
SELECT *
WHERE { 
    ?sample a gbol:Sample .
} LIMIT 10
```

### ASVs

Number of ASVs per sample

```bash
PREFIX gbol: <http://gbol.life/0.1/>
SELECT ?sample (COUNT(DISTINCT(?asv)) AS ?asvCount)
WHERE { 
    ?sample a gbol:Sample .
    ?sample gbol:asv ?asv .
} GROUP BY ?sample
LIMIT 10
```



### ASV sequences

To obtain the ASV sequences for each sample

```
PREFIX gbol: <http://gbol.life/0.1/>
SELECT DISTINCT ?sample ?fwdseq ?revseq
WHERE { 
    ?sample a gbol:Sample .
    ?sample gbol:asv ?asv .
    ?asv gbol:forwardASV ?fwd .
    ?fwd gbol:sequence ?fwdseq .
    ?asv gbol:reverseASV ?rev . 
    ?rev gbol:sequence ?revseq .
} LIMIT 10
```



### ASV sequences shared among samples

The number of samples an ASV is found in

```bash
PREFIX gbol: <http://gbol.life/0.1/>
SELECT DISTINCT ?fwdseq ?revseq (COUNT(DISTINCT(?sample)) AS ?samples)
WHERE { 
    ?sample a gbol:Sample .
    ?sample gbol:asv ?asv .
    ?asv gbol:forwardASV ?fwd .
    ?fwd gbol:sequence ?fwdseq .
    ?asv gbol:reverseASV ?rev . 
    ?rev gbol:sequence ?revseq .
} GROUP BY ?fwdseq ?revseq
```

### ASVs of different types

In the ontology it is stored wether an ASV is rejected or not. You can then count for the number of types an ASV has as if this is larger than 1 it means that the ASV is two of the following: a true, chimera or rejected ASV

```
PREFIX gbol: <http://gbol.life/0.1/>
SELECT DISTINCT ?fwdseq ?revseq (COUNT(DISTINCT(?asvType)) AS ?types)
WHERE { 
    ?sample a gbol:Sample .
    ?sample gbol:asv ?asv .
    ?asv gbol:forwardASV ?fwd .
    ?asv a ?asvType .
    ?fwd gbol:sequence ?fwdseq .
    ?asv gbol:reverseASV ?rev . 
    ?rev gbol:sequence ?revseq .
} GROUP BY ?fwdseq ?revseq
```



### NGTax settings

The settings used for NGTax can be found through the provenance of each library.

```bash
PREFIX gbol: <http://gbol.life/0.1/>
SELECT DISTINCT ?library ?predicate ?object
WHERE { 
    ?library gbol:provenance ?prov .
    ?prov gbol:annotation ?annot  .
    ?annot ?predicate ?object .
}
```





## Advanced queries



### Compare ASV with different database

This query compares the taxonomic assignment to different databases from an ASV.

```bash
PREFIX gbol: <http://gbol.life/0.1/>
select ?fseq ?rseq ?taxName7 ?dbName ?mismatchDB
where { 
?library gbol:provenance ?prov .
    ?prov gbol:annotation ?annot .
    ?annot gbol:refdb ?db .
    bind (replace (?db , "../database/", "") AS ?dbName)
    ?annot gbol:mismatchDB ?mismatchDB .
    ?library gbol:sample ?sample .
    ?sample gbol:asv ?asv .
    ?asv gbol:forwardASV ?fasv .
    ?fasv gbol:sequence ?fseq .
    ?asv gbol:reverseASV ?rasv .
    ?rasv gbol:sequence ?rseq .
    ?asv gbol:assignedTaxon ?tax .
    ?tax gbol:taxonName ?taxName .
    bind (replace (?taxName , "d__", "") AS ?taxName2)
    bind (replace (?taxName2 , "p__", "") AS ?taxName3)
    bind (replace (?taxName3 , "c__", "") AS ?taxName4)
    bind (replace (?taxName4 , "o__", "") AS ?taxName5)
    bind (replace (?taxName5 , "f__", "") AS ?taxName6)
    bind (replace (?taxName6 , "g__", "") AS ?taxName7)
} ORDER BY ?fseq ?rseq
```

### ASV overview

For each sample show the total abundance per taxonomic lineage identified and the number of ASVs used for this particular assignment

```bash
PREFIX ssb: <http://ssb.wur.nl/NG-Tax/0.1/>
PREFIX gbol:  <http://gbol.life/0.1/>

select ?sample (SUM(?clusteredReadCount) AS ?totalCount) (COUNT(?asv) AS ?asvs) ?taxonName
where { 
    ?lib a gbol:Library .
    ?lib gbol:sample ?sample .
    ?sample gbol:name ?name .
    ?sample gbol:asv ?asv .
    ?asv gbol:assignedTaxon ?assignedTaxon .
    ?asv gbol:clusteredReadCount ?clusteredReadCount .
    ?assignedTaxon gbol:taxonName ?taxonName .
} GROUP BY ?sample ?taxonName
ORDER BY ?sample DESC(?totalCount)
```

### Unique taxonomic assignments

This query can be used to identify unique taxonomic groups in each sample

```
PREFIX gbol: <http://gbol.life/0.1/>
SELECT DISTINCT ?sample ?taxonName
WHERE {
        ?sample a gbol:Sample .
        ?sample gbol:asv ?asv .
        ?asv gbol:assignedTaxon ?assignedTaxon .
        ?assignedTaxon gbol:taxonName ?taxonName .
    {
    SELECT DISTINCT ?taxonName (COUNT(DISTINCT(?sample)) AS ?sampleC)
    WHERE {
        ?sample a gbol:Sample .
        ?sample gbol:asv ?asv .
        ?asv gbol:assignedTaxon ?assignedTaxon .
        ?assignedTaxon gbol:taxonName ?taxonName .
    } group by ?taxonName
    HAVING(?sampleC = 1)
    }
}
```

