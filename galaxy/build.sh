# Pulling latest known image, saves time in the long run
docker pull wurssb/ngtax

# Building the docker image
docker build --build-arg CACHEBUST=$(date +%s) -t wurssb/ngtax .

# Pushing to docker
docker push wurssb/ngtax

