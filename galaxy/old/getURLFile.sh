#!/bin/bash

# First argument is link to file on the NASS
# Second argument is output file

#Get path of script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
tmpfile=$(mktemp)

echo "Getting file"
wget $1 &> $tmpfile
echo "File saved"

if grep -q 'ERROR 404' $tmpfile; then
  echo "File not found on NAS server, link incorrect" 1>&2
  exit
fi

oldname=$(head $tmpfile | grep "^Saving to:" | cut -d' ' -f 3 | sed 's/‘//' |  sed 's/’//' | sed 's/`//' | sed "s/'//")
echo "File name is: " $oldname
foo=$(file $oldname)
echo "File extension: " $foo
echo "checking if file is zipped"

if [[ $foo == *bunzip2* ]] ; then     echo "Unzipping bunzip file";     bunzip2 -q $oldname ; fi
if [[ $foo == *bzip2* ]] ; then     echo "Unzipping bzip file";     bunzip2 -q $oldname ; fi
if [[ $foo == *gzip* ]] ; then     echo "Unzipping zipfile file";     mv $oldname $oldname.gz;     gunzip -q $oldname.gz ; fi

echo "Removing tmp file and moving to galaxy output"
rm $tmpfile

# If FASTQ
if [ -f $oldname ]; then
    head=$(head $oldname)
    if [ ! "${head:0:1}" == "@" ]; then
        echo "File was not unzipped, file type not supported: " $foo 1>&2
    fi
    mv $oldname $2
fi

# If zipped
if [ -f $oldname.out ]; then
    head=$(head $oldname.out)
    if [ ! "${head:0:1}" == "@" ]; then
        >&2 echo "File was not unzipped, file type not supported: " $foo
    fi
    mv $oldname.out $2
fi

