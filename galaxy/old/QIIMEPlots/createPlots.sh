#!/bin/bash

#As galaxy runs in a virtual env the user python path is reset... therefor I (jasper) used this uqly hacky solution... (for now...)
PYTHONPATH=/home/QIIME_deploy/qiime_180/qiime-galaxy-0.0.1-repository-c2814c3c/lib/:/home/QIIME_deploy/qiime_180/qiime-1.8.0-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/tax2tree-1.0-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/biom-format-1.3.1-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/pynast-1.2.2-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/pprospector-1.0.1-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/qiime-1.8.0-release/lib/:/home/QIIME_deploy/qiime_180/emperor-0.9.3-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/matplotlib-1.3.1-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/sphinx-1.0.4-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/pyqi-0.3.1-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/pycogent-1.5.3-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/MySQL-python-1.2.3-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/mpi4py-1.2.2-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/setuptools-0.6c11-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/gdata-2.0.17-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/pysqlite-2.6.3-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/tornado-3.1.1-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/qcli-0.1.0-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/ipython-latest-repository-126bd580/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/pyzmq-2.1.11-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/SQLAlchemy-0.7.1-release/lib/python2.7/site-packages:/home/QIIME_deploy/qiime_180/pysqlite-2.6.3-release/lib/:/home/QIIME_deploy/qiime_180/numpy-1.7.1-release/lib/python2.7/site-packages:

#Get path of script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo 'Summarize_taxa_though_plots'
summarize_taxa_through_plots.py -f -i $1 -o taxa_summary_test 2>&1
#/usr/bin/Rscript --vanilla $DIR/R/fastaFilesFromJson.R $1

#echo 'clustalo'
#clustalo -i ./fasta_files/all_otus.fa --guidetree-out=./taxa_summary_test/all_otus_omega.tree --outfmt=clu --force
#sed -i s/";"/"|"/g ./taxa_summary_test/all_otus_omega.tree
#mkdir ./taxa_summary_test/treeFiles/
#for f in ./fasta_files/*; do name=$(basename $f); clustalo -i $f --guidetree-out=./taxa_summary_test/treeFiles/$name.tree --outfmt=clu --force; sed -i s/";"/"|"/g ./taxa_summary_test/treeFiles/$name.tree; done

echo 'removing fasta_files'
#rm -R ./fasta_files

#echo 'zipping'
zip -r taxa_summary_test.zip taxa_summary_test

mv taxa_summary_test.zip $2
