docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer

docker run -p 8081:80 -p 8021:21 -p 8800:8800 --privileged=true -v ~/ngtax_storage/:/export/ wurssb/ngtax
