#!/bin/bash
#============================================================================
#title          :NG-Tax
#description    :NG-Tax, a highly accurate and validated pipeline for analysis of 16S rRNA amplicons from complex biomes
#authors        :Wasin Poncheewin, Jesse van Dam, Jasper Koehorst
#date           :2020
#version        :2.0
#============================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Directory clean up
rm -rf $DIR/NGTax*jar
rm -rf $DIR/build

git -C $DIR pull

if [ "$1" == "test" ]; then
	gradle build -b "$DIR/build.gradle" --info
else
	echo "Skipping tests, run './install.sh test' to perform tests"
	gradle build -b "$DIR/build.gradle" -x test --stacktrace
fi

cp $DIR/build/libs/*jar $DIR/

LATEST="$(ls -t $DIR/NGTax*.jar | head -1)"

java -jar $LATEST

java -jar $LATEST -ngtax -fS ./src/test/resources/input/small_1.fastq,./src/test/resources/input/small_2.fastq -for_p "[AG]GGATTAGATACCC" -rev_p "CGAC[AG][AG]CCATGCA[ACGT]CACCT" -refdb ./databases/SILVA_138_SSURef_tax_silva.subset.fasta.gz -for_read_len 70 -rev_read_len 70 -mapFile ./src/test/resources/input/small_mapping.txt -minPerT 1 -b file.biom -t file.ttl 