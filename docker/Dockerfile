FROM bgruening/galaxy-stable

MAINTAINER Jasper Koehorst, jasper.koehorst@wur.nl

ENV GALAXY_CONFIG_BRAND NG-TAX2.0

WORKDIR /galaxy-central

######################################
#### JAVA 8 - Default 7 in Galaxy ####

RUN apt-get update && apt-get install -y --no-install-recommends software-properties-common && \
  echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
  add-apt-repository -y ppa:webupd8team/java && \
  apt-get update && \
  apt-get install -y oracle-java8-installer && \
  rm -rf /var/lib/apt/lists/* && \
  rm -rf /var/cache/oracle-jdk8-installer

ENV JAVA_HOME /usr/lib/jvm/java-8-oracle

#######################################################
################## Tool installation ##################

# Location of the NGTax databases due to export permission issues
RUN mkdir /databases
RUN chown galaxy:galaxy /databases

#BECOME THE GALAXY USER
#GO TO DIRECTORY
USER galaxy

RUN mkdir /galaxy-central/tools/ngtax 
WORKDIR "/galaxy-central/tools/ngtax"

# Such that lookup databases etc can be created... Issue with /export/
# RUN chmod 777 -R /galaxy-central/tools/ngtax

# ADD NGTax / xml and database

# There is an issue with docker and mounted volumes under unix where the folders are owned by root not by local user or galaxy user inside docker

# Databases
ADD SILVA_132_SSURef_tax_silva.fasta.gz /databases/SILVA_132_SSURef_tax_silva.fasta.gz
ADD SILVA_128_SSURef_tax_silva.fasta.gz /databases/SILVA_128_SSURef_tax_silva.fasta.gz
# NGTAx program
ADD NGTax-2.1.12.jar NGTax-2.1.12.jar
# NGTax and demultiplex
ADD NG-Tax.xml NG-Tax.xml
ADD demultiplex.xml demultiplex.xml
# CONFIGURING TOOL CONFIG
ADD tool_conf.xml /local_tools/my_tools.xml
# HTML main view
ADD welcome.html /galaxy-central/static/welcome.html
ADD welcome.html /etc/galaxy/web/welcome.html
#######################################################
#######################################################


ENV GALAXY_CONFIG_TOOL_CONFIG_FILE=config/tool_conf.xml.sample,config/shed_tool_conf.xml.sample,/local_tools/my_tools.xml

USER root

WORKDIR /galaxy-central

# Mark folders as imported from the host.
VOLUME ["/export/", "/data/", "/var/lib/docker"]


# Expose port 80 (webserver), 21 (FTP server), 8800 (Proxy)
EXPOSE :80
EXPOSE :21
EXPOSE :8800

# Autostart script that is invoked during container start
CMD ["/usr/bin/startup"]
# CMD ["/bin/bash"]
