#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd ${DIR}

wget -nc https://www.arb-silva.de/fileadmin/silva_databases/release_132/Exports/SILVA_132_SSURef_tax_silva.fasta.gz
wget -nc https://www.arb-silva.de/fileadmin/silva_databases/release_128/Exports/SILVA_128_SSURef_tax_silva.fasta.gz
wget -nc http://download.systemsbiology.nl/ngtax/NGTax-2.1.12.jar
wget -nc https://gitlab.com/wurssb/NG-Tax/raw/master/galaxy/NG-Tax.xml

# To build the docker image
docker build -t wurssb/ngtax .