package nl.wur.ssb.NGTax;

import junit.framework.TestCase;
import nl.wur.ssb.NGTax.CommandOptions.CommandOptionsNGTax;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {

    public void testLog() {
        Logger logger = LogManager.getLogger(AppTest.class);
        LogBuilder x = logger.atLevel(Level.INFO);
        x.log("bla?");

        System.out.println(logger.isEnabled(Level.INFO));
        logger.info("INFO LOG TEST");
        logger.error("ERROR LOG TEST");
        logger.debug("DEBUG LOG TEST");
        logger.trace("TRACE LOG TEST");

    }

    public void testCount() {
        String taxName = "Bacteria;Bacteroidetes;Bacteroidia;Bacteroidales;Porphyromonadaceae;Parabacteroides";
        Assert.assertEquals(6, StringUtils.countMatches(taxName, ";") + 1);
    }

    public void testRevComplement() {
        NGTax ngTax = new NGTax(null);
        Assert.assertEquals("T", ngTax.revComplementRNA("A"));
        Assert.assertEquals("A", ngTax.revComplementRNA("T"));
        Assert.assertEquals("G", ngTax.revComplementRNA("C"));
        Assert.assertEquals("C", ngTax.revComplementRNA("G"));
        Assert.assertEquals("CGAT", ngTax.revComplementRNA("ATCG"));
    }

    public void testMakePrimerMask() {
        Assert.assertEquals("AACDD", decodeMask(CommandOptionsNGTax.makePrimerMask("AA[AT]GG")));
        Assert.assertEquals("AAODH", decodeMask(CommandOptionsNGTax.makePrimerMask("AA[ATCG]GC")));
    }

    public void testGetSeqForLength() {
        Assert.assertEquals("A-C", NGTax.getSeqForLength("A-C---G", 2));
        Assert.assertEquals("A-C---G", NGTax.getSeqForLength("A-C---G", 3));
        Assert.assertEquals("A", NGTax.getSeqForLength("A-C---G", 1));
        Assert.assertNull(NGTax.getSeqForLength("A-C---G", 4));
        Assert.assertEquals("C---G", NGTax.getSeqRevLength("A-C---G", 2));
        Assert.assertEquals("A-C---G", NGTax.getSeqRevLength("A-C---G", 3));
        Assert.assertEquals("G", NGTax.getSeqRevLength("A-C---G", 1));
        Assert.assertNull(NGTax.getSeqRevLength("A-C---G", 4));
    }

    private String decodeMask(byte[] in) {
        for (int i = 0; i < in.length; i++) {
            in[i] = (byte) (in[i] + 'A' - 1);
        }
        return new String(in);
    }

    // TODO test input zip file
    public void testApp() throws Exception {
        String[] args = {
                "-ngtax",
                "-fS", "./src/test/resources/input/small_1.fastq,./src/test/resources/input/small_2.fastq",
                "-for_p", "[AG]GGATTAGATACCC",
                "-rev_p", "CGAC[AG][AG]CCATGCA[ACGT]CACCT",
                "-refdb", "./databases/SILVA_138_SSURef_tax_silva.subset.fasta.gz",
                "-for_read_len", "70",
                "-rev_read_len", "70",
                "-mapFile", "./src/test/resources/input/small_mapping.txt",
                "-minPerT", "1",
                "-rjl", "5",
                "-identLvl", "100",
                "-errorCorr", "1",
                "-b", "./src/test/resources/output/small_sample.biom",
                "-t", "./src/test/resources/output/small_sample.ttl",
                "-classifyRatio", "0.8",
                "-debug",
                "-prefixId", "007",
                "-mock3", "REFMOCK3-TEST",
                "-mock4", "REFMOCK4-TEST"
        };
        App.main(args);
    }

    public void testAppGZ() throws Exception {
        String[] args = {
                "-ngtax",
                "-fS", "./src/test/resources/input/small_1.fastq.gz,./src/test/resources/input/small_2.fastq.gz",
                "-for_p", "[AG]GGATTAGATACCC",
                "-rev_p", "CGAC[AG][AG]CCATGCA[ACGT]CACCT",
                "-refdb", "./databases/SILVA_138_SSURef_tax_silva.subset.fasta.gz", // bac_ssu_r86.1_20180911.fna.gz
                "-for_read_len", "70",
                "-rev_read_len", "70",
                "-mapFile", "./src/test/resources/input/small_mapping.txt",
                "-minPerT", "1",
                "-rjl", "5",
                "-identLvl", "100",
                "-errorCorr", "1",
                "-b", "./src/test/resources/output/small_sample_gzip.biom",
                "-t", "./src/test/resources/output/small_sample_gzip.ttl",
                "-classifyRatio", "0.8",
                "-debug",
                "-prefixId", "007",
                "-mock3", "REFMOCK3-TEST",
                "-mock4", "REFMOCK4-TEST"
        };
        App.main(args);
    }

    public void testAppBZ() throws Exception {
        String[] args = {
                "-ngtax",
                "-fS", "./src/test/resources/input/small_1.fastq.bz2,./src/test/resources/input/small_2.fastq.bz2",
                "-for_p", "[AG]GGATTAGATACCC",
                "-rev_p", "CGAC[AG][AG]CCATGCA[ACGT]CACCT",
                "-refdb", "./databases/SILVA_138_SSURef_tax_silva.subset.fasta.gz", // bac_ssu_r86.1_20180911.fna.gz
                "-for_read_len", "70",
                "-rev_read_len", "70",
                "-mapFile", "./src/test/resources/input/small_mapping.txt",
                "-minPerT", "1",
                "-rjl", "5",
                "-identLvl", "100",
                "-errorCorr", "1",
                "-b", "./src/test/resources/output/small_sample_bzip2.biom",
                "-t", "./src/test/resources/output/small_sample_bzip2.ttl",
                "-classifyRatio", "0.8",
                "-debug",
                "-prefixId", "007",
                "-mock3", "REFMOCK3-TEST",
                "-mock4", "REFMOCK4-TEST"
        };
        App.main(args);
    }
}