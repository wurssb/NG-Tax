package nl.wur.ssb.NGTax;

import junit.framework.TestCase;

import java.io.File;

public class CoverageTest extends TestCase {
    public void testCoverageCalculation() throws Exception {
        String[] args = {
                "-coverage",
                "-for_p", "ACTCCTACGGRAGGCAGCA",// "[AG]GGATTAGATACCC",
                "-rev_p", "GACTACHVGGGTWTCTAAT", // "CGAC[AG][AG]CCATGCA[ACGT]CACCT",
                "-refdb", "./databases/SILVA_138_SSURef_tax_silva.subset.fasta.gz",
                "-for_read_len", "70",
                "-rev_read_len", "70",
                "-debug"
        };
         App.main(args);
    }
}
