package nl.wur.ssb.NGTax;

import junit.framework.TestCase;

import java.io.File;

public class MakeDBTest extends TestCase {
    public void testDatabaseCreation() throws Exception {
        String[] args = {
                "-ngtax",
                "-fS", "./src/test/resources/input/small_1.fastq,./src/test/resources/input/small_2.fastq",
                "-for_p", "[AG]GGATTAGATACCC",
                "-rev_p", "CGAC[AG][AG]CCATGCA[ACGT]CACCT",
                "-refdb", "./databases/SILVA_128_SSURef_tax_silva.fasta.gz",
                "-for_read_len", "70",
                "-rev_read_len", "70",
                "-mapFile", "./src/test/resources/input/small_mapping.txt",
                "-minPerT", "0",
                "-identLvl", "60",
                "-errorCorr", "1",
                "-b", "./src/test/resources/output/small_sample.biom",
                "-t", "./src/test/resources/output/small_sample.ttl",
                "-classifyRatio", "0.8",
                "-debug"
        };
         App.main(args);
    }

    public void testDatabaseCreationSilvaNoMisMatch() throws Exception {
        String[] args = {
                "-ngtax",
                "-fS", "./src/test/resources/input/small_1.fastq,./src/test/resources/input/small_2.fastq",
                "-for_p", "[AG]GGATTAGATACCC",
                "-rev_p", "CGAC[AG][AG]CCATGCA[ACGT]CACCT",
                "-16db", "./databases/SILVA_128_SSURef_tax_silva.fasta.gz",
                "-for_read_len", "70",
                "-rev_read_len", "70",
                "-mapFile", "./src/test/resources/input/small_mapping.txt",
                "-minPerT", "0",
                "-identLvl", "60",
                "-errorCorr", "1",
                "-b", "./src/test/resources/output/small_sample2.biom",
                "-t", "./src/test/resources/output/small_sample2.ttl",
                "-classifyRatio", "0.8",
                "-debug",
                "-nomismatch"
        };
         App.main(args);
    }

    public void testDB2RDF() throws Exception {
        String input = "./databases/bac_ssu_r86.1_20180911.fna.gz_CCTA[CT]GGG[AG][CGT]GCA[GC]CAG__70_70_mismatch_false_v1_2_full.gz";
        String[] args = {
                "-db2rdf",
                "-i", input,
                "-o", "./databases/bac_ssu_r86.1_20180911.fna.gz_CCTA[CT]GGG[AG][CGT]GCA[GC]CAG__70_70_mismatch_false_v1_2_full.gz.ttl",
                "-for_p", "CCTAYGGGRBGCASCAG"
        };

        if (new File(input).exists()) {
             App.main(args);
        }
    }
}
