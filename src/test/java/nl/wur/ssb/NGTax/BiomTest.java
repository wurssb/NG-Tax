package nl.wur.ssb.NGTax;

import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import static nl.wur.ssb.NGTax.Biom2Rdf.ngTaxBiomParser;

public class BiomTest extends TestCase {
    static final Logger logger = LogManager.getLogger(BiomTest.class);

    private static String rootIRI = "http://ssb.wur.nl/NG-Tax/0.1/";

    /**
     * Test converting BIOM to RDF (ngTaxBiomParser) method.
     *
     * @throws Exception
     */
    public void testBiom2rdf() throws Exception {
        new NGTaxResult();
        File inputFile = new File("./src/test/resources/input/small_sample.biom");
        String data = new String(Files.readAllBytes(Paths.get(inputFile.getAbsolutePath())));
        NGTaxResult res = NGTaxResult.fromJSon(data);
        logger.info(res);

        String fileLocation = "./src/test/resources/output/test2.ttl";

        ngTaxBiomParser(res, fileLocation);
    }

    public void testBiom2RdfNoDB() throws Exception {
        String[] args = {
                "-biom2rdf",
                "-i", "./src/test/resources/input/small_noDB.biom",
                "-o", "./src/test/resources/output/small_noDB.ttl"
        };
        App.main(args);
    }

    /**
     * Test direct conversion options within the App.
     *
     * @throws Exception
     */
    public void testDirectBiom2Rdf() throws Exception {
        // Conversion test
        String[] args = {
                "-biom2rdf",
                "-i", "./src/test/resources/output/small_sample.biom",
                "-o", "./src/test/resources/output/small_sample.ttl"
        };
        App.main(args);
    }

    public void testMock() throws Exception {
        String[] args = {
                "-biom2rdf",
                "-i", "./src/test/resources/output/mock.biom",
                "-o", "./src/test/resources/output/mock.ttl"
        };
        App.main(args);
    }
}