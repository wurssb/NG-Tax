package nl.wur.ssb.NGTax;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.lang3.StringUtils;

/**
 * Unit test for simple App.
 */
public class LevenDistTest extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public LevenDistTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(LevenDistTest.class);
    }
    
    public void testDist() {
        System.out.println(StringUtils.getLevenshteinDistance("ATGAAA","ATAAA"));
    }
}