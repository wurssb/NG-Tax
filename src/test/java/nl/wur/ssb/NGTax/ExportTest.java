package nl.wur.ssb.NGTax;

import junit.framework.TestCase;

public class ExportTest extends TestCase {
    public void testExport() throws Exception {
        String args[] = new String[]{
                "-exportFasta",
                "-i", "./src/test/resources/input/biom/small_sample.biom",
                "-o", "./src/test/resources/output/small_sample.biom.zip",
                "-createTree"
        };
        App.main(args);
    }
}