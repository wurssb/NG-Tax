package nl.wur.ssb.NGTax;

import junit.framework.TestCase;

public class NGTaxResultTest extends TestCase {
    public void testLevensDistance() {
        Levenshtein levenshtein = new Levenshtein("ABCDEFGHIJKLMN".getBytes(), 5);

        String seq1 = "";
        String seq2 = "";

        //5 deletion at begin
        seq2 = "FGHIJKLMNOOOOO";
        assert levenshtein.getLevenshteinDistance(seq2) == 5;

        //0 mutation
        seq2 = "ABCDEFGHIJKLMN";
        assert levenshtein.getLevenshteinDistance(seq2) == 0;
        //1 mutation
        seq2 = "ABCDEFGHAJKLMN";
        assert levenshtein.getLevenshteinDistance(seq2) == 1;
        //2 mutations
        seq2 = "ABCMEFGMIJKLMN";
        assert levenshtein.getLevenshteinDistance(seq2) == 2;
        //to many 6 mutation
        seq2 = "ABCDOOGOOJOOMN";
        assert levenshtein.getLevenshteinDistance(seq2) == -1;

        //5 mutation at beginning
        seq2 = "OOOOOFGHIJKLMN";
        assert levenshtein.getLevenshteinDistance(seq2) == 5;

        //1 insertion 1 deletion
        seq2 = "ABCDFGHIJKLOMN";
        assert levenshtein.getLevenshteinDistance(seq2) == 2;

        //3 deletions
        seq2 = "ABCDEFGKLMNOOO";
        assert levenshtein.getLevenshteinDistance(seq2) == 3;

        //2 deletion
        seq2 = "ABCDEHIJKLMNOO";
        assert levenshtein.getLevenshteinDistance(seq2) == 2;

        //2 insertion
        seq2 = "ABCDEFGHKLMNOO";
        assert levenshtein.getLevenshteinDistance(seq2) == 2;

        //2 insertion at begin
        seq2 = "CDEFGHIJKLMNOO";
        assert levenshtein.getLevenshteinDistance(seq2) == 2;

        //2 deletion at begin
        seq2 = "CDEFGHIJKLMNOO";
        assert levenshtein.getLevenshteinDistance(seq2) == 2;


        //5 insertion at begin
        seq2 = "FGHIJKLMNOOOOO";
        assert levenshtein.getLevenshteinDistance(seq2) == 5;

        //5 deletion at end (same as 5 mismatches at the end)
        seq2 = "ABCDEFGHIOOOOO";
        assert levenshtein.getLevenshteinDistance(seq2) == 5;

        //5 insertion at end
        seq2 = "ABCDEFGHIOOOOO";
        assert levenshtein.getLevenshteinDistance(seq2) == 5;

        //5 deletion at end -1
        seq2 = "ABCDEFGHOOOOOI";
        assert levenshtein.getLevenshteinDistance(seq2) == 5;

        //5 insertion at end -1
        seq2 = "ABCDEFGHOOOOON";
        assert levenshtein.getLevenshteinDistance(seq2) == 5;
    }
  /*
  public void testJSonSave() throws IOException
  {
    NGTaxResult result = new NGTaxResult();
    result.date = LocalDateTime.parse("2016-08-10T12:57:25.723");
    result.addValue("1","sample1",4);
    result.addValue("2","sample2",5);
    result.addValue("3","sample4",6);
    result.addValue("4","sample5",7);
    result.addValue("1","sample3",8);
    result.addValue("1","sample2",9);
    result.addValue("2","sample1",10);
    result.addTaxonToOtu("1","k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides");
    result.addTaxonToOtu("2","k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides");
    result.addTaxonToOtu("3","k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides");
    result.addTaxonToOtu("4","k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides");
    String jsonText = result.toJSon();

    checkRes("test.biom",jsonText);
    //NGTaxResult result2 = NGTaxResult.fromJSon(jsonText);


    //Date date = Date.
  }

  private void checkRes(String fileCorrect,String toTest) throws IOException
  {
    InputStream in = this.getClass().getResourceAsStream("/" + fileCorrect);
    String orig = IOUtils.toString(in);
    in.close();
    int line = 0;
    int col = 0;
    String sourceLastLine = "";
    for(int i = 0;i < orig.length();i++)
    {
      assert "file :" + fileCorrect + " (" + line + ":" + col + ") " + sourceLastLine + "...",i < toTest.length())
      char origChar = orig.charAt(i);
      char newChar = toTest.charAt(i);
      assert "file :" + fileCorrect + " (" + line + ":" + col + ") " + sourceLastLine + "...",origChar == newChar;
      sourceLastLine += new String(new char[] {origChar});
      col++;
      if(origChar == '\n')
      {
        sourceLastLine = "";
        line++;
        col = 0;
      }
    }
  }*/
}
