package nl.wur.ssb.NGTax;

import junit.framework.TestCase;

import java.io.File;

public class LocalTest extends TestCase {
    public void testBiom2RDF() throws Exception {
        File input = new File("A_INS001-P1-13-11-16.biom");
        if (input.exists()) {
            String[] args = {
                    "-biom2rdf",
                    "-i", input.getAbsolutePath(),
                    "-o", "A_INS001-P1-13-11-16.ttl"};
            App.main(args);
        }
    }
}
