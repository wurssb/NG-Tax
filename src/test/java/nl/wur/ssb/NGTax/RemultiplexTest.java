package nl.wur.ssb.NGTax;

import junit.framework.TestCase;

public class RemultiplexTest extends TestCase{

    /**
     * Test direct conversion options within the App.
     *
     * @throws Exception
     */
    public void testRemultiplex() throws Exception {
        String[] args = {"-remultiplex", "-fS", "./src/test/resources/input/small_1.fastq,./src/test/resources/input/small_2.fastq",
                "-for_p", "[AG]GGATTAGATACCC", "-rev_p", "CGAC[AG][AG]CCATGCA[ACGT]CACCT",
                "-mapFile", "./src/test/resources/input/small_mapping.txt"};
//        App.main(args);
    }

    public void testNoMapping() throws Exception {
        String[] args = {"-remultiplex", "-fS", "./src/test/resources/input/small_1.fastq,./src/test/resources/input/small_2.fastq",
                "-for_p", "[AG]GGATTAGATACCC", "-rev_p", "CGAC[AG][AG]CCATGCA[ACGT]CACCT"};
//        App.main(args);
    }

    public void testFolder() throws Exception {
        String[] args = {"-ngtax", "-folder", "/Users/lumluk/RemultiplexerTest/",
                "-for_p", "[AG]GGATTAGATACCC", "-rev_p", "CGAC[AG][AG]CCATGCA[ACGT]CACCT", "-mapFile", "./src/test/resources/input/mappingZichtbaar.txt",
                "-b", "./src/test/resources/output/mappingZichtbaar.biom"};
//        App.main(args);
    }

}
