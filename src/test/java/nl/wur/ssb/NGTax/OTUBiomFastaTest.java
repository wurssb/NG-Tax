package nl.wur.ssb.NGTax;

import junit.framework.TestCase;

public class OTUBiomFastaTest extends TestCase {

    /**
     * Test direct conversion options within the App.
     *
     * @throws Exception
     */
    public void testDirectBiom2OTUFasta() throws Exception {
        String[] args = {"-otu2fasta", "-i", "./src/test/resources/input/small_sample.biom", "-o", "./src/test/resources/output/"};
        App.main(args);
    }

    public void testDirectBiom2OTUFastaEmptyDB() throws Exception {
        String[] args = {"-otu2fasta", "-i", "./src/test/resources/output/mapping_file_test_emptyempty.biom", "-o", "./src/test/resources/output/"};
//        App.main(args);
    }


}
