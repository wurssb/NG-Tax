package nl.wur.ssb.NGTax;

import com.sun.jdi.request.DuplicateRequestException;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.junit.Test;

import java.util.HashSet;
import java.util.Iterator;

public class MockTest {

    @Test
    public void testApp() throws Exception {
        String[] args = {"-ngtax",
                "-for_p", "GTGYCAGCMGCCGCGGTAA", // "[AG]GGATTAGATACCC",
                "-rev_p", "GGACTACNVGGGTWTCTAAT", // "CGAC[AG][AG]CCATGCA[ACGT]CACCT",
                "-refdb", "./databases/SILVA_138_SSURef_tax_silva.subset.fasta.gz", // bac_ssu_r86.1_20180911.fna.gz
                "-mapFile", "./src/test/resources/input/small_mapping.txt",
                "-for_read_len", "100",
                "-rev_read_len", "100",
                "-minPerT", "0.00001",
                "-identLvl", "100",
                "-errorCorr", "1",
                "-b", "./src/test/resources/output/mock3.biom",
                "-t", "./src/test/resources/output/mock3.ttl",
                "-classifyRatio", "0.8",
                "-debug",
                "-mock3","REFMOCK3-TEST",
                "-mock4", "REFMOCK4-TEST"
        };
        App.main(args);

        // Validate RDF file...
        String file = "/Users/jasperk/gitlab/NG-Tax/src/test/resources/output/mock3.ttl";
        Domain domain = new Domain("file://"+file);
        Iterator<ResultLine> resultLineIterator = domain.getRDFSimpleCon().runQuery("validate.sparql", true).iterator();

        HashSet<String> checker = new HashSet<>();

        while (resultLineIterator.hasNext()) {
            ResultLine resultLine = resultLineIterator.next();
            String line = resultLine.getLitString("fwd") + "-" + resultLine.getLitString("rev");
            if (checker.contains(line)) {
                System.err.println(line);
                throw new DuplicateRequestException("ASV duplication detected");
            }
            checker.add(line);
        }
    }
}
