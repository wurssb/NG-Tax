package nl.wur.ssb.NGTax;

import junit.framework.TestCase;

import java.io.File;

public class ReClassifyTest extends TestCase{

    public void testReClassifyNoTurtle() throws Exception {
        if (new File("./src/test/resources/input/biom/small_sample.biom").exists()) {
            String[] args = {
                    "-reClassify",
                    "-i", "./src/test/resources/input/biom/small_sample.biom",
                    "-refdb", "./databases/SILVA_138_SSURef_tax_silva.subset.fasta.gz",
                    "-o", "./src/test/resources/output/small_sample_zero_test_reclassify.biom"
            };
            App.main(args);
        }
    }

    public void testReClassifyBiom() throws Exception {
        if (new File("./src/test/resources/input/biom/small_sample.biom").exists()) {
            String[] args = {
                    "-reClassify",
                    "-i", "./src/test/resources/input/biom/small_sample.biom",
                    "-refdb", "./databases/SILVA_138_SSURef_tax_silva.subset.fasta.gz",
                    "-o", "src/test/resources/output/small_reannotate.biom",
                    "-ttl", "src/test/resources/output/small_reannotate.ttl"
            };
            App.main(args);
        }
    }
}
