package nl.wur.ssb.NGTax;

import junit.framework.TestCase;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DemultiplexTest extends TestCase {

    /**
     * Test direct conversion options within the App.
     *
     * @throws Exception
     */
    public void testDirectDemultiplex() throws Exception {
        String base =  "./tmp/";
        // "./src/test/resources/input/small_1_modified.fastq,./src/test/resources/input/small_2_modified.fastq"
        String[] args = {
                "-demultiplex",
                "-fS", base+"NG-9086_JE02_lib109649_4374_2_1.fastq.bz2,"+base+"NG-9086_JE02_lib109649_4374_2_2.fastq.bz2",
                "-for_p", "GTGYCAGCMGCCGCGGTAA", "-rev_p", "GGACTACNVGGGTWTCTAAT",
                "-mapFile", base + "/jobs/mapping_file_PRJ_TIFN-ME001-K444A_70a76d295c2dec58433997c69bb849e2.txt",
                "-debug",
        };
        App.main(args);
    }

    public void testDirectDemultiplexTwoLibraries() throws Exception {
        String[] args = {
                "-demultiplex",
                "-fS", "./src/test/resources/input/small_1_modified.fastq,./src/test/resources/input/small_2_modified.fastq ./src/test/resources/input/small_3_modified.fastq,./src/test/resources/input/small_4_modified.fastq",
                "-for_p", "[AG]GGATTAGATACCC",
                "-rev_p", "CGAC[AG][AG]CCATGCA[ACGT]CACCT",
                "-mapFile", "./src/test/resources/input/small_mapping_two_libraries.txt"
        };
        App.main(args);
    }

    public void testBZIP2DirectDemultiplexTwoLibraries() throws Exception {
        String[] args = {
                "-demultiplex",
                "-fS", "./src/test/resources/input/small_1_modified.fastq.bz2,./src/test/resources/input/small_2_modified.fastq.bz2 ./src/test/resources/input/small_3_modified.fastq,./src/test/resources/input/small_4_modified.fastq",
                "-for_p", "[AG]GGATTAGATACCC",
                "-rev_p", "CGAC[AG][AG]CCATGCA[ACGT]CACCT",
                "-mapFile", "./src/test/resources/input/small_mapping_two_libraries.txt",
                "-debug"
        };
        App.main(args);
    }

    public void testDirectDemultiplexUntrim() throws Exception {
        String[] args = {"-demultiplex", "-untrim","-fS", "./src/test/resources/input/small_1_modified.fastq,./src/test/resources/input/small_2_modified.fastq",
                "-for_p", "[AG]GGATTAGATACCC", "-rev_p", "CGAC[AG][AG]CCATGCA[ACGT]CACCT",
                "-mapFile", "./src/test/resources/input/small_mapping.txt"};
        App.main(args);
    }

    public void testDirectDemultiplexTwoLibrariesUntrim() throws Exception {
        String[] args = {"-demultiplex", "-untrim", "-fS", "./src/test/resources/input/small_1_modified.fastq,./src/test/resources/input/small_2_modified.fastq ./src/test/resources/input/small_3_modified.fastq,./src/test/resources/input/small_4_modified.fastq",
                "-for_p", "[AG]GGATTAGATACCC", "-rev_p", "CGAC[AG][AG]CCATGCA[ACGT]CACCT",
                "-mapFile", "./src/test/resources/input/small_mapping_two_libraries.txt"};
        App.main(args);
    }

    public void testZipDemultiplex() throws Exception {
        String[] args = {"-demultiplex", "-fS", "./src/test/resources/input/small_1.fastq,./src/test/resources/input/small_2.fastq",
                "-for_p", "[AG]GGATTAGATACCC", "-rev_p", "CGAC[AG][AG]CCATGCA[ACGT]CACCT",
                "-mapFile", "./src/test/resources/input/small_mapping.txt",
                "-zip", "./src/test/resources/output/testzip.zip"};
        App.main(args);
    }

    public void testDirectDemultiplexSingleColumnMappingFile() throws Exception {
        String[] args = {"-demultiplex", "-fS", "./src/test/resources/input/small_1.fastq,./src/test/resources/input/small_2.fastq",
                "-for_p", "[AG]GGATTAGATACCC", "-rev_p", "CGAC[AG][AG]CCATGCA[ACGT]CACCT",
                "-mapFile", "./src/test/resources/input/small_mapping_single.txt"};
//        App.main(args);
    }

//    public void testFailedMultiplexFolder() throws Exception {
//        String[] args = {
//                "-folder", "./src/test/resources/input/demulti_single/",
//                "-for_p", "CCTACGGG[ACGT]GGC[AT]GCAG",
//                "-rev_p", "GACTAC[ACT][ACG]GGGTATCTAATCC",
//                "-b","./src/test/resources/output/demulti_single.biom"
//        };
//        // "-mapFile", "./src/test/resources/input/small_mapping_single.txt"
//        App.main(args);
//
//    }

    public void testAppPairFolder() throws Exception {
        String[] args = {
                "-ngtax",
                "-folder", "./src/test/resources/input/demultiplexed/",
                "-for_p", "[AG]GGATTAGATACCC",
                "-rev_p", "CGAC[AG][AG]CCATGCA[ACGT]CACCT",
                "-for_read_len", "70",
                "-rev_read_len", "70",
                "-mapFile", "./src/test/resources/input/small_mapping.txt",
                "-minPerT", "0.1",
                "-identLvl", "100",
                "-errorCorr", "1",
                "-b", "./src/test/resources/output/small_sample.biom",
                "-t", "./src/test/resources/output/small_sample.ttl",
                "-classifyRatio", "0.5",
                // "-m", "false",
                // "-fQF", "false",
                "-debug"
        };
        App.main(args);
    }

    public void testAppSingleFolder() throws Exception {
        String[] args = {
                "-ngtax",
                "-single",
                "-folder", "./src/test/resources/input/demultiplexed/",
                "-for_p", "[AG]GGATTAGATACCC",
                "-16db", "./databases/SILVA_138_SSURef_tax_silva.subset.fasta.gz",
                "-for_read_len", "70",
                "-mapFile", "./src/test/resources/input/small_mapping_single.txt",
                "-minPerT", "0.1",
                "-identLvl", "100",
                "-errorCorr", "1",
                "-b", "./src/test/resources/output/small_single.biom",
                "-t", "./src/test/resources/output/small_single.ttl",
                "-classifyRatio", "0.5",
                "-m",
                "-debug"
        };
        // App.main(args);
    }
}
