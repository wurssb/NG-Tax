package nl.wur.ssb.NGTax;

import com.beust.jcommander.converters.IParameterSplitter;

import java.util.Arrays;
import java.util.List;

public class NoneParameterSplitter implements IParameterSplitter {

    public List<String> split(String value) {
        return Arrays.asList(value);
    }

}