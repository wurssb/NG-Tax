package nl.wur.ssb.NGTax;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class ASVMatchStats implements Serializable {
    @Expose
    int taxonHitCount;
    @Expose
    String tax;
    @Expose
    double ratio = Double.NaN;
    @Expose
    int mismatchCount;

    public ASVMatchStats(String tax, int mismatchCount) {
        this.tax = tax;
        this.mismatchCount = mismatchCount;
    }

    public ASVMatchStats(int count, String tax, double ratio, int mismatchCount) {
        this.taxonHitCount = count;
        this.tax = tax;
        this.ratio = ratio;
        this.mismatchCount = mismatchCount;
    }

    public ASVMatchStats clone() {
        return new ASVMatchStats(this.taxonHitCount, this.tax, this.ratio, this.mismatchCount);
    }
}
