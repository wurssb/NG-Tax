package nl.wur.ssb.NGTax;

import nl.wur.ssb.NGTax.CommandOptions.CommandOptionsDemultiplex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static nl.wur.ssb.NGTax.CommandOptions.CommandOptionsNGTax.fixPrimer;
import static nl.wur.ssb.NGTax.Generic.isGzipped;
import static nl.wur.ssb.NGTax.generic.Generic.getMappingInfo;

class Demultiplex {

    private static int primerMatchCount = 0;
    private static int barcodeMatchCount = 0;
    static final Logger logger = LogManager.getLogger(Demultiplex.class);
    private static CommandOptionsDemultiplex commandOptions;

    static void directDemultiplex(CommandOptionsDemultiplex argsDemultiplex, String fastQSet, String mappingFile, String fPrimer, String rPrimer, File zip, Boolean untrim, File output) throws Exception {
        commandOptions = argsDemultiplex;
        fPrimer = fixPrimer(fPrimer);
        if (rPrimer != null)
            rPrimer = fixPrimer(rPrimer);

        HashMap<Integer, List<HashMap<List<String>, String>>> mappingInfo = getMappingInfo(mappingFile);

        demultiplex(fastQSet, mappingInfo, fPrimer, rPrimer, zip, untrim, output);

        logger.info("Finished demultiplexed");
    }

    /**
     * Demultiplex fastq files
     * Remove both barcode and primer.
     *
     * @param fastQSet
     * @param mappingInfo
     * @param fPrimer
     * @param rPrimer
     * @param output
     * @throws Exception
     */
    private static void demultiplex(String fastQSet, HashMap<Integer, List<HashMap<List<String>, String>>> mappingInfo, String fPrimer, String rPrimer, File zip, Boolean untrim, File output) throws Exception {

        /*
            Create output fastq files, both forward and reverse, based on the sample name.
            Keep it in a hashmap.

            Create a list of forward and reverse barcode
         */

        // To check if file already exists.
        HashSet<String> check = new HashSet<>();

        output.mkdir();

        // Go through each fastq set
        int fastQsetNumber = 0;
        for (String set : fastQSet.split(" ")) {
            // run through each fastQ set, each set is separated using space.
            logger.info("Processing fastq set: " + set);
            fastQsetNumber = fastQsetNumber + 1;

            // Initiate multiple files in a HashMap
            HashMap<String, BufferedWriter> fileToWrite = new HashMap<>();
            List<String> forwardBarcodeSet = new ArrayList<>();
            List<String> reverseBarcodeSet = new ArrayList<>();

            for (HashMap<List<String>, String> key : mappingInfo.get(fastQsetNumber)) {
                for (List barcode : key.keySet()) {
                    BufferedWriter forwardWriter = new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(new File(output + "/" + key.get(barcode) + "_f.fastq.gz"))), "UTF-8"));
                    BufferedWriter reverseWriter = new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(new File(output + "/" + key.get(barcode) + "_r.fastq.gz"))), "UTF-8"));

                    fileToWrite.put(key.get(barcode) + "_f.fastq", forwardWriter);
                    fileToWrite.put(key.get(barcode) + "_r.fastq", reverseWriter);

                    forwardBarcodeSet.add((String) barcode.get(0));
                    reverseBarcodeSet.add((String) barcode.get(1));
                }
            }

            String tmp[] = set.split(","); // Split each fastQ set by comma(,)

            if (tmp.length < 2 && rPrimer != null) // Check if the file set correctly separated by comma(,)
                throw new Exception("fastQ files should be given as sets separated by a , : " + set);

            // Assign forward and reverse file
            String forwardFileName = tmp[0]; // forwardFiles = small_1.fastq

            String reverseFileName = "";
            if (rPrimer != null)
                reverseFileName = tmp[1]; // reverseFiles = small_2.fastq

            // Check if the the fastQ file is in the 'check' hashset.
            // If it is in the hashset, it has been used before which is not allowed.
            if (check.contains(forwardFileName) || check.contains(reverseFileName) || forwardFileName.equals(reverseFileName))
                throw new Exception("fastq file can only be used once");

            // Add files to 'check' hashset
            check.add(forwardFileName);

            if (rPrimer != null)
                check.add(reverseFileName);

            // Set up scanner to read fastq file
            BufferedReader forwardFile;
            BufferedReader reverseFile = null;
            if (isGzipped(new File(forwardFileName))) {
                logger.info("Processing gzipped file");
                InputStream finFWD = new FileInputStream(forwardFileName);
                InputStream inStreamFWD = new GZIPInputStream(finFWD);
                Reader readerFWD = new InputStreamReader(inStreamFWD);
                forwardFile = new BufferedReader(readerFWD);
                if (rPrimer != null) {
                    InputStream finREV = new FileInputStream(reverseFileName);
                    InputStream inStreamREV = new GZIPInputStream(finREV);
                    Reader readerREV = new InputStreamReader(inStreamREV);
                    reverseFile = new BufferedReader(readerREV);
                }
            } else if (forwardFileName.endsWith(".bz2") && (reverseFileName.endsWith(".bz2") || reverseFile == null)) {
                logger.info("Processing bzipped file");
                forwardFile = new BufferedReader(new InputStreamReader(new BZip2CompressorInputStream(new FileInputStream(forwardFileName))));

//                FileInputStream finFWD = new FileInputStream(forwardFileName);
//                BufferedInputStream bisFWD = new BufferedInputStream(finFWD);
//                CompressorInputStream inputFWD = new CompressorStreamFactory().createCompressorInputStream(bisFWD);
//                forwardFile = new BufferedReader(new InputStreamReader(inputFWD));
                if (rPrimer != null) {
                    reverseFile = new BufferedReader(new InputStreamReader(new BZip2CompressorInputStream(new FileInputStream(forwardFileName))));

//                    FileInputStream finREV = new FileInputStream(reverseFileName);
//                    BufferedInputStream bisREV = new BufferedInputStream(finREV);
//                    CompressorInputStream inputREV = new CompressorStreamFactory().createCompressorInputStream(bisREV);
//                    reverseFile = new BufferedReader(new InputStreamReader(inputREV));
                }
            } else if (!isGzipped(new File(forwardFileName))) {
                logger.info("Processing plain file");
                forwardFile = new BufferedReader(new FileReader(forwardFileName));
                if (rPrimer != null) {
                    reverseFile = new BufferedReader(new FileReader(reverseFileName));
                }
            } else {
                throw new Exception("Unrecognized file extension, .fastq, .bz2, .gz, .dat (galaxy/fastq) supported");
            }

            String f_header = null;
            String r_header = null;
            String f_seq = null;
            String r_seq = null;
            String f_plus = null;
            String r_plus = null;
            String f_qual;
            String r_qual;
            int fastqLine = 0;
            int fastqMatch = 0;
            int lineCounter = 0;

            while (true) {
                String fline = forwardFile.readLine();
                String rline = null;
                if (reverseFile != null) {
                    rline = reverseFile.readLine();
                }

                if (fline == null || (rline == null && reverseFile != null))
                    break;

                lineCounter = lineCounter + 1;

                if (lineCounter == 1) {
                    f_header = fline;
                    r_header = rline;
                    // TODO check if starts with @?
                    if (!f_header.startsWith("@")) {
                        throw new Exception("Unknown file detected. FASTQ headers should start with @");
                    }

                } else if (lineCounter == 2) {
                    f_seq = fline;
                    r_seq = rline;
                } else if (lineCounter == 3) {
                    f_plus = fline;
                    r_plus = rline;
                } else if (lineCounter == 4) {
                    lineCounter = 0;

                    f_qual = fline;
                    r_qual = rline;

                    fastqLine = fastqLine + 1;

                    int threshold = 1000000;
                    if (commandOptions.debug)
                        threshold = 1000;
                    if (fastqLine % threshold == 0) {
                        logger.info("Parsed " + fastqLine + " reads and matched " + fastqMatch);
                    }

                    List<String> matchingResults = patternMatching(fPrimer, rPrimer, f_seq, r_seq, forwardBarcodeSet, reverseBarcodeSet);

                    List<String> barcode = new ArrayList<>();
                    barcode.add(matchingResults.get(0));

                    if (reverseFile != null)
                        barcode.add(matchingResults.get(2));
                    else
                        barcode.add(matchingResults.get(0));

                    if (barcode.get(0) != null) {
                        for (HashMap<List<String>, String> key : mappingInfo.get(fastQsetNumber)) {
                            if (key.keySet().contains(barcode)) {
                                if (!untrim) {
                                    f_seq = f_seq.substring(Integer.parseInt(matchingResults.get(1)), f_seq.length());
                                    f_qual = f_qual.substring(Integer.parseInt(matchingResults.get(1)), f_qual.length());
                                    if (r_seq != null) {
                                        r_seq = r_seq.substring(Integer.parseInt(matchingResults.get(3)), r_seq.length());
                                        r_qual = r_qual.substring(Integer.parseInt(matchingResults.get(3)), r_qual.length());
                                    }
                                }

                                if (f_seq.length() == 0) {
                                    // Skipp forward and optional reverse read if the forward is too small
                                } else if (r_seq != null && r_seq.length() == 0) {
                                    // Skip forward and reverse read as the reverse is too small
                                } else {
                                    fastqMatch = fastqMatch + 1;
                                    BufferedWriter forwardWriter = fileToWrite.get(key.get(barcode) + "_f.fastq");

                                    forwardWriter.write(f_header + '\n');
                                    forwardWriter.write(f_seq + '\n');
                                    forwardWriter.write(f_plus + '\n');
                                    forwardWriter.write(f_qual + '\n');
                                    if (r_seq != null) {
                                        BufferedWriter reverseWriter = fileToWrite.get(key.get(barcode) + "_r.fastq");
                                        reverseWriter.write(r_header + '\n');
                                        reverseWriter.write(r_seq + '\n');
                                        reverseWriter.write(r_plus + '\n');
                                        reverseWriter.write(r_qual + '\n');
                                    }
                                }
                            }
                        }
                    }
                }
            }

            for (String key : fileToWrite.keySet()) {
                fileToWrite.get(key).close();
            }

            /*
                Split files by libraries.
                By merge the files split by sample created above.
             */

            BufferedWriter libForward = new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(new File(output + "/library" + fastQsetNumber + "_f.fastq.gz"))), "UTF-8"));
            BufferedWriter libReverse = new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(new File(output + "/library" + fastQsetNumber + "_r.fastq.gz"))), "UTF-8"));

            for (HashMap<List<String>, String> key : mappingInfo.get(fastQsetNumber)) {
                for (List barcode : key.keySet()) {
                    Reader fwd = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(new File(output + "/" + key.get(barcode) + "_f.fastq.gz")))));
                    IOUtils.copy(fwd, libForward);

                    Reader rev = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(new File(output + "/" + key.get(barcode) + "_r.fastq.gz")))));
                    IOUtils.copy(rev, libReverse);
                }
            }
            libForward.close();
            libReverse.close();
        }
// Add md5 files
        for (File filename : output.listFiles()) {
            String md5value = null;
            FileInputStream is = null;
            try {
                is = new FileInputStream(filename);
                md5value = DigestUtils.md5Hex(IOUtils.toByteArray(is));
            } catch (IOException e) {
                System.out.println("Error on generating md5 files");
            } finally {
                IOUtils.closeQuietly(is);
            }
            // Write to file
            String md5filename = String.valueOf(filename) + ".md5";
            BufferedWriter writer = new BufferedWriter(new FileWriter(md5filename));
            writer.write(md5value);
            writer.close();
        }


        // Enable zip option for the entire directory.
        if (zip != null) {
            if (!zip.exists()) {
                Files.createFile(Paths.get(zip.getAbsolutePath()));
            }
            Path p = Paths.get(zip.getAbsolutePath());

            try (ZipOutputStream zs = new ZipOutputStream(Files.newOutputStream(p))) {
                Path pp = Paths.get(output.getAbsolutePath());
                Files.walk(pp)
                        .filter(path -> !Files.isDirectory(path))
                        .forEach(path -> {
                            ZipEntry zipEntry = new ZipEntry(pp.relativize(path).toString());
                            try {
                                zs.putNextEntry(zipEntry);
                                Files.copy(path, zs);
                                zs.closeEntry();
                            } catch (IOException e) {
                                logger.error(e);
                            }
                        });
            }
            for (File filename : output.listFiles()) {
                logger.info("Removing " + filename);
                filename.delete();
            }
            logger.info("Removing " + output);
            output.delete();
        }
    }

    /**
     * Match pattern of the barcode and primer.
     *
     * @param fPrimer
     * @param rPrimer
     * @param f_seq
     * @param r_seq
     * @param forwardBarcodeSet
     * @param reverseBarcodeSet
     * @return List of [forward barcode, whole pattern matched length, reverse barcode, whole pattern matched length]
     */
    private static List<String> patternMatching(String fPrimer, String rPrimer, String f_seq, String r_seq, List<String> forwardBarcodeSet, List<String> reverseBarcodeSet) {
        ArrayList<String> matchedResults = new ArrayList<>();

        // Degenerative option here
        ArrayList<Pattern> fPrimers = Database.createDegeneratePrimers(fPrimer, 0, true);
        ArrayList<Pattern> rPrimers = null;
        if (rPrimer != null) {
            rPrimers = Database.createDegeneratePrimers(rPrimer, 0, true);
        }


        for (int i = 0; i < forwardBarcodeSet.size(); i++) {
            boolean barcodeMatch = false;
            if (r_seq == null) {
                if (f_seq.startsWith(forwardBarcodeSet.get(i))) {
                    barcodeMatch = true;
                }
            } else if (f_seq.startsWith(forwardBarcodeSet.get(i)) && r_seq.startsWith(reverseBarcodeSet.get(i))) {
                barcodeMatch = true;
            } else if (f_seq.startsWith(reverseBarcodeSet.get(i)) && r_seq.startsWith(forwardBarcodeSet.get(i))) {
                barcodeMatch = true;
            }
            // Hit found
            if (barcodeMatch) {
                barcodeMatchCount = barcodeMatchCount + 1;
                // Now create patterns when a primer mismatch is allowed
                for (int j = 0; j < fPrimers.size(); j++) {
                    Pattern forwardPattern = Pattern.compile("^(" + forwardBarcodeSet.get(i) + ".*" + fPrimers.get(j) + ")");
                    Matcher fMatches = forwardPattern.matcher(f_seq);
                    Pattern reversePattern;
                    Matcher rMatches = null;
                    if (r_seq != null) {
                        reversePattern = Pattern.compile("^(" + reverseBarcodeSet.get(i) + ".*" + rPrimers.get(j) + ")");
                        rMatches = reversePattern.matcher(r_seq);
                    }

                    // Single end or Both need a hit
                    if (r_seq == null && fMatches.find()) {
                        primerMatchCount = primerMatchCount + 1;
                        matchedResults = new ArrayList<>();
                        matchedResults.add(forwardBarcodeSet.get(i));
                        matchedResults.add(String.valueOf(fMatches.group().length()));
                        matchedResults.add(null);
                        matchedResults.add(null);
                        return matchedResults;
                    } else if (fMatches.find() && rMatches.find()) {
                        primerMatchCount = primerMatchCount + 1;
                        matchedResults = new ArrayList<>();
                        matchedResults.add(forwardBarcodeSet.get(i));
                        matchedResults.add(String.valueOf(fMatches.group().length()));
                        matchedResults.add(reverseBarcodeSet.get(i));
                        matchedResults.add(String.valueOf(rMatches.group().length()));
                        return matchedResults;
                    }

                    if (r_seq == null)
                        continue;

                    forwardPattern = Pattern.compile("^(" + reverseBarcodeSet.get(i) + ".*" + fPrimers.get(j) + ")");
                    reversePattern = Pattern.compile("^(" + forwardBarcodeSet.get(i) + ".*" + rPrimers.get(j) + ")");

                    fMatches = forwardPattern.matcher(r_seq);
                    rMatches = reversePattern.matcher(f_seq);
//                    if (fMatches.find()) {
//                        System.err.println(fMatches.find() + " " + rMatches.find());
//                    }
                    // Single end or Both need a hit
                    if (r_seq == null && fMatches.find()) {
                        primerMatchCount = primerMatchCount + 1;
                        matchedResults = new ArrayList<>();
                        matchedResults.add(forwardBarcodeSet.get(i));
                        matchedResults.add(String.valueOf(fMatches.group().length()));
                        matchedResults.add(null);
                        matchedResults.add(null);
                        return matchedResults;
                    } else if (fMatches.find() && rMatches.find()) {
                        primerMatchCount = primerMatchCount + 1;
                        matchedResults = new ArrayList<>();
                        matchedResults.add(forwardBarcodeSet.get(i));
                        matchedResults.add(String.valueOf(fMatches.group().length()));
                        matchedResults.add(reverseBarcodeSet.get(i));
                        matchedResults.add(String.valueOf(rMatches.group().length()));
                        return matchedResults;
                    }
                }
            }
//            System.err.println("false" + i);
        }
        matchedResults.add(null);
        matchedResults.add(null);
        matchedResults.add(null);
        matchedResults.add(null);
        return matchedResults;
    }
}
