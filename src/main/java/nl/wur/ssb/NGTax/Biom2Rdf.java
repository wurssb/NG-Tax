package nl.wur.ssb.NGTax;

import com.google.gson.internal.LinkedTreeMap;
import life.gbol.domain.*;
import nl.wur.ssb.RDFSimpleCon.RDFFormat;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.Resource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.purl.ontology.bibo.domain.Document;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static nl.wur.ssb.NGTax.CommandOptions.CommandOptionsNGTax.getMD5;
import static org.apache.jena.rdf.model.ResourceFactory.createProperty;

/**
 * Convert BIOM file into RDF format.
 */

public class Biom2Rdf {

    private static String rootIRI = "http://gbol.life/0.1/NG-Tax/";
    private static Domain domain;
    static final Logger logger = LogManager.getLogger(Biom2Rdf.class);

    private static Map<Integer, Rank> lineageLookup = new HashMap<>() {{
        put(1, Rank.RankKingdom);
        put(2, Rank.RankPhylum);
        put(3, Rank.RankClass);
        put(4, Rank.RankOrder);
        put(5, Rank.RankFamily);
        put(6, Rank.RankGenus);
        put(7, Rank.RankSpecies);
    }};

    /**
     * Directly convert an existing BIOM file(s) into RDF format.
     *
     * @param biomPath           -- List of biom file(s)
     * @param turtleFileLocation -- List of RDF output location(s)
     * @throws Exception
     */
    public static void biomDirectParser(List<String> biomPath, List<String> turtleFileLocation) throws Exception {

        String biomFile[] = null;
        String outputFile[] = null;

        for (String bFile : biomPath) {
            biomFile = bFile.split(" ");
        }

        for (String oFile : turtleFileLocation) {
            outputFile = oFile.split(" ");
        }

        if (biomFile.length != outputFile.length) {
            throw new Exception("Number of BIOM file(s) and output location(s) does not match.");
        } else {
            for (int i = 0; i < biomFile.length; i++) {
                new NGTaxResult();
                File inputFile = new File(biomFile[i]);
                String data = new String(java.nio.file.Files.readAllBytes(Paths.get(inputFile.getAbsolutePath())));
                NGTaxResult res = NGTaxResult.fromJSon(data);

                if (res == null) {
                    throw new Exception("Input file is NOT in the ngtax biom json format (Check: http://biom-format.org).");
                }

                String fileLocation = outputFile[i];
                if (res.provData != null) {
                    logger.info("NGTax biom format file detected.");
                    ngTaxBiomParser(res, fileLocation);
                } else {
                    logger.info("Traditional biom format file detected.");
                    traditionalBiomParser(res, fileLocation);
                }
            }
        }
    }


    /**
     * Convert BIOM file result from NGTax into RDF format (optional to the program)
     *
     * @param res                -- NGTax results object
     * @param turtleFileLocation -- RDF output location
     * @throws Exception
     */
    public static void ngTaxBiomParser(NGTaxResult res, String turtleFileLocation) throws Exception {

        // Saves memory and maybe speed on the bigger files
        domain = new Domain("file://" + Files.createTempDirectory("NGTAX"));
        domain.getRDFSimpleCon().setNsPrefix("ssb", "http://ssb.wur.nl/0.1/");
        domain.getRDFSimpleCon().setNsPrefix("gbol", "http://gbol.life/0.1/");

        // Set unique id for IRI
        String uniqID;

        String id = res.provData.args.forwardReadLength + "_" + res.provData.args.reverseReadLength;

        if (res.provData.args.project != null) {
            uniqID = res.provData.args.project;
        } else {
            logger.info(res.provData.args.biomFile);
            uniqID = res.provData.args.biomFile.split(".biom")[0];
            if (uniqID.contains("/")) {
                String temp[] = uniqID.split("/");
                uniqID = temp[temp.length - 1];
            }
        }

        // Primers and reference database?
        // String dbhex = DigestUtils.sha256Hex(res.provData.args.refdb + res.provData.args.forwardPrimer + res.provData.args.reversePrimer);
        rootIRI = rootIRI + UUID.randomUUID();

        logger.info("Converting " + res.provData.args.biomFile + " to RDF format using root iri " + rootIRI);

        // Setting ngtax object
        ProvenanceAnnotation commandArgsRDF = creatingCommandArgs(res);

        // Initial Library RDF Entry and create data table
        life.gbol.domain.Library libraryRDF;
        HashMap<String, Library> libraryData = createLibraryInfo(res);

        // Initial Sample RDF Entry
        life.gbol.domain.Sample sampleRDF;

        // Initial Rejected as chimera ASV RDF Entry
        RejectedAsChimera rejectedAsChimeraRDF;

        // Create ASV information.
        LinkedHashMap<String, ASV> otuData = createOTUInfo(res.provData.asvList);

        // Create ASV rejected information.
        LinkedHashMap<String, ASV> otuRejectData = createOTUInfo(res.provData.rejectedASVList);

        // Initial Assigned taxon RDF Entry and create data table
        life.gbol.domain.Taxon assignedTaxonRDF;

        // Initial Library RDF Entry to store hits and best taxon
//        ASVAssignment asvAssignment;

        // Initial Hits taxon RDF Entry and create data table
        life.gbol.domain.Taxon hitsTaxonRDF;

        // Initial Best taxon RDF Entry and create data table
        life.gbol.domain.Taxon bestTaxonRDF;

        // Create random unique identifier
        String randomUniqueID = UUID.randomUUID().toString();

        PCRPrimer fprimerSeq = domain.make(PCRPrimer.class, "http://ssb.wur.nl/NG-Tax/0.1/sequence/" + DigestUtils.sha384Hex(res.provData.args.forwardPrimer));
        fprimerSeq.setSequence(res.provData.args.forwardPrimer);
        fprimerSeq.setSha384(DigestUtils.sha384Hex(res.provData.args.forwardPrimer));

        PCRPrimerSet primerSet;
        if (res.provData.args.reversePrimer.length() > 0) {
            PCRPrimer rprimerSeq = domain.make(PCRPrimer.class, "http://ssb.wur.nl/NG-Tax/0.1/sequence/" + DigestUtils.sha384Hex(res.provData.args.reversePrimer));
            rprimerSeq.setSequence(res.provData.args.reversePrimer);
            rprimerSeq.setSha384(DigestUtils.sha384Hex(res.provData.args.reversePrimer));

            //  Setting up the primer information
            primerSet = domain.make(PCRPrimerSet.class, rootIRI + "/primerset/" + fprimerSeq.getSha384() + "/" + rprimerSeq.getSha384());
            fprimerSeq.setLength((long) fprimerSeq.getSequence().length());
            rprimerSeq.setLength((long) rprimerSeq.getSequence().length());
            primerSet.setForwardPrimer(fprimerSeq);
            primerSet.setReversePrimer(rprimerSeq);
        } else {
            //  Setting up the primer information
            primerSet = domain.make(PCRPrimerSet.class, rootIRI + "/primerset/" + fprimerSeq.getSha384());
            fprimerSeq.setLength((long) fprimerSeq.getSequence().length());
            primerSet.setForwardPrimer(fprimerSeq);
        }

        Provenance prov = domain.make(Provenance.class, rootIRI + "/provenance");

        logger.info("Setting annotation activities");

        commandArgsRDF.getResource().addLiteral(createProperty("http://gbol.life/0.1/id"), res.id);
        commandArgsRDF.getResource().addLiteral(createProperty("http://gbol.life/0.1/version"), res.version);
        prov.setAnnotation(commandArgsRDF);

        AnnotationResult origin = domain.make(AnnotationResult.class, prov.getResource().getURI() + "/origin");
        AnnotationActivity annotationActivity = domain.make(AnnotationActivity.class, rootIRI + "/AnnotationActivity");
        annotationActivity.setStartedAtTime(res.startTime);
        annotationActivity.setEndedAtTime(LocalDateTime.now());
        origin.setWasGeneratedBy(annotationActivity);

        AnnotationSoftware annotationSoftware = domain.make(AnnotationSoftware.class, "http://gbol.life/0.1/NGTax" + res.version);
        annotationSoftware.setCodeRepository("https://gitlab.com/wurssb/NG-Tax");

        // When doing direct conversion we have no commit or version information
        if (res.commit != null)
            annotationSoftware.setCommitId(res.commit);
        if (res.version == null) {
            res.version = "0.0";
        }
        annotationSoftware.setVersion(res.version);
        annotationSoftware.setHomepage("http://wurssb.gitlab.io/ngtax/");
        annotationSoftware.setName("NG-Tax");
        origin.setWasAttributedTo(annotationSoftware);
        logger.info("Setting origin");
        prov.setOrigin(origin);

        /*
         * Set up library information
         */

        Iterator libIter = libraryData.entrySet().iterator();

        while (libIter.hasNext()) {

            Map.Entry libraryMap = (Map.Entry) libIter.next();

            logger.info("Library " + libraryMap.getKey());

            nl.wur.ssb.NGTax.Library libraryInfo = (nl.wur.ssb.NGTax.Library) libraryMap.getValue();
            libraryRDF = domain.make(life.gbol.domain.Library.class, rootIRI + "/Library/" + libraryInfo.index);
            libraryRDF.setLibraryNum(libraryInfo.index);
            libraryRDF.setFFile(new File(libraryInfo.forwardFile).getName());
            if (libraryInfo.reverseFile != null) {
                libraryRDF.setRFile(new File(libraryInfo.reverseFile).getName());
            } else {
                libraryRDF.setRFile("null");
            }
            libraryRDF.setTotalReads(libraryInfo.totalReads);
            libraryRDF.setPrimerHitsAccepted(libraryInfo.primerHitsAccepted);
            libraryRDF.setPrimerHitsAcceptedRatio((float) libraryInfo.primerHitsAcceptedRatio);
            libraryRDF.setBarcodeHitsAccepted(libraryInfo.barcodeHitsAccepted);
            libraryRDF.setBarcodeHitsAcceptedRatio((float) libraryInfo.barcodeHitsAcceptedRatio);
            libraryRDF.setAccpetedSameBarcodeRatio((float) libraryInfo.acceptedSameBarCodeRatio);
            libraryRDF.setFBarcodeLength(libraryInfo.fBarCodeLength);
            libraryRDF.setRBarcodeLength(libraryInfo.rBarCodeLength);

            try {
                libraryRDF.setFBarcodeFile(new File(libraryInfo.forwardBarcodeFile).getName());
                libraryRDF.setRBarcodeFile(new File(libraryInfo.reverseBarcodeFile).getName());
            } catch (NullPointerException e) {

            }
            libraryRDF.setProvenance(prov);
        }

        // iterate over the Samples
        int sampleCount = 1;
        for ( Sample sample : res.provData.samples ) {
            String sampleCountString = "(" + sampleCount + "/" + res.provData.samples.size() + ")";
            logger.info(sampleCountString + " >> Creating RDF database for sample name: " + sample.sampleName);

            /*
             * Set up sample information
             */
            libraryRDF = domain.make(life.gbol.domain.Library.class, rootIRI + "/Library/" + sample.libraryNum);
            sampleRDF = domain.make(life.gbol.domain.Sample.class, rootIRI + "/Library/" + sample.libraryNum + "/Sample/" + sample.sampleName);

            sampleRDF.setName(sample.sampleName);
            sampleRDF.setPCRPrimers(primerSet);

            if (!(sample.fBarcode.equals(""))) {
                // Forward barcode
                Barcode fBarcodeSeq = domain.make(Barcode.class, "http://ssb.wur.nl/NG-Tax/0.1/sequence/" + DigestUtils.sha384Hex(sample.fBarcode));
                fBarcodeSeq.setSequence(sample.fBarcode);
                fBarcodeSeq.setSha384(DigestUtils.sha384Hex(sample.fBarcode));
                fBarcodeSeq.setLength((long) sample.fBarcode.length());

                // Reverse barcode
                Barcode rBarcodeSeq = domain.make(Barcode.class, "http://ssb.wur.nl/NG-Tax/0.1/sequence/" + DigestUtils.sha384Hex(sample.rBarcode));
                rBarcodeSeq.setSequence(sample.rBarcode);
                rBarcodeSeq.setSha384(DigestUtils.sha384Hex(sample.rBarcode));
                rBarcodeSeq.setLength((long) sample.rBarcode.length());

                // Coupling the barcodes to the sample through a set
                BarcodeSet barcodes = domain.make(life.gbol.domain.BarcodeSet.class, "http://ssb.wur.nl/NG-Tax/0.1/barcodeset/" + fBarcodeSeq.getSha384() + "/" + rBarcodeSeq.getSha384());
                barcodes.setForwardBarcode(fBarcodeSeq);
                barcodes.setReverseBarcode(rBarcodeSeq);
                sampleRDF.setBarcodes(barcodes);
            }

//            sampleRDF.setFastQFileType(sample.fastQFiles);
            if (sample.mappingInfo != null) {
                String mappingTypeURL = "http://gbol.life/0.1/MappingFile";
                String mappingURL = sampleRDF.getResource().getURI() + "/mappingInfo";
                sampleRDF.addMetadata(mappingURL);
                domain.getRDFSimpleCon().add(mappingURL, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", mappingTypeURL);
                // System.err.println(StringUtils.join(sample.mappingInfo.keySet(), " "));
                for ( String key : sample.mappingInfo.keySet() ) {
                    domain.getRDFSimpleCon().addLit(mappingURL, "http://gbol.life/0.1/" + key, sample.mappingInfo.get(key));
                }
            }

            String totalCountsTypeURL = "http://gbol.life/0.1/TotalCounts";
            String totalCountsURL = sampleRDF.getResource().getURI() + "/totalCounts";
            sampleRDF.addMetadata(totalCountsURL);
            domain.getRDFSimpleCon().add(totalCountsURL, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", totalCountsTypeURL);
            domain.getRDFSimpleCon().addLit(totalCountsURL, "http://gbol.life/0.1/totalCounts", String.valueOf(sample.totalCounts));

            String numAcceptedReadsBeforeChimeraTypeURL = "http://gbol.life/0.1/NumAcceptedReadsBeforeChimera";
            String numAcceptedReadsBeforeChimeraURL = sampleRDF.getResource().getURI() + "/numAcceptedReadsBeforeChimera";
            sampleRDF.addMetadata(numAcceptedReadsBeforeChimeraURL);
            domain.getRDFSimpleCon().add(numAcceptedReadsBeforeChimeraURL, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", numAcceptedReadsBeforeChimeraTypeURL);
            domain.getRDFSimpleCon().addLit(numAcceptedReadsBeforeChimeraURL, "http://gbol.life/0.1/numAcceptedReadsBeforeChimera", String.valueOf(sample.numAcceptedReadsBeforeChimera));

            String percentAcceptedReadsBeforeChimeraTypeURL = "http://gbol.life/0.1/PercentAcceptedReadsBeforeChimera";
            String percentAcceptedReadsBeforeChimeraURL = sampleRDF.getResource().getURI() + "/percentAcceptedReadsBeforeChimera";
            sampleRDF.addMetadata(percentAcceptedReadsBeforeChimeraURL);
            domain.getRDFSimpleCon().add(percentAcceptedReadsBeforeChimeraURL, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", percentAcceptedReadsBeforeChimeraTypeURL);
            domain.getRDFSimpleCon().addLit(percentAcceptedReadsBeforeChimeraURL, "http://gbol.life/0.1/percentAcceptedReadsBeforeChimera", String.valueOf(sample.percentAcceptedReadsBeforeChimera));

            String numReadsChimeraTypeURL = "http://gbol.life/0.1/NumReadsChimera";
            String numReadsChimeraURL = sampleRDF.getResource().getURI() + "/numReadsChimera";
            sampleRDF.addMetadata(numReadsChimeraURL);
            domain.getRDFSimpleCon().add(numReadsChimeraURL, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", numReadsChimeraTypeURL);
            domain.getRDFSimpleCon().addLit(numReadsChimeraURL, "http://gbol.life/0.1/numReadsChimera", String.valueOf(sample.numReadsChimera));

            String numAcceptedReadsAfterErrorCorrectionTypeURL = "http://gbol.life/0.1/NumAcceptedReadsAfterErrorCorrection";
            String numAcceptedReadsAfterErrorCorrectionURL = sampleRDF.getResource().getURI() + "/numAcceptedReadsAfterErrorCorrection";
            sampleRDF.addMetadata(numAcceptedReadsAfterErrorCorrectionURL);
            domain.getRDFSimpleCon().add(numAcceptedReadsAfterErrorCorrectionURL, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", numAcceptedReadsAfterErrorCorrectionTypeURL);
            domain.getRDFSimpleCon().addLit(numAcceptedReadsAfterErrorCorrectionURL, "http://gbol.life/0.1/numAcceptedReadsAfterErrorCorrection", String.valueOf(sample.numAcceptedReadsAfterErrorCorrection));

            String percentAcceptedReadsAfterErrorCorrectionTypeURL = "http://gbol.life/0.1/PercentAcceptedReadsAfterErrorCorrection";
            String percentAcceptedReadsAfterErrorCorrectionURL = sampleRDF.getResource().getURI() + "/percentAcceptedReadsAfterErrorCorrection";
            sampleRDF.addMetadata(percentAcceptedReadsAfterErrorCorrectionURL);
            domain.getRDFSimpleCon().add(percentAcceptedReadsAfterErrorCorrectionURL, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", percentAcceptedReadsAfterErrorCorrectionTypeURL);
            domain.getRDFSimpleCon().addLit(percentAcceptedReadsAfterErrorCorrectionURL, "http://gbol.life/0.1/percentAcceptedReadsAfterErrorCorrection", String.valueOf(sample.percentAcceptedReadsAfterErrorCorrection));

            String numAcceptedOtuBeforeChimeraTypeURL = "http://gbol.life/0.1/NumAcceptedOtuBeforeChimera";
            String numAcceptedOtuBeforeChimeraURL = sampleRDF.getResource().getURI() + "/numAcceptedOtuBeforeChimera";
            sampleRDF.addMetadata(numAcceptedOtuBeforeChimeraURL);
            domain.getRDFSimpleCon().add(numAcceptedOtuBeforeChimeraURL, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", numAcceptedOtuBeforeChimeraTypeURL);
            domain.getRDFSimpleCon().addLit(numAcceptedOtuBeforeChimeraURL, "http://gbol.life/0.1/numAcceptedOtuBeforeChimera", String.valueOf(sample.numAcceptedOtuBeforeChimera));

            String numRejectedOtuTypeURL = "http://gbol.life/0.1/NumRejectedOtu";
            String numRejectedOtuURL = sampleRDF.getResource().getURI() + "/numRejectedOtu";
            sampleRDF.addMetadata(numRejectedOtuURL);
            domain.getRDFSimpleCon().add(numRejectedOtuURL, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", numRejectedOtuTypeURL);
            domain.getRDFSimpleCon().addLit(numRejectedOtuURL, "http://gbol.life/0.1/numRejectedOtu", String.valueOf(sample.numRejectedOtu));

            String evennessTypeURL = "http://gbol.life/0.1/Evenness";
            String evennessURL = sampleRDF.getResource().getURI() + "/evenness";
            sampleRDF.addMetadata(evennessURL);
            domain.getRDFSimpleCon().add(evennessURL, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", evennessTypeURL);
            domain.getRDFSimpleCon().addLit(evennessURL, "http://gbol.life/0.1/evenness", String.valueOf(sample.evenness));

//            sampleRDF.setTotalCounts(sample.totalCounts);
//            sampleRDF.setPercentAcceptedReads((float) sample.percentAcceptedReads);
//            sampleRDF.setNumAcceptedOtuBeforeChimera(sample.numAcceptedOtuBeforeChimera);
//            sampleRDF.setNumRejectedOtu(sample.numRejectedOtu);


            // For each sample, get the ASV information : ids and sequences
            for (SampleASV sampleOTU : sample.otus) {
                ASVSet asvSet = domain.make(ASVSet.class, sampleRDF.getResource().getURI() + "/ASV/" + sampleOTU.masterOtuId);

                ASVSequence fASV = domain.make(ASVSequence.class, "http://ssb.wur.nl/NG-Tax/0.1/sequence/" + DigestUtils.sha384Hex(sampleOTU.forwardSequence));
                fASV.setSequence(sampleOTU.forwardSequence);
                fASV.setLength((long) sampleOTU.forwardSequence.length());
                fASV.setSha384(DigestUtils.sha384Hex(sampleOTU.forwardSequence));

                if (sampleOTU.reverseSequence.length() > 0) {
//                    App.logger.info(sampleOTU.reverseSequence);
                    ASVSequence rASV = domain.make(ASVSequence.class, "http://ssb.wur.nl/NG-Tax/0.1/sequence/" + DigestUtils.sha384Hex(sampleOTU.reverseSequence));
                    rASV.setSequence(sampleOTU.reverseSequence);
                    rASV.setLength((long) sampleOTU.reverseSequence.length());
                    rASV.setSha384(DigestUtils.sha384Hex(sampleOTU.reverseSequence));
                    asvSet.setReverseASV(rASV);
                }

                asvSet.setForwardASV(fASV);
                asvSet.setMasterASVId(sampleOTU.masterOtuId);
                asvSet.setReadCount(sampleOTU.readCount);
                asvSet.setRatio((float) sampleOTU.ratio);
                asvSet.setClusteredReadCount(sampleOTU.clusteredReadCount);

                sampleRDF.addAsv(asvSet);

                taxonAssignment(otuData, sampleOTU, asvSet, sampleRDF, prov);
            }

            /*
             * Set up rejected ASV information
             */
            logger.debug("Rejected creation");
            LinkedList<SampleASV> rejectedOTU = sample.rejectedOtus;
            if (rejectedOTU != null) {
                for (int i = 0; i < rejectedOTU.size(); i++) {

                    RejectedASV rejectedASVRDF = domain.make(RejectedASV.class, sampleRDF.getResource().getURI() + "/RejectedASV/" + i);

                    ASVSequence rejectedASVSeq = domain.make(ASVSequence.class, "http://ssb.wur.nl/NG-Tax/0.1/sequence/" + DigestUtils.sha384Hex(rejectedOTU.get(i).forwardSequence));
                    rejectedASVSeq.setSequence(rejectedOTU.get(i).forwardSequence);
                    rejectedASVSeq.setSha384(DigestUtils.sha384Hex(rejectedOTU.get(i).forwardSequence));
                    rejectedASVSeq.setLength((long) rejectedASVSeq.getSequence().length());
                    rejectedASVRDF.setForwardASV(rejectedASVSeq);
                    if (rejectedOTU.get(i).reverseSequence.length() > 0) {
                        rejectedASVSeq = domain.make(ASVSequence.class, "http://ssb.wur.nl/NG-Tax/0.1/sequence/" + DigestUtils.sha384Hex(rejectedOTU.get(i).reverseSequence));
                        rejectedASVSeq.setSequence(rejectedOTU.get(i).reverseSequence);
                        rejectedASVSeq.setSha384(DigestUtils.sha384Hex(rejectedOTU.get(i).reverseSequence));
                        rejectedASVSeq.setLength((long) rejectedASVSeq.getSequence().length());
                        rejectedASVRDF.setReverseASV(rejectedASVSeq);
                    }
                    rejectedASVRDF.setClusteredReadCount(rejectedOTU.get(i).clusteredReadCount);
                    rejectedASVRDF.setRatio((float) rejectedOTU.get(i).ratio);
                    rejectedASVRDF.setReadCount(rejectedOTU.get(i).readCount);
                    rejectedASVRDF.setMasterASVId(rejectedOTU.get(i).masterOtuId);
                    sampleRDF.addAsv(rejectedASVRDF);
                    taxonAssignment(otuRejectData, rejectedOTU.get(i), rejectedASVRDF, sampleRDF, prov);
                }
            }


            /*
             * Set up rejected as chimera information
             */
            LinkedList<ChimeraRejection> rejectedAsChimera = sample.rejectedAsChimera;
            if (rejectedAsChimera != null) {
                for (int i = 0; i < rejectedAsChimera.size(); i++) {
                    rejectedAsChimeraRDF = domain.make(RejectedAsChimera.class, sampleRDF.getResource().getURI() + "/RejectedAsChimera/" + i);

                    ASVSequence rejectedAsChimeraSeq = domain.make(ASVSequence.class, "http://ssb.wur.nl/NG-Tax/0.1/sequence/" + DigestUtils.sha384Hex(rejectedAsChimera.get(i).otu.forwardSequence));
                    rejectedAsChimeraSeq.setSequence(rejectedAsChimera.get(i).otu.forwardSequence);
                    rejectedAsChimeraSeq.setSha384(DigestUtils.sha384Hex(rejectedAsChimera.get(i).otu.forwardSequence));
                    rejectedAsChimeraSeq.setLength((long) rejectedAsChimeraSeq.getSequence().length());
                    rejectedAsChimeraRDF.setForwardASV(rejectedAsChimeraSeq);

                    rejectedAsChimeraSeq = domain.make(ASVSequence.class, "http://ssb.wur.nl/NG-Tax/0.1/sequence/" + DigestUtils.sha384Hex(rejectedAsChimera.get(i).otu.reverseSequence));
                    rejectedAsChimeraSeq.setSequence(rejectedAsChimera.get(i).otu.reverseSequence);
                    rejectedAsChimeraSeq.setSha384(DigestUtils.sha384Hex(rejectedAsChimera.get(i).otu.reverseSequence));
                    rejectedAsChimeraSeq.setLength((long) rejectedAsChimeraSeq.getSequence().length());
                    rejectedAsChimeraRDF.setReverseASV(rejectedAsChimeraSeq);

                    rejectedAsChimeraRDF.setClusteredReadCount(rejectedAsChimera.get(i).otu.clusteredReadCount);
                    rejectedAsChimeraRDF.setRatio((float) rejectedAsChimera.get(i).otu.ratio);
                    rejectedAsChimeraRDF.setReadCount(rejectedAsChimera.get(i).otu.readCount);
                    rejectedAsChimeraRDF.setPForwardId(rejectedAsChimera.get(i).pForwardId);
                    rejectedAsChimeraRDF.setPForwardRatio((float) rejectedAsChimera.get(i).pForwardRatio);
                    rejectedAsChimeraRDF.setPReverseId(rejectedAsChimera.get(i).pReverseId);
                    rejectedAsChimeraRDF.setPReverseRatio((float) rejectedAsChimera.get(i).pForwardRatio);
                    rejectedAsChimeraRDF.setMasterASVId("null");
                    sampleRDF.addAsv(rejectedAsChimeraRDF);
                }
            }
            sampleCount = sampleCount + 1;

            libraryRDF.addSample(sampleRDF);
        }

        /*
         * Write RDF Entry to file
         */
        writeOutputRDFFile(domain, turtleFileLocation);
    }

    private static ProvenanceAnnotation creatingCommandArgs(NGTaxResult res) throws Exception {
        ProvenanceAnnotation commandArgsRDF = domain.make(ProvenanceAnnotation.class, rootIRI + "/provenance/ngtax/" + res.commit);
        Document document = domain.make(Document.class, "https://doi.org/10.3389/fgene.2019.01366");
        commandArgsRDF.setReference(document);
        Resource resource = commandArgsRDF.getResource();
        if (res.provData.args.refdb != null) {
            resource.addLiteral(createProperty("http://gbol.life/0.1/refdb"), new File(res.provData.args.refdb).getName());
        }

        resource.addLiteral(createProperty("http://gbol.life/0.1/forwardReadLength"), res.provData.args.forwardReadLength);
        resource.addLiteral(createProperty("http://gbol.life/0.1/reverseReadLength"), res.provData.args.reverseReadLength);
        resource.addLiteral(createProperty("http://gbol.life/0.1/mapFile"), new File(res.provData.args.mapFile).getName());
        resource.addLiteral(createProperty("http://gbol.life/0.1/minPerT"), res.provData.args.minPerT);
        resource.addLiteral(createProperty("http://gbol.life/0.1/identLvl"), res.provData.args.identLvl);
        resource.addLiteral(createProperty("http://gbol.life/0.1/errorCorr"), res.provData.args.errorCorr);
        resource.addLiteral(createProperty("http://gbol.life/0.1/chimeraRatio"), res.provData.args.chimeraRatio);
        resource.addLiteral(createProperty("http://gbol.life/0.1/classifyRatio"), res.provData.args.classifyRatio);
        resource.addLiteral(createProperty("http://gbol.life/0.1/markIfMoreThen1"), res.provData.args.markIfMoreThen1);
        resource.addLiteral(createProperty("http://gbol.life/0.1/biomFile"), new File(res.provData.args.biomFile).getName());

        if (res.provData.args.subfragment != null) {
            logger.info("Setting subfragment to " + res.provData.args.subfragment);
            resource.addLiteral(createProperty("http://gbol.life/0.1/subfragment"), res.provData.args.subfragment);
        }

        if (res.provData.args.email != null) {
            resource.addLiteral(createProperty("http://gbol.life/0.1/email"), res.provData.args.email);
        }

        if (res.provData.args.project != null) {
            resource.addLiteral(createProperty("http://gbol.life/0.1/project"), res.provData.args.project);
        }

        if (res.provData.args.folder != null) {
            resource.addLiteral(createProperty("http://gbol.life/0.1/folder"), res.provData.args.folder);
        }

        if (res.provData.args.turtleFile != null) {
            resource.addLiteral(createProperty("http://gbol.life/0.1/turtle"), new File(res.provData.args.turtleFile).getName());
        }

        resource.addLiteral(createProperty("http://gbol.life/0.1/logFile"), new File(res.provData.args.log).getName());

        resource.addLiteral(createProperty("http://gbol.life/0.1/nomismatch"), res.provData.args.nomismatch);
        resource.addLiteral(createProperty("http://gbol.life/0.1/shannon"), res.provData.args.shannon);
        resource.addLiteral(createProperty("http://gbol.life/0.1/minOTUsizeT"), res.provData.args.minOTUsizeT);

        resource.addLiteral(createProperty("http://gbol.life/0.1/maxChimeraDistF"), res.provData.args.maxChemeraDistF);
        resource.addLiteral(createProperty("http://gbol.life/0.1/maxChimeraDistR"), res.provData.args.maxChemeraDistR); //*** TODO Che Chi mera
        resource.addLiteral(createProperty("http://gbol.life/0.1/maxClusteringMismatchCount"), res.provData.args.maxClusteringMismatchCount);
        resource.addLiteral(createProperty("http://gbol.life/0.1/identity97MismatchCount"), res.provData.args.identity97MismatchCount);
        resource.addLiteral(createProperty("http://gbol.life/0.1/identity95MismatchCount"), res.provData.args.identity95MismatchCount);
        resource.addLiteral(createProperty("http://gbol.life/0.1/identity92MismatchCount"), res.provData.args.identity92MismatchCount);
        resource.addLiteral(createProperty("http://gbol.life/0.1/identity90MismatchCount"), res.provData.args.identity90MismatchCount);
        resource.addLiteral(createProperty("http://gbol.life/0.1/identity85MismatchCount"), res.provData.args.identity85MismatchCount);
        resource.addLiteral(createProperty("http://gbol.life/0.1/fPrimerLength"), res.provData.args.forwardPrimerLength);
        resource.addLiteral(createProperty("http://gbol.life/0.1/rPrimerLength"), res.provData.args.reversePrimerLength);
        resource.addLiteral(createProperty("http://gbol.life/0.1/generatedBy"), res.generated_by);
        resource.addLiteral(createProperty("http://gbol.life/0.1/headerType"), res.type);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formatDatetime = res.date.format(formatter);
        resource.addLiteral(createProperty("http://gbol.life/0.1/date"), formatDatetime);
        resource.addLiteral(createProperty("http://gbol.life/0.1/format"), res.format);
        resource.addLiteral(createProperty("http://gbol.life/0.1/formatURL"), res.format_url);
        resource.addLiteral(createProperty("http://gbol.life/0.1/matrixType"), res.matrix_type);
        resource.addLiteral(createProperty("http://gbol.life/0.1/matrixElementType"), res.matrix_element_type);

        if (res.provData.args.fastQSet != null) {
            for (String fastqFiles : res.provData.args.fastQSet) {
                for (String fastqFile : fastqFiles.split(",")) {
                    if (res.provData.args.fileMetadataHashMap != null) {
                        FileMetadata fileMetadata = res.provData.args.fileMetadataHashMap.get(new File(fastqFile).getAbsolutePath());
                        // Calculate SHA384 of file?, need consistent URIs for file objects
                        String sha256 = fileMetadata.getSha256();
                        logger.info("Checksum of file: " + sha256);
                        // domain.getRDFSimpleCon().setNsPrefix("ni","ni");
                        LocalSequenceFile localSequenceFile = domain.make(LocalSequenceFile.class, "ni:///sha-256;" + sha256);
                        logger.info("Local sequence file created");
                        localSequenceFile.setFileType(FileType.FASTQ);
                        localSequenceFile.setMd5(fileMetadata.getMd5());
                        localSequenceFile.setLocalFileName(new File(fastqFile).getName());
                        localSequenceFile.setLocalFilePath(new File(fastqFile).getAbsolutePath());
                        logger.info("Obtaining local sequence information");
                        // getSequenceFileInfo(localSequenceFile, new File(fastqFile));
                        localSequenceFile.setBases(fileMetadata.getBases());
                        localSequenceFile.setReads(fileMetadata.getReads());
                        localSequenceFile.setReadLength(fileMetadata.getMaxReadLength());
                        logger.info("Setting fastq set information");
                        resource.addLiteral(createProperty("http://gbol.life/0.1/fastQSet"), localSequenceFile);
                    } else {
                        logger.info("No file metadata present");
                    }
                }
            }
        } else {
            // Fake dataset for reference mocks
            LocalSequenceFile localSequenceFile = domain.make(LocalSequenceFile.class, "ni:///sha-256;" + "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
            logger.info("Local sequence file created");
            localSequenceFile.setFileType(FileType.Other);
            localSequenceFile.setMd5("d41d8cd98f00b204e9800998ecf8427e");
            localSequenceFile.setLocalFileName(new File("mock").getName());
            localSequenceFile.setLocalFilePath(new File("/").getAbsolutePath());
            logger.info("Obtaining local sequence information");
            // getSequenceFileInfo(localSequenceFile, new File(fastqFile));
            localSequenceFile.setBases(-1L);
            localSequenceFile.setReads(-1L);
            localSequenceFile.setReadLength(-1L);
            logger.info("Setting fastq set information");
            resource.addLiteral(createProperty("http://gbol.life/0.1/fastQSet"), localSequenceFile);
        }
        return commandArgsRDF;
    }

    private static void taxonAssignment(LinkedHashMap<String, ASV> otuData, SampleASV sampleOTU, ASVSet asvSet, life.gbol.domain.Sample sampleRDF, Provenance provenance) {
        if (otuData.get(sampleOTU.masterOtuId) != null) {
            asvSet.setUsedTaxonLevel(otuData.get(sampleOTU.masterOtuId).usedTaxonLevel);
            /*
             * Set up hits taxon information
             */
            if (otuData.get(sampleOTU.masterOtuId).usedMisMatchLevel != null) {
                HashMap<String, ASVMatchStats>[] hitsTaxonMap = otuData.get(sampleOTU.masterOtuId).usedMisMatchLevel.hitsTaxon;
                ASVAssignment asvAssignment;
                for (int i = 0; i < hitsTaxonMap.length; i++) {

                    for (String key : hitsTaxonMap[i].keySet()) {

                        ASVMatchStats hitsTaxonInfo = hitsTaxonMap[i].get(key);
                        String taxName = cleanBracketInEmptyTaxon(hitsTaxonInfo.tax);
                        taxName = cleanSquareBracketInTaxon(taxName);
                        String taxNameMD5 = getMD5(taxName);
                        asvAssignment = domain.make(ASVAssignment.class, rootIRI + "/ASVAssignment/Level/" + i + "/" + taxNameMD5);

                        life.gbol.domain.Taxon taxonRDF = domain.make(life.gbol.domain.Taxon.class, "http://gbol.life/0.1/Lineage/" + taxNameMD5);
                        taxonRDF.setTaxonName(taxName);
                        taxonRDF.setTaxonRank(lineageLookup.get(StringUtils.countMatches(taxName, ";") + 1));
                        taxonRDF.setProvenance(provenance);

                        asvAssignment.setNumberHits(hitsTaxonInfo.taxonHitCount);
                        asvAssignment.setRatio((float) hitsTaxonInfo.ratio);
                        asvAssignment.setLevelCount(hitsTaxonInfo.mismatchCount);
                        asvAssignment.setType(TaxonAssignmentType.HitsTaxon);
                        asvAssignment.setTaxon(taxonRDF);
                        asvSet.addAsvAssignment(asvAssignment);
                    }
                }

                /*
                 * Set up best taxon information
                 */
                ASVMatchStats[] bestTaxonMap = otuData.get(sampleOTU.masterOtuId).usedMisMatchLevel.bestTaxon;
                for (int i = 0; i < bestTaxonMap.length; i++) {
                    String taxName = cleanBracketInEmptyTaxon(bestTaxonMap[i].tax);
                    taxName = cleanSquareBracketInTaxon(taxName);
                    String taxNameMD5 = getMD5(taxName);
                    asvAssignment = domain.make(ASVAssignment.class, rootIRI + "/ASVAssignment/Level/" + i + "/" + taxNameMD5);

                    life.gbol.domain.Taxon taxonRDF = domain.make(life.gbol.domain.Taxon.class, "http://gbol.life/0.1/" + taxNameMD5);
                    taxonRDF.setTaxonName(taxName);
                    taxonRDF.setTaxonRank(lineageLookup.get(StringUtils.countMatches(taxName, ";") + 1));
                    taxonRDF.setProvenance(provenance);

                    asvAssignment.setNumberHits(bestTaxonMap[i].taxonHitCount);
                    asvAssignment.setRatio((float) bestTaxonMap[i].ratio);
                    asvAssignment.setLevelCount(bestTaxonMap[i].mismatchCount);
                    asvAssignment.setType(TaxonAssignmentType.BestTaxon);
                    asvAssignment.setTaxon(taxonRDF);
                    asvSet.addAsvAssignment(asvAssignment);

                }

                /*
                 * Set up assigned taxon information
                 */
                ASVMatchStats assignedTaxonInfo = otuData.get(sampleOTU.masterOtuId).assignedTaxon;

                if (assignedTaxonInfo != null) {
                    String taxName = cleanBracketInEmptyTaxon(assignedTaxonInfo.tax);
                    String taxNameMD5 = getMD5(taxName);
                    taxName = cleanSquareBracketInTaxon(taxName);
                    life.gbol.domain.Taxon assignedTaxonRDF = domain.make(life.gbol.domain.Taxon.class, "http://gbol.life/0.1/lineage/" + taxNameMD5);
                    assignedTaxonRDF.setTaxonName(taxName);
                    assignedTaxonRDF.setProvenance(provenance);
                    assignedTaxonRDF.setTaxonRank(lineageLookup.get(StringUtils.countMatches(taxName, ";") + 1));
                    asvAssignment = domain.make(ASVAssignment.class, rootIRI + "/ASVAssignment/Level/" + assignedTaxonInfo.mismatchCount + "/" + taxNameMD5);

                    life.gbol.domain.Taxon taxonRDF = domain.make(life.gbol.domain.Taxon.class, "http://gbol.life/0.1/lineage/" + taxNameMD5);
                    taxonRDF.setTaxonName(taxName);
                    taxonRDF.setTaxonRank(lineageLookup.get(StringUtils.countMatches(taxName, ";") + 1));
                    taxonRDF.setProvenance(provenance);
                    asvAssignment.setNumberHits(assignedTaxonInfo.taxonHitCount);
                    asvAssignment.setRatio((float) assignedTaxonInfo.ratio);
                    asvAssignment.setLevelCount(assignedTaxonInfo.mismatchCount);
                    asvAssignment.setType(TaxonAssignmentType.AssignedTaxon);
                    asvAssignment.setTaxon(taxonRDF);
                    asvSet.addAsvAssignment(asvAssignment);

                    asvSet.setAssignedTaxon(assignedTaxonRDF);      // Set assigned taxon to ASV

                    // Break it down
                    String lineage = "";
                    ArrayList<life.gbol.domain.Taxon> taxons = new ArrayList<>();
                    for (String s : taxName.split(";")) {
                        lineage = lineage + ";" + s;
                        lineage = lineage.replaceAll("^;", "");
                        String lineageMD5 = getMD5(lineage);
                        // System.err.println(lineage);
                        life.gbol.domain.Taxon taxon = domain.make(life.gbol.domain.Taxon.class, "http://gbol.life/0.1/lineage/" + lineageMD5);
                        taxon.setTaxonName(lineage);
                        taxon.setTaxonRank(lineageLookup.get(StringUtils.countMatches(lineage, ";") + 1));
                        taxon.setProvenance(provenance);
                        taxons.add(taxon);
                    }

                    Collections.reverse(taxons);

                    for (int i = 0; i < taxons.size() - 1; i++) {
                        life.gbol.domain.Taxon child = taxons.get(i);
                        life.gbol.domain.Taxon parent = taxons.get(i + 1);
                        child.setSubClassOf(parent);
                    }
                }
                // TODO only do this when we have a taxon hit in the database
                //                    asvSet.addAsvAssignment(asvAssignment);
                //                sampleRDF.addOtu(otuRDF);
                //                    asvSet.setSample(sampleRDF);
                sampleRDF.addAsv(asvSet);
            }
        }
    }

    /**
     * @param domain
     * @param turtleFileLocation
     */
    private static void writeOutputRDFFile(Domain domain, String turtleFileLocation) throws IOException {
        File file = new File(turtleFileLocation);
        logger.info("Job done!");
        logger.info("RDF turtle file saved to :" + file.getAbsolutePath());
        domain.save(file.getAbsolutePath(), RDFFormat.TURTLE);
    }

    /**
     * Extract information of the library created
     *
     * @param res -- NGTax results object
     * @return
     */
    private static HashMap createLibraryInfo(NGTaxResult res) {
        logger.info("Creating library information");
        HashMap<String, nl.wur.ssb.NGTax.Library> libraryData = new HashMap<>();
        Iterator library_it = res.provData.libraries.entrySet().iterator();

        while (library_it.hasNext()) {
            Map.Entry libraryIdMap = (Map.Entry) library_it.next();
            nl.wur.ssb.NGTax.Library library = (nl.wur.ssb.NGTax.Library) libraryIdMap.getValue();

            libraryData.put(String.valueOf(library.index), library);

            library_it.remove();
        }
        return libraryData;
    }

    /**
     * Extract information of the ASV created
     *
     * @param otuList
     * @return
     */
    public static LinkedHashMap<String, ASV> createOTUInfo(HashMap<String, ASV> otuList) {

        LinkedHashMap<String, ASV> OTUInfo = new LinkedHashMap<>();
        Iterator OTUInfo_it = otuList.entrySet().iterator();

        while (OTUInfo_it.hasNext()) {
            Map.Entry otuIdMap = (Map.Entry) OTUInfo_it.next();
            ASV otu = (ASV) otuIdMap.getValue();
            OTUInfo.put(otu.id, otu);
        }
        return OTUInfo;
    }

    /**
     * Remove bracket in empty taxonomy
     *
     * @param tax -- taxonomy
     * @return
     */
    private static String cleanBracketInEmptyTaxon(String tax) {

        String modifiedTax;

        if (tax.contains("<") || tax.contains(">")) {
            modifiedTax = tax.replace("<", "");
            modifiedTax = modifiedTax.replace(">", "");
        } else {
            modifiedTax = tax;
        }
        return modifiedTax;
    }

    /**
     * Remove square bracket in the taxonomy
     *
     * @param tax -- taxonomy
     * @return
     */
    private static String cleanSquareBracketInTaxon(String tax) {
        tax = cleanTaxonHash(tax);
        String modifiedTax;

        if (tax.contains("[") || tax.contains("]")) {
            modifiedTax = tax.replace("[", "");
            modifiedTax = modifiedTax.replace("]", "");
        } else {
            modifiedTax = tax;
        }
        return modifiedTax;
    }

    private static String cleanTaxonHash(String tax) {
        return tax.replace("#","_").replace("^","_");
    }

//    /**
//     * Create the hashcode of the file and use as the IRI
//     *
//     * @param file         -- input file
//     * @param checksumType -- checksum type: MD5 or SHA384
//     * @return checksum string
//     * @throws IOException
//     */
//    public static String checksum(File file, HashFunction checksumType) throws IOException {
//        HashCode hc = Files.hash(file, checksumType);

//        return hc.toString();
//    }

    /**
     * Create RDF data structure for traditional BIOM format, QIIME
     *
     * @param res
     * @param turtleFileLocation
     * @throws Exception
     */
    private static void traditionalBiomParser(NGTaxResult res, String turtleFileLocation) throws Exception {

        /*
         * Get information from rows parameters in the traditional biom file -- taxonomic information.
         */
        LinkedHashMap<Double, Object> rowsInfo = new LinkedHashMap<>();
        double ind = 0.0;
        for (LinkedHashMap<String, Object> taxonInfo : res.rows) {
            LinkedList taxon = new LinkedList();

            taxon.add(taxonInfo.values().toArray()[0].toString());

            LinkedTreeMap taxonName = (LinkedTreeMap) taxonInfo.values().toArray()[1];
            taxon.add(taxonName.values().toArray()[0].toString());

            rowsInfo.put(ind, taxon);
            ind += 1;
        }

        /*
         * Get information from columns parameters in the traditional biom file -- sample name information.
         */
        LinkedHashMap<Double, Object> colsInfo = new LinkedHashMap<>();
        ind = 0.0;
        for (LinkedHashMap<String, String> sampleName : res.columns) {
            colsInfo.put(ind, sampleName.values().toArray()[0]);
            ind += 1;
        }

        /*
         * Get information from data parameters in the traditional biom file -- matrix information.
         * Create a HashMap of all the corresponding data from both rows and columns parameters
         * Key = sample name
         * Value = [taxon ID, taxon, abundance]
         */
        LinkedHashMap<String, ArrayList<LinkedList<String>>> dataInfo = new LinkedHashMap<>();
        for (LinkedList<Object> dataCoordinates : res.data) {

            Double r = (Double) dataCoordinates.toArray()[0];
            Double c = (Double) dataCoordinates.toArray()[1];
            Double abundance = (Double) dataCoordinates.toArray()[2];

            LinkedList otuinfo = new LinkedList((Collection) rowsInfo.get(r));
            otuinfo.add(abundance);

            if (!(dataInfo.containsKey(colsInfo.get(c)))) {
                dataInfo.put((String) colsInfo.get(c), new ArrayList<>());
                dataInfo.get(colsInfo.get(c)).add(otuinfo);
            } else {
                dataInfo.get(colsInfo.get(c)).add(otuinfo);
            }

        }

        /*
         * Create the RDF database.
         */

        Domain domain = new Domain("");

        // Set unique id for IRI
        String uniqID;
        uniqID = turtleFileLocation.split(".ttl")[0];
        uniqID = uniqID.split("/")[uniqID.split("/").length - 1];


        rootIRI = rootIRI + uniqID;
        // Create random unique identifier
        String randomUniqueID = UUID.randomUUID().toString();
        logger.info("Converting " + uniqID + " to RDF format.");

        // Initial NG-Tax meta-settings
//        nl.wur.ssb.domain.NGTaxMeta biomMetaRDF;

        // Initial Sample RDF Entry
        life.gbol.domain.Sample sampleRDF;

        // Initial ASV RDF Entry
        ASVSet otuRDF;

        // Initial Assigned taxon RDF Entry and create data table
        life.gbol.domain.Taxon assignedTaxonRDF;

        /*
         * Set up NG-Tax meta-settings
         */
//        biomMetaRDF = domain.make(nl.wur.ssb.domain.NGTaxMeta.class, rootIRI + "/" + randomUniqueID + "/NGTaxMeta");
//        biomMetaRDF.setId(res.id);
//        biomMetaRDF.setFormat(res.format);
//        biomMetaRDF.setFormatURL(res.format_url);
//        biomMetaRDF.setType(res.type);
//        biomMetaRDF.setGeneratedBy(res.generated_by);
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//        String formatDatetime = res.date.format(formatter);
//        biomMetaRDF.setDate(formatDatetime);
//        biomMetaRDF.setMatrixType(res.matrix_type);
//        biomMetaRDF.setMatrixElementType(res.matrix_element_type);

        int sampleCount = 1;
        for ( String sampleName : dataInfo.keySet() ) {
//            App.logger.info(sampleName);

            String sampleCountString = "(" + Integer.toString(sampleCount) + "/" + Integer.toString(dataInfo.keySet().size()) + ")";
            logger.info(sampleCountString + " >> Creating RDF database for sample name: " + sampleName);


            /*
             * Set up sample information
             */
            sampleRDF = domain.make(Sample.class, rootIRI + "/Sample/" + sampleName);
            sampleRDF.setName(sampleName);

            for (LinkedList<String> sampleOTU : dataInfo.get(sampleName)) {
                otuRDF = domain.make(ASVSet.class, sampleRDF.getResource().getURI() + "/ASV/" + sampleOTU.toArray()[0].toString());
                otuRDF.setMasterASVId(sampleOTU.toArray()[0].toString());

                assignedTaxonRDF = domain.make(life.gbol.domain.Taxon.class, otuRDF.getResource().getURI() + "/AssignedTaxon/" + sampleOTU.toArray()[1].toString());
                assignedTaxonRDF.setTaxonName(sampleOTU.toArray()[1].toString());
//                assignedTaxonRDF.setTaxonHitCount((int) Math.round((Double) sampleOTU.toArray()[2]));

                otuRDF.setAssignedTaxon(assignedTaxonRDF);
//                sampleRDF.addOtu(otuRDF);
//                otuRDF.setSample(sampleRDF);
                sampleRDF.addAsv(otuRDF);

                // Add sample metadata from the mapping file
//                addMetadata(domain, sampleRDF);
            }

            sampleCount += 1;
        }
        /*
         * Write RDF Entry to file
         */
        writeOutputRDFFile(domain, turtleFileLocation);

    }
}
