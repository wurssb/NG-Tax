package nl.wur.ssb.NGTax;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

@Parameters()
public class ExportFastaOptions
{

  @Parameter(names = {"-exportFasta"}, description = "Export fasta")
  public boolean exportFasta;

  @Parameter(names = "--help", help = true)
  private Boolean help;

  @Parameter(names = {"-i","-biom"}, description = "The biom file to export the fasta file", required = true)
  String input;

  @Parameter(names = {"-o"}, description = "The output zip file to write to", required = true)
  String output;

  @Parameter(names = {"-t","-createTree"}, description = "Include trees, created with clustalo")
  boolean createTree = false;

  public ExportFastaOptions(String args[])
  {
    try
    {
      new JCommander(this, args); 
      if(this.help != null)
      {
        new JCommander(this).usage();
        System.exit(0);
      }
    }
    catch(ParameterException pe) 
    {
      System.err.println(pe.getMessage());
      new JCommander(this).usage();
      System.err.println("  * required parameter");
      System.exit(1);
    }
  }
  
}
