package nl.wur.ssb.NGTax;

import nl.wur.ssb.NGTax.CommandOptions.CommandOptionsNGTax;
import nl.wur.ssb.RDFSimpleCon.Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static nl.wur.ssb.NGTax.Database.createDegeneratePrimers;
import static nl.wur.ssb.NGTax.Database.logger;

public class Mock {
    public static void run(NGTax ngTax, String mock) {
        logger.info("Running " + mock);

        // Create samples
        int libraryNumber = ngTax.libraries.size() + 1;
        Sample sample = new Sample("", libraryNumber, "", "", new HashMap<>());

        // ASV number lookup
        HashMap<String, String> asvLookup = new HashMap<>();

        ngTax.asvList.forEach((key, asv) -> {
            String sequence = asv.forwardSequence + "___"+ asv.reverseSequence;
            asvLookup.put(sequence, key);
        });

        // Mock from resource fasta file and set sample name based on argument
        if (mock.matches("mock3")) {
            mock = "mock/mock3.fa";
            sample.sampleName = ngTax.args.mock3;
        }
        if (mock.matches("mock4")) {
            mock = "mock/mock4.fa";
            sample.sampleName = ngTax.args.mock4;
        }

        ngTax.samples.add(sample);

        // Create library, fwd and rev files are the same as we are working with full length 16S here
        ngTax.libraries.put(libraryNumber, new Library(libraryNumber, mock, mock, null, null));

        try {
            InputStream inputStream = Util.getResourceFile(mock);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String header = null;
            int id = 0;
            int asvCount = 0;
            int readCount = 0;
            HashMap<String, SampleASV> sampleOtuLookup = new HashMap<>();
            if (mock.contains("mock3"))
                id = 3000;
            if (mock.contains("mock4"))
                id = 4000;

            while(reader.ready()) {
                String line = reader.readLine();
                if (line.startsWith(">")) {
                    header = line.replaceFirst(">", "");
                    asvCount++;
                } else {
                    id++;
                    if (!line.contains("A")) {
                        System.err.println(line);
                    } else {
                        String sequence = line;
                        // Create sample object?
                        ASV asv = getRegion(ngTax.args, sequence);
                        if (asv == null) {
                            logger.warn("No match with mock dataset with current primers for sequence " + header);
                            continue;
                        }
                        if (asvLookup.containsKey(asv.forwardSequence + "___" + asv.reverseSequence)) {
                            asv.id = asvLookup.get(asv.forwardSequence + "___" + asv.reverseSequence);
                        } else {
                            asv.id = String.valueOf(id);
                            // If not exists add to list otherwise it is already there
                            ngTax.asvList.put(asv.id, asv);
                            asvLookup.put(asv.forwardSequence + "___" + asv.reverseSequence, asv.id);
                        }

                        // Create sample specific OTU

                        SampleASV sampleOTU;
                        if (sampleOtuLookup.containsKey(asv.forwardSequence + "-" + asv.reverseSequence)) {
                            sampleOTU = sampleOtuLookup.get(asv.forwardSequence + "-" + asv.reverseSequence);
                        } else {
                            sampleOTU = new SampleASV(asv.forwardSequence, asv.reverseSequence);
                            sampleOtuLookup.put(asv.forwardSequence + "-" + asv.reverseSequence, sampleOTU);
                            sampleOTU.id = id; // Integer.parseInt(asv.id);
                            sampleOTU.masterOtuId = String.valueOf(asv.id);
                            sampleOTU.readCount = 0;
                            sampleOTU.clusteredReadCount = Math.round(Float.parseFloat(header.split("conc=")[1]) * 10000);
                            sample.otus.add(sampleOTU);
                        }

                        // If you have the same OTU from a different 16S sum the counts
                        sampleOTU.readCount += Math.round(Float.parseFloat(header.split("conc=")[1]) * 10000);

                        // Standard counter
                        readCount += Math.round(Float.parseFloat(header.split("conc=")[1]) * 10000);
                    }
                }
            }

            sample.numAcceptedOtuBeforeChimera = asvCount;
            sample.numAcceptedReadsAfterErrorCorrection = readCount;
            sample.numAcceptedReadsBeforeChimera = readCount;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static ASV getRegion(CommandOptionsNGTax options, String sequence) {
        String fwdSequence = sequence;
        String revSequence = ClassifyASV.revComplementRNA(fwdSequence);

        ArrayList<Pattern> forwardPrimers = createDegeneratePrimers(options.forwardPrimer, options.forwardReadLength, options.nomismatch);
        ArrayList<Pattern> reversePrimers = createDegeneratePrimers(options.reversePrimer, options.reverseReadLength, options.nomismatch);

        boolean fwdHit = false;
        String fwdSeq = null;
        String revSeq = null;

//        int fwdCount = 0;
        for (Pattern primerPattern : forwardPrimers) { // A U T C G?
            Matcher m = primerPattern.matcher(fwdSequence);
            boolean fwdMatch = m.find();
            if (!fwdMatch) {
                m = primerPattern.matcher(revSequence);
                fwdMatch = m.find();
            }
            if (fwdMatch) {
                fwdHit = true;
//                fwdCount = fwdCount + 1;
                fwdSeq = m.group().replaceAll("U", "T");
                fwdSeq = fwdSeq.substring(options.forwardPrimerLength, fwdSeq.length());
                // System.err.println(fwdSeq);
//                line += sequence + "\t";
                break;
            }
        }
        // Perform pair primer matching
        // boolean revHit = false;
        if (fwdHit && reversePrimers != null) {
            for (Pattern primerPattern : reversePrimers) {// A U T C G?
                Matcher m = primerPattern.matcher(fwdSequence);
                boolean revMatch = m.find();
                if (!revMatch) {
                    m = primerPattern.matcher(revSequence);
                    revMatch = m.find();
                }

                if (revMatch) {
                    //revHit = true;
//                    revCount = revCount + 1;
                    revSeq = m.group().replaceAll("U", "T");
                    revSeq = revSeq.substring(revSeq.length() - options.reverseReadLength);
                    // System.err.println(revSeq);
//                    line += sequence + "\t";
                    break;
                }
            }
        }
        if (fwdSeq == null || revSeq == null) return null;

        return new ASV("INTERNAL_MOCK", fwdSeq, revSeq);
    }
}
