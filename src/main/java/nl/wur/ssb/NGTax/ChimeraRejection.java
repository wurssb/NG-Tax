package nl.wur.ssb.NGTax;

import com.google.gson.annotations.Expose;

public class ChimeraRejection {
    @Expose
    SampleASV otu;
    @Expose
    int pForwardId;
    @Expose
    double pForwardRatio;
    @Expose
    int pReverseId;
    @Expose
    double pReverseRatio;

    public ChimeraRejection(SampleASV otu, int pForwardId, double pForwardRatio, int pReverseId, double pReverseRatio) {
        this.otu = otu;
        this.pForwardId = pForwardId;
        this.pForwardRatio = pForwardRatio;
        this.pReverseId = pReverseId;
        this.pReverseRatio = pReverseRatio;
    }

}
