package nl.wur.ssb.NGTax;

import nl.wur.ssb.NGTax.CommandOptions.CommandOptionsBarCodeSplitting;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.zip.GZIPOutputStream;

/**
 * Function that splits the input raw fastq files into separate library files
 */
public class BarCodeSplitting {
    public static void split(CommandOptionsBarCodeSplitting argsBarcodeSplit) throws Exception {

        // NOTE: Barcode does not have to be on both reads as we create RAW DATA (UNFILTERED)
        HashMap<String, String> barcodes = new HashMap<>();
        int libraryCol = -1;
        for (String mapLine : new String(Files.readAllBytes(Paths.get(argsBarcodeSplit.mapFile))).replaceAll("\r\n", "\n").replaceAll("\r", "\n").split("\n")) {

            // If the trimmed version of each line empty or starts with #, ignore
            // .trim() uses to trim of all white spaces

            if (mapLine.trim().isEmpty() || mapLine.trim().startsWith("#")) {
                String[] header = mapLine.split("\t");

                for (int i = 0; i < header.length; i++) {
                    if (header[i].trim().matches("LibraryNumber")) {
                        libraryCol = i;
                        break;
                    }
                }

                continue; // skip that line (the header)
            }

            if (libraryCol < 0) {
                throw new Exception("Could not find the LibraryNumber header");
            }

            String[] mapLineParts = mapLine.split("\t");

            if (mapLineParts.length < 3) { // Check if there is enough fields in mapping file
                throw new Exception("Need at least 3 fields in the mapping file separated by a tab: " + mapLine);
            }

            if (mapLine.contains(argsBarcodeSplit.forward.getName()) || mapLine.contains(argsBarcodeSplit.reverse.getName())) {
                // Set second column as fBarcode (first column is the sample name)
                String fBarcode = mapLineParts[1]; // fBarcode = GAACGTAT
                String rBarcode = fBarcode; // rBarcode = GAACGTAT

                barcodes.put(fBarcode, mapLineParts[libraryCol]);
                barcodes.put(rBarcode, mapLineParts[libraryCol]);
            }
        }

        // Create writers for the libraries
        HashMap<String, ArrayList<PrintWriter>> libraryWriter = new HashMap<>();
        for (String library : barcodes.values()) {
            String filenameFWD = argsBarcodeSplit.libraryFile + "_FWD.fastq.gz";
            OutputStream fileStreamFWD = new FileOutputStream(filenameFWD);
            OutputStream gzipStreamFWD = new GZIPOutputStream(fileStreamFWD);
            PrintWriter printWriterFWD = new PrintWriter(gzipStreamFWD);

            String filenameRev = argsBarcodeSplit.libraryFile + "_REV.fastq.gz";
            OutputStream fileStreamREV = new FileOutputStream(filenameRev);
            OutputStream gzipStreamREV = new GZIPOutputStream(fileStreamREV);
            PrintWriter printWriterREV = new PrintWriter(gzipStreamREV);

            libraryWriter.put(library, new ArrayList<>());
            libraryWriter.get(library).add(printWriterFWD);
            libraryWriter.get(library).add(printWriterREV);
        }

        Scanner forwardScanner = new Scanner(argsBarcodeSplit.forward);
        Scanner reverseScanner = new Scanner(argsBarcodeSplit.reverse);

        while (forwardScanner.hasNextLine()) {
            String fwdHeader = forwardScanner.nextLine();
            String fwdSequence = forwardScanner.nextLine();
            String fwdPlus = forwardScanner.nextLine();
            String fwdQuality = forwardScanner.nextLine();

            String revHeader = reverseScanner.nextLine();
            String revSequence = reverseScanner.nextLine();
            String revPlus = reverseScanner.nextLine();
            String revQuality = reverseScanner.nextLine();

            for (String barcode : barcodes.keySet()) {
                if (fwdSequence.contains(barcode) || revSequence.contains(barcode)) {
                    // Write to this file...
                    String library = barcodes.get(barcode);
                    libraryWriter.get(library).get(0).println(fwdHeader + "\n" + fwdSequence + "\n" + fwdPlus + "\n" + fwdQuality);
                    libraryWriter.get(library).get(1).println(revHeader + "\n" + revSequence + "\n" + revPlus + "\n" + revQuality);
                }
            }
        }

        for (ArrayList<PrintWriter> writers : libraryWriter.values()) {
            writers.get(0).close();
            writers.get(1).close();
        }
    }
}
