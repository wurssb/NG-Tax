package nl.wur.ssb.NGTax;

import com.google.gson.*;
import com.google.gson.annotations.Expose;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;

public class NGTaxResult {
    private static Gson gson;
    @Expose
    public String id = "None";
    @Expose
    public String format = "Biological Observation Matrix 1.0.0";
    @Expose
    public String format_url = "http://biom-format.org";
    @Expose
    public String type = "Taxon table";
    @Expose
    public String generated_by = "NGTax";
    @Expose
    public String version;
    @Expose
    public String commit;
    @Expose
    public String gitRepo = "https://gitlab.com/wurssb/NG-Tax";
    @Expose
    public LocalDateTime date;
    @Expose
    public String matrix_type = "sparse";
    @Expose
    public String matrix_element_type = "float";
    @Expose
    public NGTax provData;

    public LocalDateTime startTime = LocalDateTime.now();
    LinkedHashSet<String> otuMap = new LinkedHashSet<>();
    LinkedHashSet<String> sampleMap = new LinkedHashSet<>();
    HashMap<String, HashMap<String, Float>> valueMap = new HashMap<>();
    HashMap<String, String> otuToTaxonMap = new HashMap<>();
    @Expose
    public int[] shape;
    @Expose
    public LinkedHashMap<String, Object> rows[];
    @Expose
    public LinkedHashMap<String, String> columns[];
    @Expose
    public LinkedList<LinkedList<Object>> data;

    {
        GsonBuilder builder = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().serializeSpecialFloatingPointValues().serializeNulls();
        builder.registerTypeAdapter(LocalDateTime.class, new DateTimeDeserializer());
        builder.registerTypeAdapter(LocalDateTime.class, new DateTimeSerializer());
        gson = builder.create();
    }
    public NGTaxResult() {
        date = LocalDateTime.now();
    }

    public static NGTaxResult fromJSon(String json) {
        try {
            NGTaxResult ngTaxResult = NGTaxResult.gson.fromJson(json, NGTaxResult.class);
            // Backward compatability for the old Biom file which still use OTUs instead of ASVs.
            if (ngTaxResult.provData.asvList.size() == 0 && ngTaxResult.provData.otuList.size() > 0) {
                ngTaxResult.provData.asvList = ngTaxResult.provData.otuList;
                ngTaxResult.provData.otuList = null;
            }
            return ngTaxResult;
        } catch (com.google.gson.JsonSyntaxException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    public void save(String file) throws Exception {
        Writer writer = new BufferedWriter(new FileWriter(file));
        writer.write(this.toJSon());
        writer.close();
    }

    public String toJSon() {
        rows = new LinkedHashMap[otuMap.size()];
        int count = 0;
        for (String otuId : otuMap) {
            rows[count] = new LinkedHashMap<>();
            rows[count].put("id", otuId);
            HashMap<String, String[]> taxonMap = new HashMap<>();
            HashMap<String, String[]> allTaxonMap = new HashMap<>();
            taxonMap.put("taxonomy", this.otuToTaxonMap.get(otuId).split(";"));
            allTaxonMap.put("taxonomy", this.otuToTaxonMap.get(otuId).split(","));
            rows[count].put("metadata", taxonMap);
            count++;
        }
        columns = new LinkedHashMap[sampleMap.size()];
        count = 0;
        for (String sample : sampleMap) {
            columns[count] = new LinkedHashMap<>();
            columns[count].put("id", sample);
            columns[count].put("metadata", null);
            count++;
        }
        data = new LinkedList<>();
        int row = 0;
        int column = 0;
        for (String specie : otuMap) {
            column = 0;
            HashMap<String, Float> rowValues = valueMap.get(specie);
            if (rowValues != null) {
                for (String sample : sampleMap) {
                    Float value = rowValues.get(sample);
                    if (value != null) {
                        LinkedList<Object> list = new LinkedList<>();
                        list.add(row);
                        list.add(column);
                        list.add((double) value);
                        data.add(list);
                    }
                    column++;
                }
            }
            row++;
        }
        this.shape = new int[]{otuMap.size(), sampleMap.size()};
        return NGTaxResult.gson.toJson(this);
    }

    public void addSpecie(String specie) {
        otuMap.add(specie);
    }

    public void addSample(String sample) {
        sampleMap.add(sample);
    }

    public void addValue(String otuId, String sample, float value) {
        otuMap.add(otuId);
        sampleMap.add(sample);
        HashMap<String, Float> row = valueMap.get(otuId);
        if (row == null) {
            row = new HashMap<>();
            valueMap.put(otuId, row);
        }
        row.put(sample, value);
    }

    public void addTaxonToOtu(String otuId, String taxon) {
        otuToTaxonMap.put(otuId, taxon);
    }

    private class DateTimeSerializer implements JsonSerializer<LocalDateTime> {
        public JsonElement serialize(LocalDateTime src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(src.toString());
        }
    }

    private class DateTimeDeserializer implements JsonDeserializer<LocalDateTime> {
        public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            return LocalDateTime.parse(json.getAsJsonPrimitive().getAsString());
        }
    }
}
