package nl.wur.ssb.NGTax;

import java.io.Serializable;

/**
 * Create a library of database
 * Index, forward sequence, reverse sequence and its corresponding taxonomy
 */

class EntryDB implements Serializable {
    int abundance;
    int index;
    byte[] forwardSeq;
    byte[] reverseSeq;
    String taxon[];
    //String id;
    int forwardScore = -1;

    EntryDB(int index, String forwardSeq, String reverseSeq, String taxon[], int abundance)//,String id
    {
        this.index = index;
        this.forwardSeq = forwardSeq.getBytes();
        this.reverseSeq = reverseSeq.getBytes();
        for (int i = 0; i < this.forwardSeq.length; i++)
            this.forwardSeq[i] = (byte) (this.forwardSeq[i] & 0x0f);
        for (int i = 0; i < this.reverseSeq.length; i++)
            this.reverseSeq[i] = (byte) (this.reverseSeq[i] & 0x0f);
        this.taxon = taxon;
        this.abundance = abundance;
        //this.id = id;
    }


}
