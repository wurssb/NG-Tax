package nl.wur.ssb.NGTax;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Initialize mismatch level
 */

public class MismatchLevel implements Serializable{
    @Expose
    int mismatchCount = 0;
    @Expose
    int levelCount = 0;
    @Expose
    HashMap<String, ASVMatchStats> hitsTaxon[] = new HashMap[6];
    @Expose
    ASVMatchStats bestTaxon[] = new ASVMatchStats[6];
    @Expose
    LinkedList<Integer> idList = new LinkedList<>();

    MismatchLevel(int mismatchCount) {
        this.mismatchCount = mismatchCount;
        for (int i = 0; i < hitsTaxon.length; i++)
            hitsTaxon[i] = new HashMap<>();
    }
}
