package nl.wur.ssb.NGTax;

import java.util.Arrays;

/**
 * Calculate the Levenshtein distance between two sequences
 */

public class Levenshtein {
    //TODO memory print can be smaller
    private int matrix[][];
    private byte[] source;
    private byte[] prevComp;
    private int threshold;

    public Levenshtein(byte[] source, int threshold) {
        this.threshold = threshold;
        matrix = new int[source.length + 1][source.length + 1];
        this.source = source;
        // fill in starting table values
        int n = this.source.length;
        final int boundary = Math.min(n, threshold) + 1;
        for (int i = 0; i < boundary; i++) {
            //p[i] = i;
            this.matrix[0][i] = i;
        }
        // these fills ensure that the value above the rightmost entry of our
        // stripe will be ignored in following loop iterations
        // Fill the rest of the top-row matrix with the maximum value of the INT.
        Arrays.fill(this.matrix[0], boundary, this.matrix[0].length, Integer.MAX_VALUE);
    }


    public int getLevenshteinDistanceMask(String t) {
        return this.getLevenshteinDistanceMask(t.getBytes());
    }

    /*
     * Source taken from org.apache.commons.lang3.StringUtils class and modified for our purposes
     * String should be equal length, as we do global alignment
     * We use full matrix, as we do not count the mismatches at the end of the string
     */
    public int getLevenshteinDistanceMask(byte[] t) {
        if (threshold < 0 || source.length != t.length) {
            throw new IllegalArgumentException("getLevenshteinDistance faulty parameters");
        }

    /*
    This implementation only computes the distance if it's less than or equal to the
    threshold value, returning -1 if it's greater.  The advantage is performance: unbounded
    distance is O(nm), but a bound of k allows us to reduce it to O(km) time by only
    computing a diagonal stripe of width 2k + 1 of the cost table.
    It is also possible to use this to compute the unbounded Levenshtein distance by starting
    the threshold at 1 and doubling each time until the distance is found; this is O(dm), where
    d is the distance.

    One subtlety comes from needing to ignore entries on the border of our stripe
    eg.
    p[] = |#|#|#|*
    d[] =  *|#|#|#|
    We must ignore the entry to the left of the leftmost member
    We must ignore the entry above the rightmost member

    Another subtlety comes from our stripe running off the matrix if the strings aren't
    of the same size.  Since string s is always swapped to be the shorter of the two,
    the stripe will always run off to the upper right instead of the lower left of the matrix.

    As a concrete example, suppose s is of length 5, t is of length 7, and our threshold is 1.
    In this case we're going to walk a stripe of length 3.  The matrix would look like so:

       1 2 3 4 5
    1 |#|#| | | |k  i   t   t   e   n
    2 |#|#|#| | |
    3 | |#|#|#| |
    4 | | |#|#|#|
    5 | | | |#|#|
    6 | | | | |#|
    7 | | | | | |

    Note how the stripe leads off the table as there is no possible way to turn a string of length 5
    into one of length 7 in edit distance of 1.

    Additionally, this implementation decreases memory usage by using two
    single-dimensional arrays and swapping them back and forth instead of allocating
    an entire n by m matrix.  This requires a few minor changes, such as immediately returning
    when it's detected that the stripe has run off the matrix and initially filling the arrays with
    large values so that entries we don't compute are ignored.

    See Algorithms on Strings, Trees and Sequences by Dan Gusfield for some discussion.
     */

        int n = this.source.length; // length of the source

        int j = 1;
        if (this.prevComp != null) {
            for (int i = 0; i < t.length; i++) {
                if (t[i] == this.prevComp[i])
                    j++;
                else
                    break;
            }
        }
        this.prevComp = t;

        // iterates through t
        for (; j <= n; j++) {
            final byte t_j = this.source[j - 1]; // jth character of t
            int p[] = this.matrix[j - 1];
            int d[] = this.matrix[j];

            // compute stripe indices, constrain to array size
            final int min = Math.max(1, j - threshold);
            final int max = Math.min(n, j + threshold);

            // the stripe may lead off of the table if s and t are of different sizes
            if (min > max) {
                return -1;
            }

            // ignore entry left of leftmost
            if (min > 1)
                d[min - 1] = Integer.MAX_VALUE;
            else
                d[0] = j;

            // iterates through [min, max] in s
            for (int i = min; i <= max; i++) {
                //use mask function
                if ((t[i - 1] & t_j) > 0) {
                    // diagonally left and up
                    d[i] = p[i - 1];
                } else {
                    // 1 + minimum of cell to the left, to the top, diagonally left and up
                    d[i] = 1 + Math.min(Math.min(d[i - 1], p[i]), p[i - 1]);
                }
            }
            if (j + threshold + 1 <= n)
                d[j + threshold + 1] = Integer.MAX_VALUE;
        }

        //do not count any insertions and deletions at the end of string
        int res = Integer.MAX_VALUE;
        for (int i = n; i >= n - threshold; i--) {
            res = Math.min(res, Math.min(this.matrix[n][i], this.matrix[i][n]));
        }
        // if p[n] is greater than the threshold, there's no guarantee on it being
        // the correct
        // distance
        if (res <= threshold) {
            return res;
        }
        return -1;
    }

    // Duplicate function to save compute time
    public int getLevenshteinDistance(String t) {
        return this.getLevenshteinDistance(t.getBytes());
    }

    /*
     * Source taken from org.apache.commons.lang3.StringUtils class and modified for our purposes
     * String should be equal length, as we do global alignment
     * We use full matrix, as we do not count the mismatches at the end of the string
     */
    public int getLevenshteinDistance(byte[] t) {
        if (threshold < 0 || source.length != t.length) {
            throw new IllegalArgumentException("getLevenshteinDistance faulty parameters");
        }
    
    /*
    This implementation only computes the distance if it's less than or equal to the
    threshold value, returning -1 if it's greater.  The advantage is performance: unbounded
    distance is O(nm), but a bound of k allows us to reduce it to O(km) time by only
    computing a diagonal stripe of width 2k + 1 of the cost table.
    It is also possible to use this to compute the unbounded Levenshtein distance by starting
    the threshold at 1 and doubling each time until the distance is found; this is O(dm), where
    d is the distance.

    One subtlety comes from needing to ignore entries on the border of our stripe
    eg.
    p[] = |#|#|#|*
    d[] =  *|#|#|#|
    We must ignore the entry to the left of the leftmost member
    We must ignore the entry above the rightmost member

    Another subtlety comes from our stripe running off the matrix if the strings aren't
    of the same size.  Since string s is always swapped to be the shorter of the two,
    the stripe will always run off to the upper right instead of the lower left of the matrix.

    As a concrete example, suppose s is of length 5, t is of length 7, and our threshold is 1.
    In this case we're going to walk a stripe of length 3.  The matrix would look like so:

       1 2 3 4 5
    1 |#|#| | | |k  i   t   t   e   n
    2 |#|#|#| | |
    3 | |#|#|#| |
    4 | | |#|#|#|
    5 | | | |#|#|
    6 | | | | |#|
    7 | | | | | |

    Note how the stripe leads off the table as there is no possible way to turn a string of length 5
    into one of length 7 in edit distance of 1.

    Additionally, this implementation decreases memory usage by using two
    single-dimensional arrays and swapping them back and forth instead of allocating
    an entire n by m matrix.  This requires a few minor changes, such as immediately returning
    when it's detected that the stripe has run off the matrix and initially filling the arrays with
    large values so that entries we don't compute are ignored.

    See Algorithms on Strings, Trees and Sequences by Dan Gusfield for some discussion.
     */

        int n = this.source.length; // length of s

        int j = 1;
        if (this.prevComp != null) {
            for (int i = 0; i < t.length; i++) {
                if (t[i] == this.prevComp[i])
                    j++;
                else
                    break;
            }
        }
        this.prevComp = t;

        // iterates through t
        for (; j <= n; j++) {
            final byte t_j = this.source[j - 1]; // jth character of t
            int p[] = this.matrix[j - 1];
            int d[] = this.matrix[j];

            // compute stripe indices, constrain to array size
            final int min = Math.max(1, j - threshold);
            final int max = Math.min(n, j + threshold);

            // the stripe may lead off of the table if s and t are of different sizes
            if (min > max) {
                return -1;
            }

            // ignore entry left of leftmost
            if (min > 1)
                d[min - 1] = Integer.MAX_VALUE;
            else
                d[0] = j;

            // iterates through [min, max] in s
            for (int i = min; i <= max; i++) {
                if (t[i - 1] == t_j) // Different condition from masked.
                {
                    // diagonally left and up
                    d[i] = p[i - 1];
                } else {
                    // 1 + minimum of cell to the left, to the top, diagonally left and up
                    d[i] = 1 + Math.min(Math.min(d[i - 1], p[i]), p[i - 1]);
                }
            }
            if (j + threshold + 1 <= n)
                d[j + threshold + 1] = Integer.MAX_VALUE;
        }

        //do not count any insertions and deletions at the end of string
        int res = Integer.MAX_VALUE;
        for (int i = n; i >= n - threshold; i--) {
            res = Math.min(res, Math.min(this.matrix[n][i], this.matrix[i][n]));
        }
        // if p[n] is greater than the threshold, there's no guarantee on it being
        // the correct
        // distance
        if (res <= threshold) {
            return res;
        }
        return -1;
    }
}
