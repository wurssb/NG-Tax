package nl.wur.ssb.NGTax;

import com.google.gson.annotations.Expose;
import nl.wur.ssb.NGTax.CommandOptions.*;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.*;

/**
 * NGTax program description HERE
 */
public class App {
    // The logger instance
    static final Logger logger = LogManager.getLogger(App.class);
    // Argument object
    static CommandOptionsNGTax arguments;

    static LinkedList<EntryDB> refDb = new LinkedList<>();
    // ordered according forward sequence
    static LinkedList<EntryDB> refDbForward = new LinkedList<>();
    // ordered according reverse sequence
    static LinkedList<EntryDB> refDbReverse = new LinkedList<>();

    @Expose
    public static HashMap<String, ASV> asvs = new HashMap<>();
    @Expose
    public static LinkedHashMap<String, ASV> rejectedASVList = new LinkedHashMap<>();

    /**
     * The main function of the program
     *
     * @param args standard arguments according to NGTax
     * @throws Exception overall exception
     */
    public static void main(String[] args) throws Exception {
        // Remove empty args e.g. ''
        List<String> list = new ArrayList<>(Arrays.asList(args));
        list.removeAll(Collections.singleton(null));
        list.removeAll(Collections.singleton(""));
        args = list.toArray(new String[0]);

        if (args.length == 0) {
            new CommandOptions(new String[]{"-help"});
        }

        CommandOptions options = new CommandOptions(args);

        arguments = null; // Initialize command line options

        if (options.db2rdf) {
            CommandOptionsRef2RDF argsRef2DB = new CommandOptionsRef2RDF(args);
            Database.toRDF(argsRef2DB);
            return;
        } else if (options.coverage) {
            logger.info("Performing a coverage analysis");
            CommandOptionsCoverage commandOptionsCoverage = new CommandOptionsCoverage(args);
            Coverage.start(commandOptionsCoverage);
            return;
        } else if (options.barcodeSplitting) {
            // Splits the input FASTQ files into separate libraries based on the barcode in the mapping file
            // TODO Wasin check this code, made by Jasper
            CommandOptionsBarCodeSplitting argsBarcodeSplit = new CommandOptionsBarCodeSplitting(args);
            BarCodeSplitting.split(argsBarcodeSplit);
            return;
        } else if (options.biom2rdf) {
            // Directly convert the existing BIOM file to RDF format
            CommandOptionsBiom2RDF argsbiom2rdf = new CommandOptionsBiom2RDF(args);
            Biom2Rdf.biomDirectParser(argsbiom2rdf.biomFile, argsbiom2rdf.outFile);
            return;
        } else if (options.otu2fasta) {
            // Extract ASV in fasta format from an existing BIOM file.
            CommandOptionsBiom2OTUfasta argsbiom2otufasta = new CommandOptionsBiom2OTUfasta(args);
            Biom2ASVfasta.biom2fastaDirectParser(argsbiom2otufasta.biomFile, argsbiom2otufasta.outFile);
            return;
        } else if (options.demultiplex) {
            // Demultiplex files for submission
            CommandOptionsDemultiplex argsDemultiplex = new CommandOptionsDemultiplex(args);
            argsDemultiplex.demultiplex = true;
            Demultiplex.directDemultiplex(argsDemultiplex,
                    argsDemultiplex.fastQSet,
                    argsDemultiplex.mapFile,
                    argsDemultiplex.forwardPrimer,
                    argsDemultiplex.reversePrimer,
                    argsDemultiplex.zip,
                    argsDemultiplex.untrim,
                    argsDemultiplex.output);
            return;
        } else if (options.exportFasta) {
            ExportFasta exportFasta = new ExportFasta(new ExportFastaOptions(args));
            exportFasta.export();
            return;
//        } else if (options.silvaTree) {
//            CommandOptionsSilvaTree commandOptionsSilvaTree = new CommandOptionsSilvaTree(args);
//            SilvaTree.main(commandOptionsSilvaTree);
//            return;
        } else if (options.reClassify) {
            CommandOptionsReClassify argsReClassify = new CommandOptionsReClassify(args);
            ReClassify reClassify = new ReClassify();
            argsReClassify.reClassify = true;
            reClassify.biomReClassify(
                    argsReClassify.biomFile,
                    argsReClassify.refdb,
                    argsReClassify.outBiomFile,
                    argsReClassify.outTTLFile);
            return;
        } else if (options.mock3 != null || options.mock4 != null) {
            // Run the mock sample from the resource folder
            arguments = new CommandOptionsNGTax(args);
        } else {
            // Enabled by default
            arguments = new CommandOptionsNGTax(args);
        }

        // Setting the logger
        LogManager.getLogger().atLevel(Level.INFO);
        if (arguments.debug) {
            LogManager.getLogger().atLevel(Level.DEBUG);
        }

        logger.info("Performing NG-Tax standard");

        // Filter several fastaQ files to one file
        NGTax ngTax = new NGTax(arguments); // Pass arguments to NGTax class
        ngTax.mkDir();

        // If no folder or Fs files are given but mock option is used...

        if ((arguments.folder == null) && (arguments.fastQSet == null)) {
            if (arguments.mock3 != null || arguments.mock4 != null) {
                if (options.mock3 != null)
                    Mock.run(ngTax, "mock3");
                if (options.mock4 != null)
                    Mock.run(ngTax, "mock4");

                asvs = ngTax.asvList;

                ClassifyASV.loadrefdb();
                ClassifyASV.classifyASVs();

                // Galaxy has the script to run this.
                logger.info("Creating biom file");
                ngTax.createBiomFile();
                logger.info("Biom file created");
                return;
            } else {
                System.err.println(arguments.mock3 + " " + arguments.mock4);
                throw new Exception("No input files detected, use -fS or -folder.");
            }
        }

        // Check if the input has many sample in a single file
        if (arguments.single == null) {
            ngTax.readSampleMapFile();
        }

        // TODO check if we should check if db is given like  if(arguments.refdb_obsolete != null)
        if (arguments.refdb != null) {
            ClassifyASV.loadrefdb();
            ngTax.asvList = asvs;
            ngTax.rejectedASVList = rejectedASVList;
        }

        if (!arguments.skipFiltering && !arguments.singleEnd)
            ngTax.filterFastQFiles();
        if (!arguments.skipFiltering && arguments.singleEnd)
            ngTax.filterFastQFilesSingle();

        if (arguments.fastQfiles) {
            ngTax.zipDirectory(new File("fastQFiles"), "fastQFiles/fastQFiles.zip");
        }

        // Create a custom reference database
        ngTax.processSamples();
        ngTax.collectGeneralOtu();

        // Process MOCK3 / MOCK4
        if (options.mock3 != null)
            Mock.run(ngTax, "mock3");
        if (options.mock4 != null)
            Mock.run(ngTax, "mock4");


        if (arguments.refdb != null) {
            ClassifyASV.classifyASVs();
        }

        // Galaxy has the script to run this.
        logger.info("Creating biom file");
        ngTax.createBiomFile();
        logger.info("Biom file created");
    }
}
