package nl.wur.ssb.NGTax;

import nl.wur.ssb.NGTax.CommandOptions.CommandOptionsNGTax;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import static nl.wur.ssb.NGTax.App.*;
import static nl.wur.ssb.NGTax.NGTax.*;

class ClassifyASV {
    static final Logger logger = LogManager.getLogger(ClassifyASV.class);
    public static final String dbVersion = "v1.3";

    static void classifyASVs() throws Exception {
        logger.info("Starting ASV classification");
         for (ASV asv : asvs.values()) {
                ClassifyASV.classifyASV(asv);
        }

        logger.info("Starting rejected otu classification");
        int numRejectClassify = 0;
        for (ASV asv : rejectedASVList.values()) {
            numRejectClassify = numRejectClassify + 1;
            if (numRejectClassify <= arguments.rejectedASVAnnotation) {
                ClassifyASV.classifyASV(asv);
            } else {
                break;
            }
        }
    }

    /**
     * ASV classification
     *
     * @param otu -- each ASV
     * @throws Exception
     */
    private static <U> Iterator<ASV> classifyASV(ASV otu) throws Exception {
        // Collect the matches for each level of mismatches to reference database
        logger.debug("classifying otu: " + otu.id);


        // Sorted list based on either the forward or reverse sequence
        LinkedList<EntryDB> refDbForwardLocal = new LinkedList<>(App.refDbForward);
        LinkedList<EntryDB> refDbReverseLocal = new LinkedList<>(App.refDbReverse);

        // long time = System.currentTimeMillis();
        CommandOptionsNGTax args = App.arguments;
        int maxDif = args.identity85MismatchCount;
        otu.misMatchLevels = new MismatchLevel[maxDif + 1];
        otu.misMatchLevelCounts = new int[maxDif + 1];

        for (int i = 0; i < otu.misMatchLevels.length; i++)
            otu.misMatchLevels[i] = new MismatchLevel(i);

        /*
         * Create a byte array, the size of primer plus sequence length.
         * then, copy primer and sequence to that byte array.
         * Do this for both forward and reverse sequence.
         */

        byte[] forwardSequence = new byte[args.forwardPrimerLength + otu.forwardSequence.length()];
        System.arraycopy(args.forwardPrimerMask, 0, forwardSequence, 0, args.forwardPrimerLength);
        System.arraycopy(encode(otu.forwardSequence.getBytes()), 0, forwardSequence, args.forwardPrimerLength, otu.forwardSequence.length());

        byte[] reverseSequence = new byte[args.reversePrimerLength + otu.reverseSequence.length()];
        System.arraycopy(args.reversePrimerMask, 0, reverseSequence, 0, args.reversePrimerLength);
        System.arraycopy(encode(otu.reverseSequence.getBytes()), 0, reverseSequence, args.reversePrimerLength, otu.reverseSequence.length());


        // If the length is not equal, exit.
        if (refDbForwardLocal.get(0).forwardSeq.length != forwardSequence.length
                || refDbForwardLocal.get(0).reverseSeq.length != reverseSequence.length)
            throw new Exception("INTERNAL error asv length does not match with db entry size: "
                    + refDbForwardLocal.get(0).forwardSeq.length + " - " + forwardSequence.length + " -- "
                    + refDbForwardLocal.get(0).reverseSeq.length + " - " + reverseSequence.length);

        Levenshtein levenshteinForward = new Levenshtein(forwardSequence, maxDif);
        Levenshtein levenshteinReverse = new Levenshtein(reverseSequence, maxDif);

        for (EntryDB entryFWD : refDbForwardLocal) {
                entryFWD.forwardScore = levenshteinForward.getLevenshteinDistanceMask(entryFWD.forwardSeq);
        }

        // Calculate levenshtein distance for reverse sequence in the database
        for (EntryDB entryREV : refDbReverseLocal) {
            // If a hit was found with the forward the value should be > -1
            int misMatchLevel = 0;
            misMatchLevel = entryREV.forwardScore;

            if (misMatchLevel != -1) {
                // Calculates the reverse Levenshtein distance
                if (!args.singleEnd) {
                    int temp = levenshteinReverse.getLevenshteinDistanceMask(entryREV.reverseSeq);

                    // If no hit was found in reverse, this entry is ignored
                    if (temp == -1) {
                        continue;
                    }
                    // When no hit was found, misMatchLevel is decreased by -1
                    misMatchLevel += temp;

                    // if the mismatch level is still greater than the maximum difference allowed
                    if (misMatchLevel > maxDif) {
                        continue;
                    }
                }
            } else {
                continue;
            }

            String taxString = entryREV.taxon[0];

            for (int i = 0; i < 6; i++) {
                taxString += ";" + entryREV.taxon[i + 1];
                ASVMatchStats matchStats = otu.misMatchLevels[misMatchLevel].hitsTaxon[i].get(taxString);
                if (matchStats == null) {
                    // misMatchLevel parameter is just extra info for in the json file
                    matchStats = new ASVMatchStats(taxString, misMatchLevel);
                    otu.misMatchLevels[misMatchLevel].hitsTaxon[i].put(taxString, matchStats);
                }

                matchStats.taxonHitCount += entryREV.abundance;

            }
            otu.misMatchLevels[misMatchLevel].levelCount += entryREV.abundance;

            if (args.includeHitIds)
                otu.misMatchLevels[misMatchLevel].idList.add(entryREV.index);
        }


        // Collect best taxon for each level and calculate the ratio's
        for (MismatchLevel level : otu.misMatchLevels) {
            for (int i = 0; i < 6; i++) {
                int bestCount = 0;
                for (ASVMatchStats matchStats : level.hitsTaxon[i].values()) {
                    matchStats.ratio = ((double) matchStats.taxonHitCount) / ((double) level.levelCount);
                    if (matchStats.taxonHitCount > bestCount) {
                        bestCount = matchStats.taxonHitCount;
                        level.bestTaxon[i] = matchStats;
                    }
                }
                if (args.markIfMoreThen1 && level.hitsTaxon[i].size() > 1)
                    level.bestTaxon[i].tax += "~*";
            }
        }
        // Select hit set with least amount of mismatches except if any of the lower
        // level has more then (10 * difference in mismatch count)
        MismatchLevel level = null;
        for (int i = 0; i < otu.misMatchLevels.length; i++) {
            otu.misMatchLevelCounts[i] = otu.misMatchLevels[i].levelCount;
            if (level == null && otu.misMatchLevels[i].levelCount != 0) {
                level = otu.misMatchLevels[i];
            } else if (level != null && otu.misMatchLevels[i].levelCount > level.levelCount
                    * Math.pow(10, (i - level.mismatchCount))) {
                level = otu.misMatchLevels[i];
            }
        }
        otu.usedMisMatchLevel = level;

        if (level != null && level.mismatchCount < args.identity90MismatchCount) {
            // If the ratio of selected taxon is not above given ratio go 1 level up.

            // Select levels
            int useTaxonLevel = 5; // Species?
            if (level.mismatchCount > 0)
                useTaxonLevel = 4;
            if (level.mismatchCount >= args.identity95MismatchCount) // 7 - (14 - 11)
                useTaxonLevel = 3;
            if (level.mismatchCount >= args.identity92MismatchCount) // 11 - 14
                useTaxonLevel = 2;
            ASVMatchStats finalTaxon = level.bestTaxon[useTaxonLevel];
            for (int i = useTaxonLevel - 1; i >= 0; i--) {
                if (finalTaxon.ratio < args.classifyRatio) {
                    finalTaxon = level.bestTaxon[i];
                    useTaxonLevel--;
                }
            }
            otu.usedTaxonLevel = useTaxonLevel + 2;
            otu.assignedTaxon = finalTaxon.clone();
        } else {
            otu.usedTaxonLevel = -1;
        }
        List<ASV> otuList = new ArrayList<>();

        otuList.add(otu);
        return otuList.iterator();
    }

    static void loadrefdb() throws Exception {
        loadrefdb(false);
    }

    static void loadrefdb(boolean keepAlignment) throws Exception {

//        int forSeqHits = 0;
//        int revSeqHits = 0;
        // TODO Expose if correct
        int barCodeHit = 0;
        int silvaTotalDbSize = 0;
        int silvaPrimerAcceptedDbSize = 0;

        logger.info("Starting with the creation of the reference database.");
        CommandOptionsNGTax args = App.arguments;

        HashMap<Integer, Integer> fTmp = new HashMap<>();
        HashMap<Integer, Integer> rTmp = new HashMap<>();


        boolean fasta = CheckIfFasta(args);

        Integer revSeqHits = null;
        Integer forSeqHits = null;

        if (!fasta) {
            // Function to obtain the index columns
            List<Integer> integerList = getIndexes(args, new SilvaDBScanner(new File(args.refdb)), fTmp, rTmp);
            forSeqHits = integerList.get(0);
            revSeqHits = integerList.get(1);
        }

        File cachedFile;
        File cachedFileFull;
        File lockFile;
        if (!fasta) {
            // This is for alignment file and then we have two options, keep alignment or not
            if (!keepAlignment) {
                cachedFile = new File(args.refdb + "_" + args.forwardPrimer + "_" + args.reversePrimer + "_"
                        + args.forwardReadLength + "_" + args.reverseReadLength + "_" + dbVersion + ".gz");
                cachedFileFull = new File(args.refdb + "_" + args.forwardPrimer + "_" + args.reversePrimer
                        + "_" + args.forwardReadLength + "_" + args.reverseReadLength + "_" + dbVersion + "_full.gz");
                lockFile = new File(cachedFile + ".lock");
            } else {
                cachedFile = new File(args.refdb + "_" + forSeqHits + "-" + args.forwardPrimerLength +"_" + revSeqHits +"-"+ args.reversePrimerLength + "_"
                        + args.forwardReadLength + "-" + args.reverseReadLength + "_" + dbVersion + ".gz");
                cachedFileFull = new File(args.refdb + "_" + forSeqHits + "-" + revSeqHits
                        + "_" + args.forwardReadLength + "-" + args.reverseReadLength + "_" + dbVersion + "_full.gz");
                lockFile = new File(cachedFile + ".lock");
            }
        } else {
            // if its a plain fasta file
            cachedFile = new File(args.refdb + "_" + args.forwardPrimer + "_" + args.reversePrimer
                    + "_" + args.forwardReadLength + "_" + args.reverseReadLength + "_nomismatch_" + args.nomismatch + "_" + dbVersion + "_full.gz");
            cachedFileFull = new File(args.refdb + "_" + args.forwardPrimer + "_" + args.reversePrimer
                    + "_" + args.forwardReadLength + "_" + args.reverseReadLength + "_nomismatch_" + args.nomismatch + "_" + dbVersion + "_full.gz");
            lockFile = new File(cachedFile + ".lock");
        }

        logger.info("Checking if file already exists.. " + cachedFile.getName());

        boolean exists = cachedFile.exists();

        // This genFullDb is an option for Galaxy
        // Still hidden, not included in the options
        if (args.genFullDb) {
            exists &= cachedFileFull.exists();
        }

        long prevSizeCount = -1;
        long lastTime = System.currentTimeMillis();
        int sleepCount = 0;

        // Check if the lockFile still exists.
        while (lockFile.exists()) {

            long sizeCount = -1;

            if (cachedFile.exists())
                sizeCount = cachedFile.length();

            if (prevSizeCount != sizeCount)
                lastTime = System.currentTimeMillis();

            if (System.currentTimeMillis() - lastTime > (8 * 3600 * 1000)) {
                // If for 8 hours nothing changes the file incomplete and we rerun it
                // and overwrite the old files
                exists = false;

                if (cachedFile.exists()) {
                    logger.info("Removing cached file " + cachedFile);
                    cachedFile.delete();
                }

                if (cachedFileFull.exists()) {
                    logger.info("Removing cached file " + cachedFileFull);
                    cachedFileFull.delete();
                }
                break;
            }
            prevSizeCount = sizeCount;
            // try again in 1 minute
            Thread.sleep(60000);
            logger.info("Sleeping: " + sleepCount++);
        }

        // The chance we hit twice in between I consider to be very small i will accept this
        //If the file does not exist
        if (!exists) {
            FileWriter lockWriter = new FileWriter(lockFile);
            lockWriter.write("lock");
            lockWriter.close();

            // Check if input file is fasta or is alignment file
            if (fasta) {
                logger.info("File does not exist, creating file from fasta..");
                Database.creation(args);
            } else {
                logger.info("File does not exist, creating file from alignment..");
//                SilvaDBScanner refdbIn = new SilvaDBScanner(new File(args.refdb));

                Writer dbPS = null;
                Writer dbPSFull = null;
                Writer log = null;

                try {
//                    RandomAccessFile randomAccessFile = new RandomAccessFile(
//                            new File(args.refdb + "_" + args.forwardPrimer + "_" + args.reversePrimer + "_"
//                                    + args.forwardReadLength + "_" + args.reverseReadLength + "_" + dbVersion + ".gz"), "rw");

                    log = new FileWriter(new File(args.refdb + "_" + args.forwardPrimer + "_" + args.reversePrimer
                            + "_" + args.forwardReadLength + "_" + args.reverseReadLength + "_" + dbVersion + ".log"));


                    // Primers found, creating sub database...
                    if (!fasta) {
                        logger.info("The locations are: Forward: " + forSeqHits + " and Reverse: " + revSeqHits);
                        log.write("The locations are: Forward: " + forSeqHits + " and Reverse: " + revSeqHits);
                        logger.info("Writing to file now..");
                        log.close();
                    }

                    dbPS = new OutputStreamWriter(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(cachedFile))));
                    dbPSFull = null;

                    if (args.genFullDb)
                        dbPSFull = new OutputStreamWriter(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(cachedFileFull))));

                    SilvaDBScanner refdbIn = new SilvaDBScanner(new File(args.refdb));

                    int forwardReadLength = args.forwardReadLength + args.forwardPrimerLength;
                    int reverseReadLength = args.reverseReadLength + args.reversePrimerLength;

                    while (refdbIn.hasNext()) {
                        silvaTotalDbSize++;
                        if (silvaTotalDbSize % 100000 == 0) {
                            logger.info("Parsed " + silvaTotalDbSize + " entries");
//                            break;
                        }
                        String seq = refdbIn.next();
                        String taxa[] = refdbIn.getTaxa();
                        // String id = refdbIn.getId();
                        String lineage = String.join(";", taxa);

                        seq = seq.replace(".", "-");

                        String forwardFull = getSeqForLength(seq.substring(forSeqHits), forwardReadLength);
                        String reverseFull = getSeqRevLength(seq.substring(0, seq.length() - revSeqHits), reverseReadLength);
                        if (reverseFull != null && forwardFull != null) {
                            if (!args.singleEnd && forwardFull != null && reverseFull != null) {
                                String forwardFinal;
                                String reverseFinal;
                                if (keepAlignment) {
                                    forwardFinal = forwardFull.replace('U', 'T');
                                    reverseFinal = revComplementRNA(reverseFull).replace('U', 'T');
                                } else {
                                    forwardFinal = removeAllBlanks(forwardFull).replace('U', 'T');
                                    reverseFinal = revComplementRNA(removeAllBlanks(reverseFull)).replace('U', 'T');
                                }
                                dbPS.write(forwardFinal + "\t" + reverseFinal.replace('U', 'T') + "\t" + lineage + "\n");
                                // +
                                // "\t"
                                // +
                                // id
                                silvaPrimerAcceptedDbSize++;
                                if (args.genFullDb) {
                                    String forwardFullFinal = forwardFull.replace('U', 'T');
                                    String reverseFullFinal = revComplementRNA(reverseFull).replace('U', 'T');
                                    dbPSFull.write(forwardFullFinal + "\t" + reverseFullFinal + "\t" + lineage + "\n");// +
                                    // "\t"
                                    // +
                                    // id
                                }
                            } else if (args.singleEnd && forwardFull != null) {
                                String forwardFinal;
                                String reverseFinal;
                                if (keepAlignment) {
                                    forwardFinal = forwardFull.replace('U', 'T');
                                    reverseFinal = revComplementRNA(reverseFull).replace('U', 'T');
                                } else {
                                    forwardFinal = removeAllBlanks(forwardFull).replace('U', 'T');
                                    reverseFinal = revComplementRNA(removeAllBlanks(reverseFull)).replace('U', 'T');
                                }
                                if (forSeqHits != 0) {
                                    dbPS.write(forwardFinal + "\t" + org.apache.commons.lang3.StringUtils.repeat("x", forwardFinal.length()) + "\t" + lineage + "\n");
                                } else if (revSeqHits != 0) {
                                    dbPS.write(reverseFinal.replace('U', 'T') + "\t" + org.apache.commons.lang3.StringUtils.repeat("x", forwardFinal.length()) + "\t" + lineage + "\n");
                                }
                                // +
                                // "\t"
                                // +
                                // id
                                silvaPrimerAcceptedDbSize++;
                                if (args.genFullDb) {

                                    String forwardFullFinal = forwardFull.replace('U', 'T');
                                    String reverseFullFinal = revComplementRNA(reverseFull).replace('U', 'T');
                                    dbPSFull.write(forwardFullFinal + "\t" + reverseFullFinal + "\t" + lineage + "\n");// +
                                    // "\t"
                                    // +
                                    // id
                                }
//                        String forwardFinal = removeAllBlanks(forwardFull).replace('U', 'T');
//
//                        dbPS.write(forwardFinal + "\t" + "\t" + lineage + "\n");
//                        silvaPrimerAcceptedDbSize++;
//
//                        if (args.genFullDb) {
//                            String forwardFullFinal = forwardFull.replace('U', 'T');
//                            dbPSFull.write(forwardFullFinal + "\t" + "\t" + lineage + "\n");// +
//                            // "\t"
//                            // +
//                            // id
//                        }
                            }
                        }
                    }
                    dbPS.close();

                    // Only when not keeping the alignment (ngtax run, not silva tree build)
                    if (!keepAlignment) {
                        // Merge redundant sequences and taxonomic assignments TODO sort uniq -c...
                        InputStream fileStream = new FileInputStream(cachedFile);
                        InputStream gzipStream = new GZIPInputStream(fileStream);
                        Reader decoder = new InputStreamReader(gzipStream);
                        BufferedReader buffered = new BufferedReader(decoder);

                        String content;
                        LinkedHashMap<String, Integer> dbPSmerged = new LinkedHashMap<>();
                        while ((content = buffered.readLine()) != null) {
                            content = content.trim();
                            if (dbPSmerged.containsKey(content)) {
                                dbPSmerged.put(content, dbPSmerged.get(content) + 1);
                            } else {
                                dbPSmerged.put(content, 1);
                            }
                        }
                        // Write to file...
                        dbPS = new OutputStreamWriter(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(cachedFile))));

                        for (String key : dbPSmerged.keySet()) {
                            dbPS.write(key + "\t" + dbPSmerged.get(key) + "\n");
                        }
                        dbPS.close();

                        logger.info("Data written to file");

                        if (args.genFullDb) {
                            dbPSFull.close();
                        }

                        refdbIn.close();
//                    randomAccessFile.close();
                    }
                } catch (Throwable th) {
                    System.err.println(">" + th.getMessage());
                    System.err.println(">" + StringUtils.join(th.getStackTrace(), "\n"));
                    if (dbPS != null)
                        dbPS.close();
                    if (dbPSFull != null)
                        dbPSFull.close();
                    if (log != null)
                        log.close();
                    if (cachedFile.exists())
                        cachedFile.delete();
                    if (cachedFileFull.exists())
                        cachedFileFull.delete();
                } finally {
                    if (lockFile.exists())
                        lockFile.delete();
                }
            }
            if (lockFile.exists())
                lockFile.delete();
        }

        if (!keepAlignment) {
            logger.info("File exists, loading into memory.."); // If the database file exists
            loadrefdbDirect(cachedFile);
        }
    }

    public static List<Integer> getIndexes(CommandOptionsNGTax args, SilvaDBScanner refdbIn, HashMap<Integer, Integer> fTmp, HashMap<Integer, Integer> rTmp) throws Exception {
        int fCounter = 0;
        int rCounter = 0;
        // Turn this into a variable
        int maxCount = args.maxCount;

        Integer forSeqHits = 0;
        Integer revSeqHits = 0;

        Pattern fP = Pattern.compile(args.forwardPrimer.replace("U", "T"));

        while (fCounter < maxCount || rCounter < maxCount) {
            if (!refdbIn.hasNext()) {
                throw new Exception(
                        "Primer rejected at least a "+args.maxCount+" db hits should be found forward count: " + fCounter
                                + " reverse counter: " + rCounter);
            }

            String fullSequence = refdbIn.next();
            String sequence = fullSequence.replaceAll("[.-]", ""); // replace all dot with empty string
            Matcher fM = fP.matcher(sequence);

            if (fM.find()) {

                // Position on alignment sequence
                int pos = 0;
                for (int i = 0; i < fullSequence.length(); i++) {
                    char c = fullSequence.charAt(i);

                    if ("ATGC".indexOf(c) >= 0) {
                        // if (pos == fM.end(0)) | if we do not include primer sequence
                        if (pos == fM.start(0)) {
                            // i--; | if we do not include primer sequence
                            int count = fTmp.containsKey(i) ? fTmp.get(i) : 0;
                            fTmp.put(i, count + 1);
                            break;
                        }
                        pos++;
                    }
                }
                fCounter++;

                if (args.singleEnd && fCounter > maxCount)
                    break;
            }

            // REVERSE THE PRIMER

            String revSeq = revComplementRNA(sequence);
            Pattern rP;

            if (args.singleEnd) {
                rP = Pattern.compile(args.forwardPrimer.replace('U', 'T'));
                Matcher rM = rP.matcher(revSeq);

                if (rM.find()) {

                    // Position on alignment sequence
                    int pos = 0;
                    String revFullSeq = revComplementRNA(fullSequence);
                    for (int i = 0; i < revFullSeq.length(); i++) {
                        char c = revFullSeq.charAt(i);

                        if ("ATGC".indexOf(c) >= 0) {
                            // if (pos == fM.end(0)) | if we do not include primer sequence
                            if (pos == rM.start(0)) {
                                // i--; | if we do not include primer sequence
                                int count = rTmp.containsKey(i) ? rTmp.get(i) : 0;
                                rTmp.put(i, count + 1);
                                break;
                            }
                            pos++;
                        }
                    }
                    rCounter++;

                    if (args.singleEnd && rCounter > maxCount)
                        break;
                }
            } else {
                rP = Pattern.compile(args.reversePrimer.replace("U", "T"));
                Matcher rM = rP.matcher(revSeq);
                if (rM.find() && !args.reversePrimer.equals("")) {

                    // Position on alignment sequence
                    int pos = 0;
                    String revFullSeq = revComplementRNA(fullSequence);
                    for (int i = 0; i < revFullSeq.length(); i++) {
                        char c = revFullSeq.charAt(i);

                        if ("ATGC".indexOf(c) >= 0) {
                            // if (pos == fM.end(0)) | if we do not include primer sequence
                            if (pos == rM.start(0)) {
                                // i--; | if we do not include primer sequence
                                int count = rTmp.containsKey(i) ? rTmp.get(i) : 0;
                                rTmp.put(i, count + 1);
                                break;
                            }
                            pos++;
                        }
                    }
                    rCounter++;
                }
            }
        }

        if (fTmp.size() != 0) {
            int maxValueInFMap = (Collections.max(fTmp.values()));
            for (Map.Entry<Integer, Integer> entry : fTmp.entrySet()) { // Iterate through hashmap
                if (entry.getValue() == maxValueInFMap) {
                    forSeqHits = entry.getKey();
                }
            }
        } else {
            forSeqHits = 0;
        }

        if (rTmp.size() != 0) {
            int maxValueInRMap = (Collections.max(rTmp.values()));
            for (Map.Entry<Integer, Integer> entry : rTmp.entrySet()) { // Iterate through hashmap
                if (entry.getValue() == maxValueInRMap) {
                    revSeqHits = entry.getKey();
                }
            }
        } else {
            revSeqHits = 0;
        }

        logger.info("Forward primer was found: " + fCounter + " times at position " + forSeqHits);
//        for (Integer i : fTmp.keySet()) {
//            logger.info(i + "\t" + fTmp.get(i));
//        }
        logger.info("Reverse primer was found: " + rCounter + " times at position " + revSeqHits);
//        for (Integer i : rTmp.keySet()) {
//            logger.info(i + "\t" + rTmp.get(i));
//        }

        logger.info("Primers detected at position " + forSeqHits + " and " + revSeqHits);
        refdbIn.close();
        return Arrays.asList(forSeqHits, revSeqHits);
    }

    // Method to check if input file is a fasta with or without alignments
    private static boolean CheckIfFasta(CommandOptionsNGTax args) throws Exception {
        SilvaDBScanner refdbIn = new SilvaDBScanner(new File(args.refdb));
        refdbIn.next();
        String sequence = refdbIn.getSequence();
        if (sequence.contains("\\.") || sequence.contains("-")) {
            logger.info("This is a multiple alignment file");
            return false;
        }
        logger.info("This is an unaligned fasta file");
        return true;
    }


    private static void loadrefdbDirect(File cashedFile) throws Exception {
        /*
         * RandomAccessFile randomAccessFile = new RandomAccessFile(new File(args.refdb_obsolete + "_" +
         * args.forwardPrimer + "_" + args.reversePrimer + "_" + args.forwardReadLength + "_" +
         * args.reverseReadLength + "_v0_3.gz"),"rw"); FileChannel channel =
         * randomAccessFile.getChannel(); synchronized (channel) { App.logger.info("Waiting");
         * channel.wait(); } randomAccessFile.close();
         */
        if (!cashedFile.exists()) {
            throw new Exception("Cannot find the reference database file " + cashedFile.getAbsolutePath());
        }

        Scanner refdb = new Scanner(new GZIPInputStream(new BufferedInputStream(new FileInputStream(cashedFile))));
        int count = 0;
        while (refdb.hasNextLine()) {
            String line = refdb.nextLine();
            String lineSplit[] = line.split("\t");
            App.refDb.add(new EntryDB(count++, encodeForDb2(lineSplit[0]), encodeForDb2(lineSplit[1]), lineSplit[2].split(";"), Integer.parseInt(lineSplit[3])));
        }
        long time = System.currentTimeMillis();
        EntryDB[] entryDBSArray = App.refDb.toArray(new EntryDB[0]);

        // Sort the database based on the forward sequence
        Arrays.sort(entryDBSArray, new Comparator<>() {
            @Override
            public int compare(EntryDB o1, EntryDB o2) {
                for (int i = 0; i < o1.forwardSeq.length; i++) {
                    if (o1.forwardSeq[i] != o2.forwardSeq[i])
                        return o1.forwardSeq[i] > o2.forwardSeq[i] ? 1 : -1;
                }
                return 0;
            }
        });

        for (EntryDB item : entryDBSArray) {
            App.refDbForward.add(item);
        }

        // Sort the database based on the reverse sequence
        Arrays.sort(entryDBSArray, new Comparator<>() {
            @Override
            public int compare(EntryDB o1, EntryDB o2) {
                for (int i = 0; i < o1.reverseSeq.length; i++) {
                    if (o1.reverseSeq[i] != o2.reverseSeq[i])
                        return o1.reverseSeq[i] > o2.reverseSeq[i] ? 1 : -1;
                }
                return 0;
            }
        });

        for (EntryDB item : entryDBSArray) {
            App.refDbReverse.add(item);
        }

        logger.debug("sort time = " + (System.currentTimeMillis() - time));
        logger.info("Size of reference database forward " + refDbForward.size() + " and reverse " + refDbReverse.size());
        if (refDbReverse.size() == 0 || refDbForward.size() == 0) {
            throw new Exception("The primers did not match to any sequence in the database. Lookup database failed...");
        }
        refdb.close();
    }

    static String revComplementRNA(String seq) {
        StringBuilder builder = new StringBuilder();
        builder.ensureCapacity(seq.length());

        // int count = 0;
        for (int i = seq.length() - 1; i >= 0; i--) {
            char c = seq.charAt(i);
            builder.append(c == 'A' ? 'T' : (c == 'T' ? 'A' : (c == 'G' ? 'C' : (c == 'C' ? 'G' : c))));
        }
        return builder.toString();
    }
}
