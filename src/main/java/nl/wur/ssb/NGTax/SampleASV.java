package nl.wur.ssb.NGTax;

import com.google.gson.annotations.Expose;

/**
 * Create SampleASV, a unique sequence with in the sample.
 */

public class SampleASV implements Comparable<SampleASV> {
    //otu id, the most common otu has id 1
    @Expose
    public int id;
    @Expose
    public String forwardSequence;
    @Expose
    public String reverseSequence;
    //number of reads that exactly match this otu
    @Expose
    public int readCount = 1;

    //ratio with exact match
    @Expose
    public double ratio;

    //number of reads including the clustered reads that match this otu
    @Expose
    public int clusteredReadCount;

    //TODO filter ratio
    //The id of the master(shared otu among all samples) otu
    @Expose
    public String masterOtuId;

    /**
     * Initiate the SampleOTU with forward and reverse sequence
     *
     * @param forwardSequence -- forward sequence
     * @param reverseSequence -- reverse sequence
     */
    public SampleASV(String forwardSequence, String reverseSequence) {
        super();
        this.forwardSequence = forwardSequence;
        this.reverseSequence = reverseSequence;
    }

    @Override
    public int compareTo(SampleASV o) {
        if (this.readCount == o.readCount)
            return 0;
        return this.readCount < o.readCount ? 1 : -1;
    }
}
