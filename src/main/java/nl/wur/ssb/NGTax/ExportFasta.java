package nl.wur.ssb.NGTax;

import nl.wur.ssb.RDFSimpleCon.ExecCommand;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ExportFasta {
    private ExportFastaOptions args;
    static final Logger logger = LogManager.getLogger(App.class);

    ExportFasta(ExportFastaOptions args) {
        this.args = args;
    }

    void export() throws Exception {

        new NGTaxResult();
        NGTax biom1 = NGTaxResult.fromJSon(FileUtils.readFileToString(new File(args.input), StandardCharsets.UTF_8)).provData;
        ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(args.output));
        ZipEntry entry = new ZipEntry("all_otus.fasta");
        zos.putNextEntry(entry);
        String res = "";

        for (ASV otu : biom1.asvList.values()) {
            res += ">" + otu.id + "\n";
            res += otu.forwardSequence + otu.reverseSequence + "\n";
        }
        zos.write(res.getBytes());

        if (args.createTree) {

            boolean foundClustalO = true;
            try {
                ExecCommand execCommand = new ExecCommand("clustalo --version");
                logger.info("Found clustalo version " + execCommand.getOutput());
            } catch (Exception e) {
                foundClustalO = false;
            }

            if (args.createTree && !foundClustalO) {
                logger.error("clustalo not found, createTree option ignored. Please install clustalo in the path.");
                args.createTree = false;
            }

            createTree(zos, "all_otus", res);
            for (Sample sample : biom1.samples) {
                logger.info("Processing " + sample.sampleName);
                entry = new ZipEntry(sample.sampleName + ".fasta");
                zos.putNextEntry(entry);
                res = "";
                for (SampleASV sampleOtu : sample.otus) {
                    ASV otu = biom1.asvList.get(sampleOtu.masterOtuId);
                    res += ">" + otu.id + "\n";
                    res += otu.forwardSequence + otu.reverseSequence + "\n";
                }
                zos.write(res.getBytes());
                createTree(zos, sample.sampleName, res);
            }
        }
        zos.close();
    }

    private void createTree(ZipOutputStream zos, String name, String data) throws Exception {
        if (args.createTree) {
            int count = count(data);
            if (count <= 2) {
                logger.info("Sample with " + count + " sequences, skipping tree building");
                return;
            }
            logger.info("Starting clustalo");
            (new File("temp")).mkdir();
            FileUtils.writeStringToFile(new File("temp/temp.fasta"), data);
            String command = "clustalo -i temp/temp.fasta --guidetree-out=./temp/out.tree --outfmt=clu --force";
            logger.info("Executing: " + command);
            ExecCommand execCommand = new ExecCommand(command);
            if (execCommand.getExit() != 0 || !execCommand.getError().equals("")) {
                throw new Exception("create tree failed exit code: " + execCommand.getExit() + "\n" + execCommand.getError());
            }

            File tempIn = new File("./temp/out.tree");
            if (tempIn.exists()) {
                ZipEntry entry = new ZipEntry(name + ".tree");
                zos.putNextEntry(entry);
                zos.write(FileUtils.readFileToString(tempIn).getBytes());
                tempIn.delete();
            } else {
                throw new Exception("clustalo did not generated a output file");
            }
            (new File("temp/temp.fasta")).delete();
            (new File("temp")).delete();
        }
    }

    public int count(String data) {
        int counter = 0;
        for (int i = 0; i < data.length(); i++) {
            if (data.charAt(i) == '>') {
                counter++;
            }
        }
        return counter;
    }
}
