package nl.wur.ssb.NGTax;

import com.google.gson.annotations.Expose;

import java.io.Writer;

/**
 * Library of details from the input file(s)
 * Details kept are as follows: index, forward file, reverse file, forward barcode file and reverse barcode file
 */

public class Library {
    @Expose
    int index;
    @Expose
    String forwardFile;
    @Expose
    String reverseFile;
    @Expose
    String forwardBarcodeFile;
    @Expose
    String reverseBarcodeFile;
    @Expose
    int totalReads;
    @Expose
    int primerHitsAccepted;
    @Expose
    double primerHitsAcceptedRatio;
    @Expose
    int barcodeHitsAccepted;
    @Expose
    double barcodeHitsAcceptedRatio;
    @Expose
    double acceptedSameBarCodeRatio;
    @Expose
    int fBarCodeLength;
    @Expose
    int rBarCodeLength;

    Writer forWriter;
    Writer revWriter;


    public Library(int index, String forwardFile, String reverseFile, String forwardBarcodeFile, String reverseBarcodeFile) {
        this.index = index;
        this.forwardFile = forwardFile;
        this.reverseFile = reverseFile;
        if (forwardBarcodeFile != null && forwardBarcodeFile.equals("null"))
            forwardBarcodeFile = null;
        this.forwardBarcodeFile = forwardBarcodeFile;
        if (reverseBarcodeFile != null && reverseBarcodeFile.equals("null"))
            reverseBarcodeFile = null;
        this.reverseBarcodeFile = reverseBarcodeFile;
    }

    public boolean equals(Library other) {
        return this.index == other.index && this.forwardFile.equals(other.forwardFile)
                && this.reverseFile.equals(other.reverseFile)
                && (this.forwardBarcodeFile != null ? this.forwardBarcodeFile : "null").equals(other.forwardBarcodeFile != null ? other.forwardBarcodeFile : "null")
                && (this.reverseBarcodeFile != null ? this.reverseBarcodeFile : "null").equals(other.reverseBarcodeFile != null ? other.reverseBarcodeFile : "null");
    }
}
