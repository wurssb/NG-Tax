package nl.wur.ssb.NGTax;

import com.google.gson.annotations.Expose;
import nl.wur.ssb.NGTax.CommandOptions.CommandOptionsNGTax;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.*;

public class NGTax {
    private static final int BUFFER_SIZE = 4096;
    @Expose
    public List<Sample> samples = new ArrayList<>();
    // keep
    @Expose
    public HashMap<Integer, Library> libraries = new HashMap<>();
    // keep
    @Expose
    public HashMap<String, ASV> asvList = new HashMap<>();

    @Expose
    public HashMap<String, ASV> otuList = new HashMap<>();

    @Expose
    public HashMap<String, ASV> rejectedASVList = new HashMap<>();

    @Expose
    public CommandOptionsNGTax args;

    private HashMap<String, Sample> libBarCodeToSample = new HashMap<>();

    // TODO Expose if correct
    private int barCodeHit = 0;
    @Expose
    private LinkedList<Taxon> foundTaxons = new LinkedList<>();
    private List<String> filesListInDir = new ArrayList<>();

    static final Logger logger = LogManager.getLogger(NGTax.class);

    public NGTax() {

    }

    public NGTax(CommandOptionsNGTax arguments) {
        this.args = arguments; //assign arguments to variable args.
    }

    /**
     * Remove all the '-' symbols
     *
     * @param in -- input sequence
     * @return
     */
    static String removeAllBlanks(String in) {
        StringBuilder builder = new StringBuilder();
        builder.ensureCapacity(in.length());
        for (int i = 0; i < in.length(); i++) {
            char c = in.charAt(i);
            if (c != '-')
                builder.append(c);
        }
        return builder.toString();
    }

    /**
     * Count forward sequence length, only nucleotides
     *
     * @param in    -- input sequence
     * @param count -- forward sequence length specify by the user
     * @return
     */
    static String getSeqForLength(String in, int count) {
        for (int i = 0; i < in.length(); i++) {
            if (in.charAt(i) != '-') {
                count--;
                if (count == 0) {
                    return in.substring(0, i + 1);
                }
            }
        }
        return null;
    }

    /**
     * Cout reverse sequence length, only nucleotides
     *
     * @param in    -- input sequence
     * @param count -- reverse sequence length specify by the user
     * @return
     */
    static String getSeqRevLength(String in, int count) {
        for (int i = in.length() - 1; i >= 0; i--) {
            if (in.charAt(i) != '-') {
                count--;
                if (count == 0) {
                    return in.substring(i, in.length());
                }
            }
        }
        return null;
    }

    /**
     * Encode the sequence
     * T > B, G > D, C > H and leave A be.
     *
     * @param seq -- sequence
     * @return
     */
    static byte[] encode(byte[] seq) {
        byte toRet[] = new byte[seq.length];

        for (int i = 0; i < seq.length; i++) {
            byte c = seq[i];
            toRet[i] = (byte) ((c == ((byte) 'T') ? ((byte) 'B')
                    : (c == ((byte) 'G') ? ((byte) 'D') : (c == ((byte) 'C') ? ((byte) 'H') : c))) & 0x0f);
        }
        return toRet;
    }

    private void openSampleFastaFile() throws Exception {
        for (Sample sample : this.samples) {
            String key = sample.libraryNum + sample.fBarcode + sample.rBarcode;
            if (libBarCodeToSample.containsKey(key)) {
                Sample other = libBarCodeToSample.get(key);
                throw new Exception(
                        "2 samples with the same barcode: " + sample.sampleName + " (" + sample.libraryNum + ":"
                                + sample.fBarcode + ":" + sample.rBarcode + ") <-> " + other.sampleName + " ("
                                + other.libraryNum + ":" + other.fBarcode + ":" + other.rBarcode + ")");
            }
            libBarCodeToSample.put(key, sample);
            sample.writer =  new BufferedWriter(new FileWriter("total_sample_files/" + sample.sampleName + ".fasta"));
        }
        if (args.fastQfiles) {
            for (Library lib : this.libraries.values()) {
                lib.forWriter =
                        new BufferedWriter(new FileWriter("fastQFiles/" + lib.index + "_forward.fasta"));
                lib.revWriter =
                        new BufferedWriter(new FileWriter("fastQFiles/" + lib.index + "_reverse.fasta"));
            }
        }
    }

    /**
     * Add ASV to the ASV class
     *
     * @param otu
     */
    private void addOTU(ASV otu) {
        App.asvs.put(otu.id, otu);
    }

    /**
     * Add rejected ASV to the ASV class
     */
    private void addRjectedOTU(ASV otu) {
        App.rejectedASVList.put(otu.id, otu);
    }


    private void closeSampleFastaFile() throws Exception {
        for (Sample sample : this.samples) {
            sample.writer.close();
            sample.writer = null;
        }
        if (args.fastQfiles) {
            for (Library lib : this.libraries.values()) {
                lib.forWriter.close();
                lib.forWriter = null;
                lib.revWriter.close();
                lib.revWriter = null;
            }
        }
    }

    private void mapToSample(String fastQReadName, int libNumber, String fBarCode, String rBarCode,
                                String forFilterdSequence, String revFilterdSequence) throws Exception {

        Sample sample;
        if (rBarCode != null) {
            sample = this.libBarCodeToSample.get(libNumber + fBarCode + rBarCode);
        }
        else {
            sample = this.libBarCodeToSample.get(libNumber + fBarCode + fBarCode);
        }
        if (sample != null) {
            sample.writer.write(fastQReadName.replace("@", ">") + "\n" + forFilterdSequence + revFilterdSequence + "\n");
            this.barCodeHit++;
        }
    }
    private Scanner createFastQScanner(String fileName) throws IOException {
        if (new File(fileName).length() == 0) {
            throw new IOException("Empty input (fastq) file detected, " + fileName);
        }
        try {
            return new Scanner(new GZIPInputStream(new BufferedInputStream(new FileInputStream(fileName))));
        } catch (ZipException bz) {
            try {
//                return new Scanner(new BZip2CompressorInputStream(new BufferedInputStream(new FileInputStream(fileName))));
//                return new Scanner(new BZip2CompressorInputStream(new FileInputStream(fileName)));
                return new Scanner(new MultiStreamBZip2InputStream(new FileInputStream(fileName)));
            } catch (IOException e) {
                return new Scanner(new BufferedInputStream(new FileInputStream(fileName)));
            }
        }
    }



//    private Scanner createFastQScanner(String fileName) throws IOException {
//        if (new File(fileName).length() == 0) {
//            throw new IOException("Empty input (fastq) file detected, " + fileName);
//        }
//        Scanner inputFile = null;
//        try {
//            if (fileName.endsWith(".bz2")) {
//                inputFile = new Scanner(new BZip2CompressorInputStream(new BufferedInputStream(new FileInputStream(fileName))));
//            } else if (fileName.endsWith(".gz")){
//                inputFile = new Scanner(new GZIPInputStream(new BufferedInputStream(new FileInputStream(fileName))));
//            }
//        } catch (ZipException e) {
//            return new Scanner(new BufferedInputStream(new FileInputStream(fileName)));
//        }
//        return inputFile;
//    }

    /**
     * Writes FASTQ files using the libraries number object
     *
     * @param libNumber
     * @param fBarCode
     * @param rBarCode
     * @param forHeader
     * @param revHeader
     * @param forSequence
     * @param revSequence
     * @param forScore
     * @param revScore
     * @return
     * @throws Exception
     */
    private boolean writeFastQ(int libNumber, String fBarCode, String rBarCode, String forHeader,
                               String revHeader, String forSequence, String revSequence, String forScore, String revScore)
            throws Exception {

        Sample sample = this.libBarCodeToSample.get(libNumber + fBarCode + rBarCode);

        if (sample != null) {
            logger.debug("Writer: " + this.libraries.get(sample.libraryNum).forWriter);

            this.libraries.get(sample.libraryNum).forWriter
                    .write(forHeader + "\n" + forSequence + "\n+\n" + forScore + "\n");
            this.libraries.get(sample.libraryNum).revWriter
                    .write(revHeader + "\n" + revSequence + "\n+\n" + revScore + "\n");
            return true;
        }
        return false;
    }

    /**
     * Add absolute path of all file in the directory
     *
     * @param dir -- directory
     */
    private void populateFilesList(File dir) {
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isFile())
                filesListInDir.add(file.getAbsolutePath());
            else
                populateFilesList(file);
        }
    }

    /**
     * Zip the directory
     *
     * @param dir        -- directory
     * @param zipDirName -- output name
     */
    void zipDirectory(File dir, String zipDirName) {
        try {
            populateFilesList(dir); // add file's path within the directory.
            // now zip files one by one
            // create ZipOutputStream to write to the zip file
            FileOutputStream fos = new FileOutputStream(zipDirName);
            ZipOutputStream zos = new ZipOutputStream(fos);
            for (String filePath : filesListInDir) // go through each file in filesListInDir
            {
                logger.info("Zipping " + filePath);
                // for ZipEntry we need to keep only relative file path, so we used
                // substring on absolute path
                ZipEntry ze =
                        new ZipEntry(filePath.substring(dir.getAbsolutePath().length() + 1, filePath.length()));
                zos.putNextEntry(ze);
                // read the file and write to ZipOutputStream
                FileInputStream fis = new FileInputStream(filePath);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                zos.closeEntry();
                fis.close();
            }
            zos.close();
            fos.close();

            // Delete original files after zipped
            for (String filePath : filesListInDir) {
                logger.info("Deleting " + filePath);
                File file = new File(filePath);
                file.delete();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void filterFastQFilesSingle() throws Exception {
        logger.warn("Running single-end read analysis");
        // Checking if the barcode pairs are unique
        // Creating writer for fasta files
        openSampleFastaFile();

        /*
         * int forwardReadLength = args.for_read_len; int reverseReadLength = args.rev_read_len; String
         * forwardPrimer = args.for_p; String reversePrimer = args.rev_p; int barCodeLength = args.l;
         */

        logger.info("Creating filtered library");

        Pattern forwardPattern = Pattern.compile(args.forwardPrimer + "([ATCG]{" + args.forwardReadLength + "})");
        for (int libNumber : this.libraries.keySet()) {
            logger.info("Now busy with library: " + libNumber + "/" + this.libraries.keySet().size());
            int totalLength = 0;
            int primerCorrect = 0;
            this.barCodeHit = 0;
            int sameBarCodeCount = 0;
            Library lib = this.libraries.get(libNumber);
            Scanner fastQForward = createFastQScanner(lib.forwardFile);

            Scanner forwardBarcodeFile = null;
            if (lib.forwardBarcodeFile != null)
                forwardBarcodeFile = createFastQScanner(lib.forwardBarcodeFile);

            // includes a auto detect to detect fasta files instead of fastQfiles
            boolean isFasta = false;
            String tempNext = null;
            while (fastQForward.hasNextLine()) {
                String forLineHeader = tempNext;
                if (tempNext != null)
                    tempNext = null;
                else
                    forLineHeader = fastQForward.nextLine();

                String forLineSequence = fastQForward.nextLine();

                String forLinePlus = "";
                String forLineQuality = "";

                if (!isFasta) {
                    forLinePlus = fastQForward.nextLine();
                    if (forLinePlus.startsWith(">")) {
                        isFasta = true;
                        tempNext = forLinePlus;
                    } else {
                        forLineQuality = fastQForward.nextLine();
                    }
                }
                totalLength++;
                // Check if the adaptors are same on both ends, otherwise is wrong
                // anyhow and we drop the read
                String[] name = forLineHeader.split("\\s+");
                // Match forward and reverse primers, one sequencing error and we drop
                // the read
                Matcher forwardMatcher = forwardPattern.matcher(forLineSequence);

                if (forwardBarcodeFile != null) {
                    String fBarcodeHeader = forwardBarcodeFile.nextLine();
                    String fbarcode = forwardBarcodeFile.nextLine();
                    forwardBarcodeFile.nextLine();
                    forwardBarcodeFile.nextLine();
                    if (!fBarcodeHeader.equals(forLineHeader))
                        throw new Exception("Headers for the forward barcode are not the same. Barcode header: " + fBarcodeHeader + "\nFastQ header" + forLineHeader);

                    String rbarcode = null;

                    if (forLineSequence.length() < args.forwardReadLength) // || revLineSequence.length() < args.reverseReadLength)
                        continue;
                    // TODO fix the N in the primers count should go up
                    if (forwardMatcher.find()) { // && reverseMatcher.find()) {
                        mapToSample(name[0], libNumber, fbarcode, rbarcode, forwardMatcher.group(1), null);//reverseMatcher.group(1));
                    }

                    primerCorrect++;
                } else {
                    if (args.primersRemoved) {
                        if (forLineSequence.length() >= args.forwardReadLength) {
                            String forSequence = forLineSequence.substring(0, args.forwardReadLength);
                            mapToSample(name[0], libNumber, forLineSequence.substring(0, lib.fBarCodeLength),
                                    null,//revLineSequence.substring(0, lib.rBarCodeLength),
                                    forSequence,
                                    null); //revLineSequence.substring(0, args.reverseReadLength));
                        } else {
                            logger.warn("Sequence shorter than read length issue detected");
                        }
                    } else if (forwardMatcher.find()) { // && reverseMatcher.find()) {
                        if (forLineSequence.length() >= lib.fBarCodeLength) {
                            String forSequence = forLineSequence.substring(0, lib.fBarCodeLength);
                            mapToSample(name[0], libNumber, forSequence,
                                    null,//revLineSequence.substring(0, lib.rBarCodeLength),
                                    forwardMatcher.group(1),
                                    null);//reverseMatcher.group(1));
                        } else {
                            logger.warn("Sequence issue with barcode detected");
                        }
                        if (args.fastQfiles) {
                            logger.debug("writeFastQ function");

                            logger.debug(StringUtils.join(new String[]{String.valueOf(libNumber), forLineSequence.substring(0, lib.fBarCodeLength),
                                    null, forLineHeader, null,
                                    forLineSequence, null, forLineQuality, null}, "\t"));
                            writeFastQ(libNumber, forLineSequence.substring(0, lib.fBarCodeLength),
                                    null, //revLineSequence.substring(0, lib.rBarCodeLength),
                                    forLineHeader,
                                    null, //revLineHeader,
                                    forLineSequence,
                                    null, //revLineSequence,
                                    forLineQuality,
                                    null);//revLineQuality);
                        }

                        primerCorrect++;
                        sameBarCodeCount++;
                    }
                }
            }
            fastQForward.close();

            lib.totalReads = totalLength;
            lib.primerHitsAccepted = primerCorrect;
            lib.primerHitsAcceptedRatio = ((double) primerCorrect) / ((double) totalLength);
            lib.barcodeHitsAccepted = this.barCodeHit;
            lib.barcodeHitsAcceptedRatio = ((double) this.barCodeHit) / ((double) totalLength);
            lib.acceptedSameBarCodeRatio = ((double) sameBarCodeCount) / ((double) totalLength);
        }
        closeSampleFastaFile();
    }


    /**
     * Separate samples into libraries and create separate fasta files
     *
     * @throws Exception
     */
    void filterFastQFiles() throws Exception {
        // Checking if the barcode pairs are unique
        // Creating writer for fasta files
        openSampleFastaFile();

        Set<String> possibleErrors = new HashSet<>();

        /*
         * int forwardReadLength = args.for_read_len; int reverseReadLength = args.rev_read_len; String
         * forwardPrimer = args.for_p; String reversePrimer = args.rev_p; int barCodeLength = args.l;
         */

        logger.info("Creating filtered library");
        Pattern forwardPattern = Pattern.compile(args.forwardPrimer + "([ATCG]{" + args.forwardReadLength + "})");
        Pattern reversePattern = Pattern.compile(args.reversePrimer + "([ATCG]{" + args.reverseReadLength + "})");

        for (int libNumber : this.libraries.keySet()) {
            logger.info("Now busy with library: " + libNumber + "/" + this.libraries.keySet().size());
            int totalLength = 0;
            int primerCorrect = 0;
            this.barCodeHit = 0;
            int sameBarCodeCount = 0;
            Library lib = this.libraries.get(libNumber);

            if (new File(lib.forwardFile).exists()) {
                logger.info("Analysing file: " + lib.forwardFile);
            }

            if (lib.reverseFile != null) {
                if (new File(lib.reverseFile).exists()) {
                    logger.info("Analysing file: " + lib.reverseFile);
                }
            }

            Scanner fastQForward = createFastQScanner(lib.forwardFile);
            Scanner fastQReverse = null;

            if (!args.singleEnd)
                fastQReverse = createFastQScanner(lib.reverseFile);

            Scanner forwardBarcodeFile = null;
            Scanner reverseBarcodeFile = null;
            if (lib.forwardBarcodeFile != null) {
                forwardBarcodeFile = createFastQScanner(lib.forwardBarcodeFile);
            }
            if (lib.reverseBarcodeFile != null && !lib.forwardBarcodeFile.equals(lib.reverseBarcodeFile)) {
                reverseBarcodeFile = createFastQScanner(lib.reverseBarcodeFile);
            }

            // includes a auto detect to detect fasta files instead of fastQfiles
            boolean isFasta = false;
            String tempNext = null;
            while (fastQForward.hasNextLine()) {
                String forLineHeader = tempNext;
                if (tempNext != null)
                    tempNext = null;
                else
                    forLineHeader = fastQForward.nextLine();

                String forLineSequence = fastQForward.nextLine();

                String revLineHeader = fastQReverse.nextLine();
                String revLineSequence = fastQReverse.nextLine();

                String forLinePlus = "";
                String forLineQuality = "";

                String revLinePlus = "";
                String revLineQuality = "";

                if (!isFasta) {
                    forLinePlus = fastQForward.nextLine();
                    if (forLinePlus.startsWith(">")) {
                        isFasta = true;
                        tempNext = forLinePlus;
                        forLinePlus = "";
                    } else {
                        forLineQuality = fastQForward.nextLine();

                        revLinePlus = fastQReverse.nextLine();
                        revLineQuality = fastQReverse.nextLine();
                    }
                }
                totalLength++;
                // Check if the adaptors are same on both ends, otherwise is wrong
                // anyhow and we drop the read
                String[] name = forLineHeader.split("\\s+");
                // Match forward and reverse primers, one sequencing error and we drop
                // the read
                Matcher forwardMatcher = forwardPattern.matcher(forLineSequence);
                Matcher reverseMatcher = reversePattern.matcher(revLineSequence);

                if (forwardBarcodeFile != null) {
                    String fBarcodeHeader = forwardBarcodeFile.nextLine();
                    String fbarcode = forwardBarcodeFile.nextLine();
                    forwardBarcodeFile.nextLine();
                    forwardBarcodeFile.nextLine();
                    if (!fBarcodeHeader.equals(forLineHeader))
                        throw new Exception("Headers for the forward barcode are not the same. Barcode header: " + fBarcodeHeader + "\nFastQ header" + forLineHeader);

                    String rbarcode = null;

                    if (reverseBarcodeFile != null) {
                        String rBarcodeHeader = reverseBarcodeFile.nextLine();
                        rbarcode = reverseBarcodeFile.nextLine();
                        reverseBarcodeFile.nextLine();
                        reverseBarcodeFile.nextLine();
                        if (!rBarcodeHeader.equals(revLineHeader))
                            throw new Exception("Headers for the reverse barcode are not the same. Barcode header: " + rBarcodeHeader + "\nFastQ header" + revLineHeader);
                    }

                    if (rbarcode == null)
                        rbarcode = fbarcode;

                    if (forLineSequence.length() < args.forwardReadLength
                            || revLineSequence.length() < args.reverseReadLength)
                        continue;
                    // TODO fix the N in the primers count should go up
                    if (forwardMatcher.find() && reverseMatcher.find()) {
                        mapToSample(name[0], libNumber, fbarcode, rbarcode, forwardMatcher.group(1),
                                reverseMatcher.group(1));
                    } else {
                        forwardMatcher = forwardPattern.matcher(revLineSequence);
                        reverseMatcher = reversePattern.matcher(forLineSequence);
                        if (forwardMatcher.find() && reverseMatcher.find()) {
                            mapToSample(name[0], libNumber, fbarcode, rbarcode, forwardMatcher.group(1),
                                    reverseMatcher.group(1));
                        }
                    }
                    primerCorrect++;
                } else {
                    // Discard reads shorter than the input read length
                    if (forLineSequence.length() < args.forwardReadLength || revLineSequence.length() < args.reverseReadLength) {
                        possibleErrors.add("Reads shorter than " + args.forwardReadLength + " on the forward or " + args.reverseReadLength + " on the reverse have been removed, perhaps all your reads are smaller than this value?");
                        continue;
                    }

                    if (args.primersRemoved) {
                        mapToSample(name[0], libNumber, forLineSequence.substring(0, lib.fBarCodeLength),
                                revLineSequence.substring(0, lib.rBarCodeLength),
                                forLineSequence.substring(0, args.forwardReadLength),
                                revLineSequence.substring(0, args.reverseReadLength));
                    } else if (forwardMatcher.find() && reverseMatcher.find()) {
                        mapToSample(name[0], libNumber, forLineSequence.substring(0, lib.fBarCodeLength),
                                revLineSequence.substring(0, lib.rBarCodeLength), forwardMatcher.group(1),
                                reverseMatcher.group(1));

                        if (args.fastQfiles) {
                            logger.debug("writeFastQ function");
                            logger.debug(StringUtils.join(new String[]{String.valueOf(libNumber), forLineSequence.substring(0, lib.fBarCodeLength),
                                    revLineSequence.substring(0, lib.rBarCodeLength), forLineHeader, revLineHeader,
                                    forLineSequence, revLineSequence, forLineQuality, revLineQuality}, "\t"));
                            //
                            writeFastQ(libNumber, forLineSequence.substring(0, lib.fBarCodeLength),
                                    revLineSequence.substring(0, lib.rBarCodeLength), forLineHeader, revLineHeader,
                                    forLineSequence, revLineSequence, forLineQuality, revLineQuality);
                        }

                        primerCorrect++;
                        if (forLineSequence.substring(0, lib.fBarCodeLength)
                                .equals(revLineSequence.substring(0, lib.rBarCodeLength)))
                            sameBarCodeCount++;
                    }
                    // Sequence can also be sequenced in reverse
                    else {
                        forwardMatcher = forwardPattern.matcher(revLineSequence);
                        reverseMatcher = reversePattern.matcher(forLineSequence);
                        if (forwardMatcher.find() && reverseMatcher.find()) {
                            mapToSample(name[0], libNumber, revLineSequence.substring(0, lib.fBarCodeLength),
                                    forLineSequence.substring(0, lib.rBarCodeLength), forwardMatcher.group(1),
                                    reverseMatcher.group(1));

                            if (args.fastQfiles)
                                writeFastQ(libNumber, revLineSequence.substring(0, lib.fBarCodeLength),
                                        forLineSequence.substring(0, lib.rBarCodeLength), forLineHeader, revLineHeader,
                                        forLineSequence, revLineSequence, forLineQuality, revLineQuality);

                            primerCorrect++;
                            if (forLineSequence.substring(0, lib.fBarCodeLength)
                                    .equals(revLineSequence.substring(0, lib.rBarCodeLength)))
                                sameBarCodeCount++;
                        }
                    }
                }
            }
            if (fastQReverse.hasNextLine())
                throw new Exception("FastQ files do not match to each other: " + fastQReverse.nextLine());

            fastQForward.close();
            fastQReverse.close();

            lib.totalReads = totalLength;
            lib.primerHitsAccepted = primerCorrect;
            lib.primerHitsAcceptedRatio = ((double) primerCorrect) / ((double) totalLength);
            lib.barcodeHitsAccepted = this.barCodeHit;
            lib.barcodeHitsAcceptedRatio = ((double) this.barCodeHit) / ((double) totalLength);
            lib.acceptedSameBarCodeRatio = ((double) sameBarCodeCount) / ((double) totalLength);
        }
        closeSampleFastaFile();

        // Checks if fasta files are empty, otherwise throw a warning
        for (Sample sample : this.samples) {
            String fastaFile = "total_sample_files/" + sample.sampleName + ".fasta";
            if (new File(fastaFile).length() == 0) {
                logger.warn(
                        fastaFile + " is empty this could be due to...\n" +
                        "Barcode or Primer matching failed for " + fastaFile + " \n" +
                        "For barcode: either the wrong barcodes are used in the mapping file or this field should be empty.\n" +
                        "For primer: if primers is removed, add -primersRemoved to the input argument.\n" +
                        "For read length: Check if the reads are longer than the provided setting.\n" +
                        "If this sample is a negative control you can ignore this message as no DNA was found"
                        );
            }
        }

        logger.info("Barcode matched, file(s) created.");
    }

    /**
     * Encode the sequence from T to B, G to D, C to H and left A unchanged.
     *
     * @param seq -- the sequence from the database
     * @return
     */
    static String encodeForDb2(String seq) {
        StringBuilder builder = new StringBuilder();
        builder.ensureCapacity(seq.length());

        // int count = 0;
        for (int i = 0; i < seq.length(); i++) {
            char c = seq.charAt(i);
            builder.append(c == 'T' ? 'B' : (c == 'G' ? 'D' : (c == 'C' ? 'H' : c)));
        }
        return builder.toString();
    }

    private void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }

    private void unzip(ZipInputStream zipIn, String destDirectory) throws IOException {
        File destDir = new File(destDirectory);
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        ZipEntry entry = zipIn.getNextEntry();
        // iterates over entries in the zip file
        boolean parsed = false;
        while (entry != null) {
            parsed = true;
            logger.debug("inside entry");
            String filePath = destDirectory + File.separator + entry.getName();
            if (!entry.isDirectory()) {
                // if the entry is a file, extracts it
                extractFile(zipIn, filePath);
            } else {
                // if the entry is a directory, make the directory
                File dir = new File(filePath);
                dir.mkdir();
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();

        if (!parsed)
            throw new ZipException("This is not a zip file");
    }

    /**
     * Check if this string is actually a sequence.
     *
     * @param test -- sequence in consideration
     * @return boolean
     */
    private boolean isATCG(String test) {
        return test.matches("^[ATCG]*$");
    }

    public String revComplement(String seq) {
        StringBuilder builder = new StringBuilder();
        builder.ensureCapacity(seq.length());

        // int count = 0;
        for (int i = seq.length() - 1; i >= 0; i--) {
            char c = seq.charAt(i);
            builder.append(c == 'A' ? 'T' : (c == 'T' ? 'A' : (c == 'G' ? 'C' : (c == 'C' ? 'G' : c))));
        }
        return builder.toString();
    }

    String revComplementRNA(String seq) {
        StringBuilder builder = new StringBuilder();
        builder.ensureCapacity(seq.length());

        // int count = 0;
        for (int i = seq.length() - 1; i >= 0; i--) {
            char c = seq.charAt(i);
            builder.append(c == 'A' ? 'T' : (c == 'T' ? 'A' : (c == 'G' ? 'C' : (c == 'C' ? 'G' : c))));
        }
        return builder.toString();
    }

    /**
     * Used to check if the given string is integer or character
     * Will give -1 if its a character
     *
     * @param in
     * @return
     */
    private int parseInt(String in) {
        int toRet = -1;
        try {
            toRet = Integer.parseInt(in);
        } catch (Throwable th) {
        }
        return toRet;
    }

    /**
     * Read fastq and barcode files from the input.
     * Assign them to the sample and library.
     *
     * @throws Exception
     */
    void readSampleMapFile() throws Exception {
        String fastQDirectory = null;
        if (args.fastQSet == null) {
            // No input files given only performing mock analysis

        } else if (args.fastQSet.size() == 1) // If the argument contains only one fastQ set (forward and reverse)
        {
            // Assign the first fastq file as filename
            String fastQFileName = args.fastQSet.get(0); // fileName = small_1.fastq,small_2.fastq

            // Option to only create the db
            if (fastQFileName.equals("empty")) // If there is no filename, exit
                return;

            File fastqFile = new File(fastQFileName); // Create file from filename

            // Check if the file exist
            // Usually will not exist if multiple input files is used
            // Will exists when import input files as zipped file or directory
            if (fastqFile.exists()) {
                if (fastqFile.isDirectory()) // Check if its a directory
                    fastQDirectory = fastQFileName;
                else {
                    try // try to open zip file and put files in "fastQfiles" folder that has been created
                    {
                        ZipInputStream zipStream = new ZipInputStream(new FileInputStream(fastQFileName));
                        logger.info("Extracting zip file");
                        unzip(zipStream, "./fastQFiles");
                        logger.info("Done");
                        fastQDirectory = "./fastQfiles";
                    } catch (ZipException e) {
                        fastQDirectory = null;
                    }
                }
            }
        }
        if (fastQDirectory == null && args.fastQSet != null) // If there is no file directory
        {
            HashSet<String> check = new HashSet<>();
            int count = libraries.size() + 1;
            for (String set : args.fastQSet) // run through each fastQ set, each set is separated using space.
            {
                String tmp[] = set.split(","); // Split each fastQ set by comma(,)


                if (tmp.length != 2) { // Check if the file set correctly separated by comma(,)
                    // TODO change this to single option
                    if (tmp.length == 1 && !args.singleEnd)
                        throw new Exception("fastQ files should be given as sets separated by a , : " + set);
                }

                // Assign forward and reverse file
                String forwardFiles = tmp[0];
                String reverseFiles = null;
                if (!args.singleEnd)
                    reverseFiles = tmp[1];

                // Initialize barcode file
                String forwardBarcodeFile = null;
                String reverseBarcodeFile = null;

                if (tmp.length > 2) // If there are more files ???, set barcode file
                {
                    forwardBarcodeFile = tmp[2];
                    reverseBarcodeFile = tmp[3];
                }

                // Check if the the fastQ file is in the 'check' hashset.
                // If it is in the hashset, it has been used before which is not allowed.
                if (check.contains(forwardFiles) || check.contains(reverseFiles) || forwardFiles.equals(reverseFiles))
                    throw new Exception("fastq file can only be used once");

                // Add files to 'check' hashset
                check.add(forwardFiles);
                if (!args.singleEnd)
                    check.add(reverseFiles);

                // Assign files to the Library class, (1, new Library(1, small_1.fastq, small_2.fastq, null, null))
                libraries.put(count, new Library(count, forwardFiles, reverseFiles, forwardBarcodeFile, reverseBarcodeFile));
                count++;
            }
        }

        // Initialize libNums
        HashSet<Integer> libNums = new HashSet<>();
        Boolean hasReverseBarCodes = null;

        // Read through all each line of the mapping file
        // replace all the special characters to \n then split and process line by line

        String[] header = new String[0];
        HashMap<String, String> mappingInfo = new HashMap<>();
        if (args.mapFile == null)
            logger.error("No mapping file given using -mapFile");

        for (String mapLine : new String(Files.readAllBytes(Paths.get(args.mapFile)))
                .replaceAll("\r\n", "\n").replaceAll("\r", "\n").split("\n")) {

            // If the trimmed version of each line empty or starts with #, ignore
            // .trim() uses to trim of all white spaces

            if (mapLine.trim().isEmpty() || mapLine.trim().startsWith("#")) {
                header = mapLine.split("\t");
                continue; // skip that line (the header)
            }

            String[] mapLineParts = mapLine.split("\t"); // mapLineParts = [Sample.talarrubias, GAACGTAT, GAACGTAT, 1, p, ...]

            if (mapLineParts.length < 3) // Check if there is enough fields in mapping file
                throw new Exception(
                        "Need at least 3 fields (sampleName, Barcode, LibraryNumber) in the mapping file seperated by a tab: " + mapLine);

            // Set second column as fBarcode (first column is the sample name)
            String fBarcode = mapLineParts[1]; // fBarcode = GAACGTAT
            String rBarcode = fBarcode; // rBarcode = GAACGTAT

            // Check if the text in barcode column is a sequence consists of ATCG.
            if (!isATCG(fBarcode))
                throw new Exception("Mapping file column 2 should contain forward barcode sequence: " + fBarcode);

            // Check content in mapping file for a reverse barcode, parseInt return -1 if it is a string of characters
            // 1 to classify that the reverse barcode is not in the mapping file
            // 2 to classify that the reverse barcode is in the mappping file.
            if (hasReverseBarCodes == null) {
                if (parseInt(mapLineParts[2]) != -1)
                    hasReverseBarCodes = false;
                else if (mapLineParts.length >= 4 && parseInt(mapLineParts[3]) != -1 && (isATCG(mapLineParts[2]) || mapLineParts[2].trim().equals("")))
                    hasReverseBarCodes = true;
                else
                    throw new Exception(
                            "Either third column should be library number or third column reverse barcode and forth column be the library number");
            }

            int libraryNum;
            String fastQFiles;

            if (!hasReverseBarCodes) {
                libraryNum = parseInt(mapLineParts[2]);
                for (int i = 3; i < header.length && i < mapLineParts.length; i++) {
                    if (new File(mapLineParts[i]).exists()) {
                        mapLineParts[i] = new File(mapLineParts[i]).getName();
                    }
                    mappingInfo.put(header[i], mapLineParts[i]);
                }
            } else {
                libraryNum = parseInt(mapLineParts[3]);
                for (int i = 4; i < header.length && i < mapLineParts.length; i++) {
                    if (new File(mapLineParts[i]).exists()) {
                        mapLineParts[i] = new File(mapLineParts[i]).getName();
                    }
                    mappingInfo.put(header[i], mapLineParts[i]);
                }
                rBarcode = mapLineParts[2].trim();
                if (!rBarcode.equals("") && !isATCG(rBarcode)) {
                    throw new Exception("Reverse barcode column should be a barcode: " + rBarcode);
                }
                if (rBarcode.equals(""))
                    rBarcode = fBarcode;
            }

            // Check if library number is correctly assigned.
            if (libraryNum == -1)
                throw new Exception("Need positive integer at library num column: " + libraryNum);

            libNums.add(libraryNum); // add library number

            // Assign sample name and add the sample
            String sampleName = mapLineParts[0];
            addSample(new Sample(sampleName, libraryNum, fBarcode, rBarcode, mappingInfo));

            // Assign forward and reverse barcode file
            if (args.folder == "") {
                fastQFiles = mappingInfo.get("LibraryName"); // Regular mapping file
            } else {
                fastQFiles = mappingInfo.get("fastQforward") + "," + mappingInfo.get("fastQReverse"); // -folder mapping file
            }

            String forwardBarcodeFile = null;
            String reverseBarcodeFile = null;
            if (fastQFiles != null) {
                forwardBarcodeFile = fastQFiles.split(",").length > 2 ? fastQFiles.split(",")[2] : "null";
                reverseBarcodeFile = fastQFiles.split(",").length > 2 ? fastQFiles.split(",")[3] : "null";
            }

            // Get library number
            Library lib = libraries.get(libraryNum);

            // If library directory exist
            logger.debug("FASTQ Directory: " + fastQDirectory);

            ArrayList<String> fastQfilesSplit = new ArrayList<>();
            for (String element : fastQFiles.split(","))
                fastQfilesSplit.add(fastQDirectory + "/" + element);
            if (fastQfilesSplit.size() < 2)
                fastQfilesSplit.add(null);

            if (fastQDirectory != null) {
                Library newLib = new Library(libraryNum, fastQfilesSplit.get(0),
                        fastQfilesSplit.get(1), forwardBarcodeFile, reverseBarcodeFile);
                if (lib != null) {
                    if (!lib.equals(newLib))
                        throw new Exception("Library information for given library should be same in each entry");
                } else {
                    lib = newLib;
                    libraries.put(libraryNum, lib);
                }
            }

            if (lib == null)
                throw new Exception("Library " + libraryNum + " is not defined, multiple libraries are separated by space.");

            // Assign and check barcode length
            if (lib.fBarCodeLength == 0)
                lib.fBarCodeLength = fBarcode.length();
            if (lib.fBarCodeLength != 0 && lib.fBarCodeLength != fBarcode.length())
                throw new Exception("Forward barcodes are not of the same length: " + lib.rBarCodeLength
                        + " : " + mapLineParts[1].length());

            if (lib.rBarCodeLength == 0)
                lib.rBarCodeLength = rBarcode.length();
            if (lib.rBarCodeLength != 0 && lib.rBarCodeLength != rBarcode.length())
                throw new Exception("Reverse barcodes are not of the same length: " + lib.rBarCodeLength
                        + " : " + mapLineParts[2].length());

        }

        Integer tmp[] = libNums.toArray(new Integer[0]);
        Arrays.sort(tmp, Collections.reverseOrder());

        if (tmp.length != 0 && libNums.size() != tmp[0])
            throw new IOException("library numbers must be a continues ordering, no gaps allowed");

        if (libNums.size() != 0 && libNums.size() != libraries.size() && !args.singleEnd)
            throw new IOException("library size does not match with numbers in map file: " + libraries.size() + " : " + libNums.size());

        HashSet<String> sampleNames = new HashSet<>();

        for (Sample sample : this.samples) {
            if (sampleNames.contains(sample.sampleName))
                throw new Exception("Sample name should be uniq: " + sample.sampleName);
            sampleNames.add(sample.sampleName);
        }
    }

    private void addSample(Sample sample) {
        samples.add(sample);
    }

    /**
     * Process all samples by the following steps
     * - readSample: read sample
     * - pickOtu: add/reject sequences
     * - filterChimera: filtered out chimera
     * - errorCorrect: re-clustering the discarded reads
     *
     * @throws Exception
     */
    void processSamples() throws Exception {
        // Process all samples
        for (Sample sample : samples) {
            logger.info("Processing: " + sample.sampleName);
            sample.readSample(args);
            sample.pickOtu(args);
            // TODO should we do first chimera filter and then error correct or other
            // way around better
            sample.filterChimera(args);
            sample.errorCorrect(args);
            sample.cleanRejectedOtu(args);
        }
    }

    /**
     * Make two directories: total_sample_files and fastqFiles
     */
    void mkDir() {
        // Creating tmp map for usage
        new File("total_sample_files").mkdir();
        new File("fastQFiles").mkdir();
    }

    void collectGeneralOtu() {
        HashMap<String, ASV> foundSequences = new HashMap<>();
        String prefix = args.prefixId;

        if (prefix == null)
            prefix = args.biomFile.replaceAll("([a-zA-Z0-9_-]*).*", "\\1") + "_";

        for (Sample sample : this.samples) {
            for (SampleASV sampleOTU : sample.otus) {
                // Try to get the ASV
                ASV otu = foundSequences.get(sampleOTU.forwardSequence + sampleOTU.reverseSequence);

                // If not yet exists, add it in to ASV object
                if (otu == null) {
                    otu = new ASV(prefix + App.asvs.size(), sampleOTU.forwardSequence, sampleOTU.reverseSequence);
                    this.addOTU(otu);
                    foundSequences.put(sampleOTU.forwardSequence + sampleOTU.reverseSequence, otu);
                }
                sampleOTU.masterOtuId = otu.id;
            }
        }

        /**
         * REJECTED PART
         */

        for (Sample sample : this.samples) {
            LinkedList<SampleASV> rejectedASVList = new LinkedList<>();
            if (sample.rejectedOtus.size() > args.rejectedASVLimit) {
                rejectedASVList.addAll(sample.rejectedOtus.subList(0, args.rejectedASVLimit));
            } else {
                rejectedASVList = sample.rejectedOtus;
            }

            for (SampleASV sampleASV : rejectedASVList) {
                // Try to get the ASV
                ASV otu = foundSequences.get(sampleASV.forwardSequence + sampleASV.reverseSequence);
                // If not yet exists, add it in to ASV object
                if (otu == null) {
                    otu = new ASV(prefix + App.rejectedASVList.size() + "R", sampleASV.forwardSequence, sampleASV.reverseSequence);
                    this.addRjectedOTU(otu);
                    foundSequences.put(sampleASV.forwardSequence + sampleASV.reverseSequence, otu);
                }
                sampleASV.masterOtuId = otu.id;
            }
            sample.rejectedOtus = rejectedASVList;
        }

        /**
         * REJECTED END
         */

        logger.info("Found " + App.asvs.size() + " uniqe otu..");
        logger.info("Found " + App.rejectedASVList.size() + " rejected otu above the threshold..");

        // Set number to reclassify the rejected OTU
        if (App.rejectedASVList.size() < args.rejectedASVAnnotation || args.rejectedASVAnnotation == -1) {
            args.rejectedASVAnnotation = App.rejectedASVList.size();
        }

        // Calculate and estimate the time of computation.
        int totalSecs = ((App.asvs.size() + args.rejectedASVAnnotation) * 4);
        int hours = totalSecs / 3600;
        int minutes = (totalSecs % 3600) / 60;
        int seconds = totalSecs % 60;
        String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
        if (args.refdb != null) {
            logger.info("Classifying " + App.asvs.size() + " otus and " + args.rejectedASVAnnotation + " rejected otus..");
            logger.info("Analysis will take around: " + timeString);
        }

    }

    /**
     * Create the biom file.
     *
     * @throws Exception
     */
    void createBiomFile() throws Exception {
        NGTaxResult result = new NGTaxResult();
        result.version = args.ngtaxversion;
        result.commit = args.ngtaxcommit;

        if (this.args.markIfMoreThen1) {
            HashSet<String> hasMulti = new HashSet<>();
            HashSet<String> hasSingle = new HashSet<>();
            for (ASV otu : App.asvs.values()) {
                if (otu.assignedTaxon != null) {
                    if (otu.assignedTaxon.tax.endsWith("~*"))
                        hasMulti.add(otu.assignedTaxon.tax.substring(0, otu.assignedTaxon.tax.length() - 2));
                    else
                        hasSingle.add(otu.assignedTaxon.tax);
                }
            }
            for (ASV otu : App.asvs.values()) {
                if (otu.assignedTaxon != null) {
                    if (otu.assignedTaxon.tax.endsWith("~*")) {
                        if (hasSingle
                                .contains(otu.assignedTaxon.tax.substring(0, otu.assignedTaxon.tax.length() - 2)))
                            otu.assignedTaxon.tax =
                                    otu.assignedTaxon.tax.substring(0, otu.assignedTaxon.tax.length() - 2) + "=*";
                    } else {
                        if (hasMulti.contains(otu.assignedTaxon.tax))
                            otu.assignedTaxon.tax = otu.assignedTaxon.tax + "=*";
                    }
                }
            }
        }

        for (ASV otu : App.asvs.values()) {
            String taxon = "NA";
            if (otu.assignedTaxon != null)
                taxon = otu.assignedTaxon.tax;
            String tmp[] = taxon.split(";");
            taxon = (tmp[0].equals("NA") ? "NA" : ("k__" + tmp[0])) + ";";
            taxon = taxon + "p__" + (tmp.length > 1 ? tmp[1] : "") + ";";
            taxon = taxon + "c__" + (tmp.length > 2 ? tmp[2] : "") + ";";
            taxon = taxon + "o__" + (tmp.length > 3 ? tmp[3] : "") + ";";
            taxon = taxon + "f__" + (tmp.length > 4 ? tmp[4] : "") + ";";
            taxon = taxon + "g__" + (tmp.length > 5 ? tmp[5] : "") + ";";
            taxon = taxon + "s__" + (tmp.length > 6 ? tmp[6] : "") + ";";
            taxon = String.join("", Arrays.copyOfRange(taxon.split(" "), 0, taxon.split(" ").length));
            result.addTaxonToOtu("" + otu.id, taxon);
            Taxon addTo = null;
            for (Taxon item : foundTaxons) {
                if (item.tax.equals(taxon)) {
                    addTo = item;
                    break;
                }
            }
            if (addTo == null) {
                addTo = new Taxon(taxon);
                this.foundTaxons.add(addTo);
            }
            addTo.otus.add(otu.id);
        }

        for (Sample sample : samples) {
            logger.info("Processing sample " + sample.sampleName);
            for (SampleASV sampleOtu : sample.otus) {
                if (!(App.asvs.get(sampleOtu.masterOtuId).id.matches(sampleOtu.masterOtuId))) {
                    throw new Exception("master otu id does not match, >" + App.asvs.get(sampleOtu.masterOtuId).id + "< vs. >" + sampleOtu.masterOtuId + "<");
                }
                result.addValue("" + sampleOtu.masterOtuId, sample.sampleName, sampleOtu.clusteredReadCount);
            }
        }

        if (args.singleEnd) {
            for (ASV otu : this.asvList.values()) {
                otu.reverseSequence = "";
            }

            for (ASV otu : this.rejectedASVList.values()) {
                otu.reverseSequence = "";
            }

            for (Sample sample : this.samples) {
                for (SampleASV otu : sample.otus) {
                    otu.reverseSequence = "";
                }
                for (SampleASV otu : sample.rejectedOtus) {
                    otu.reverseSequence = "";
                }
            }
        }

        result.provData = this;
        result.save(args.biomFile);

        // Convert biom to RDF format.
        if (args.turtleFile != null) {
            Biom2Rdf biom2rdf = new Biom2Rdf();
            biom2rdf.ngTaxBiomParser(result, args.turtleFile);
        }

        // Export ASV sequences in fasta format.
        if (args.fastaFileLocation != null) {
            Biom2ASVfasta biom2otufasta = new Biom2ASVfasta();
            biom2otufasta.biom2fastaParser(result, args.fastaFileLocation);
        }
    }
}
