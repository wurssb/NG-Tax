package nl.wur.ssb.NGTax;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;

import static nl.wur.ssb.NGTax.Biom2Rdf.createOTUInfo;
import static nl.wur.ssb.NGTax.Biom2Rdf.logger;

/**
 * Extract ASV sequences from the biom file and export it in fasta format.
 */

public class Biom2ASVfasta {

    /**
     * Directly extract ASV sequences from the existing biom file
     *
     * @param biomFile          -- BIOM file
     * @param fastaFileLocation -- Location of the output files
     * @throws IOException
     */
    public static void biom2fastaDirectParser(String biomFile, String fastaFileLocation) throws IOException {

        new NGTaxResult();
        File inputFile = new File(biomFile);
        String data = new String(Files.readAllBytes(Paths.get(inputFile.getAbsolutePath())));
        NGTaxResult res = NGTaxResult.fromJSon(data);
        biom2fastaParser(res, fastaFileLocation);

    }

    /**
     * Extract the ASV sequences from the results of NG-Tax run.
     *
     * @param res               -- NG-Tax results object
     * @param fastaFileLocation -- Location of the output files.
     * @throws IOException
     */
    public static void biom2fastaParser(NGTaxResult res, String fastaFileLocation) throws IOException {

        LinkedHashMap<String, ASV> otuData = createOTUInfo(res.provData.asvList);

        String biomfileName = res.provData.args.biomFile.split("/")[res.provData.args.biomFile.split("/").length - 1].split(".biom")[0];

        PrintWriter otu_f = new PrintWriter(fastaFileLocation + biomfileName + "_otu_f.fasta", "UTF-8");
        PrintWriter otu_r = new PrintWriter(fastaFileLocation + biomfileName + "_otu_r.fasta", "UTF-8");
        PrintWriter otu_m = new PrintWriter(fastaFileLocation + biomfileName + "_otu_m.fasta", "UTF-8");

        for (Sample sample : res.provData.samples) {
            for (SampleASV sampleOTU : sample.otus) {

                String taxName = "Not assigned";

                if (otuData.get(sampleOTU.masterOtuId).assignedTaxon != null) {
                    taxName = otuData.get(sampleOTU.masterOtuId).assignedTaxon.tax;
                }

                otu_f.println(">" + sampleOTU.masterOtuId + "_f " + taxName);
                otu_f.println(sampleOTU.forwardSequence);

                otu_r.println(">" + sampleOTU.masterOtuId + "_r " + taxName);
                otu_r.println(sampleOTU.reverseSequence);

                otu_m.println(">" + sampleOTU.masterOtuId + "_m " + taxName);
                otu_m.println(sampleOTU.forwardSequence +"NNNN"+ sampleOTU.reverseSequence);

            }
        }

        otu_f.close();
        otu_r.close();
        otu_m.close();

        logger.info("ASV sequence exported!");
    }
}
