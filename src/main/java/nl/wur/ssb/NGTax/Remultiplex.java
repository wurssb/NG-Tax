package nl.wur.ssb.NGTax;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.*;

public class Remultiplex {
    static final Logger logger = LogManager.getLogger(App.class);

    public static void directRemultiplex(String fastQSet, String mappingFile, String fPrimer, String rPrimer, Boolean primerRemoved) throws Exception {

        new File("Remultiplexed_fastq").mkdir();
        logger.info("Directory \"Remultiplex_fastq\" has been created to store the output files");


        fPrimer = fixPrimer(fPrimer);
        rPrimer = fixPrimer(rPrimer);

        logger.info("Detected " + String.valueOf(fastQSet.split(" ").length) + " fastqsets");
        remultiplex(fastQSet, mappingFile, fPrimer, rPrimer, primerRemoved);

        logger.info("Finished remultiplexed");
    }

    private static void remultiplex(String fastQSet, String mappingFile, String fPrimer, String rPrimer, Boolean primerRemoved) throws Exception {

        /*
         * Generate barcodes
         */
        LinkedList<String> barcodes = new LinkedList<>();
        barcodes = barcodeGenerator(barcodes, "");

        /*
         * Generate mapping file
         */
        if (mappingFile != null) {
            mappingFileAdjustment(mappingFile, barcodes);
        } else {
            createMappingFile(fastQSet, barcodes);
        }

        /*
         * Go through each fastq set
         */

        // To check if file already exists.
        HashSet<String> check = new HashSet<>();
        int fastQsetNumber = 0;
        for (String set : fastQSet.split(" ")) // run through each fastQ set, each set is separated using space.
        {
            logger.info("Processing fastq set: " + set);
            fastQsetNumber = fastQsetNumber + 1;

            String tmp[] = set.split(","); // Split each fastQ set by comma(,)

            if (tmp.length < 2) // Check if the file set correctly separated by comma(,)
                throw new Exception("fastQ files should be given as sets seperated by a , : " + set);

            // Assign forward and reverse file
            String forwardFileName = tmp[0]; // forwardFiles = small_1.fastq
            String reverseFileNAme = tmp[1]; // reverseFiles = small_2.fastq

            // Check if the the fastQ file is in the 'check' hashset.
            // If it is in the hashset, it has been used before which is not allowed.
            if (check.contains(forwardFileName) || check.contains(reverseFileNAme) || forwardFileName.equals(reverseFileNAme))
                throw new Exception("fastq file can only be used once");

            // Add files to 'check' hashset
            check.add(forwardFileName);
            check.add(reverseFileNAme);

            // Set up scanner to read fastq file
            Scanner forwardFile = new Scanner(new File(forwardFileName));
            Scanner reverseFile = new Scanner(new File(reverseFileNAme));

            int fastqLine = 0;
            while (forwardFile.hasNextLine()) {

                // Retrieve information of fastq file on both forward and reverse
                String f_header = forwardFile.nextLine();
                String f_seq = forwardFile.nextLine();
                String f_plus = forwardFile.nextLine();
                String f_qual = forwardFile.nextLine();

                String r_header = reverseFile.nextLine();
                String r_seq = reverseFile.nextLine();
                String r_plus = reverseFile.nextLine();
                String r_qual = reverseFile.nextLine();

                fastqLine = fastqLine + 4;
                if (fastqLine % 1000000 == 0) {
                    logger.info("I don't know how big your file is, but now its gone through: " + String.valueOf(fastqLine) + " lines");
                }
            }
        }

    }

    /**
     * Generate barcodes
     *
     * @param barcodes   -- List of barcodes generated
     * @param theBarcode -- Each generated barcode.
     * @return
     */
    private static LinkedList<String> barcodeGenerator(LinkedList<String> barcodes, String theBarcode) {

        char[] nucleotides = new char[]{'A', 'T', 'C', 'G'};

        if (theBarcode.length() == 8) {
            barcodes.add(theBarcode);
        } else {
            for ( int i = 0; i < nucleotides.length; i++ ) {
                String buildingBarcode = theBarcode;
                theBarcode += nucleotides[i];
                barcodeGenerator(barcodes, theBarcode);
                theBarcode = buildingBarcode;
            }
        }
        return barcodes;
    }

    /**
     * Adjust the given mapping file by adding the barcodes.
     * @param mappingFile
     * @param barcodes
     * @throws IOException
     */
    private static void mappingFileAdjustment(String mappingFile, LinkedList<String> barcodes) throws IOException {
        logger.info("Mapping file given, adding barcodes to the mapping file.");

        BufferedReader mapReader = new BufferedReader(new FileReader(mappingFile));
        BufferedWriter mapWriter = new BufferedWriter(new FileWriter("Remultiplexed_fastq/mappingFile_barcodesIncluded.txt"));

        String line;
        int lineNum = 0;
        while ((line = mapReader.readLine()) != null) {
            if (line.startsWith("#")){
                if (line.split("\t").length >= 4){
                    mapWriter.write(line + "\n");
                }else{
                    throw new IOException("At least 4 fields are required: #SampleID, BarcodeSequence, LibraryNumber and Direction");
                }
            }
            else {
                LinkedList<String> contents = new LinkedList<>();
                for (String content : line.split("\t")){
                    contents.add(content);
                }
                contents.add(1,barcodes.get(lineNum));
                contents.add(2,"1");
                contents.remove(3);
                contents.remove(3);
                String lineContent = "";
                for (String item : contents) {
                    lineContent += item + "\t";
                }
                lineContent += "\n";
                mapWriter.write(lineContent);
                lineNum += 1;
            }
        }
        mapWriter.close();
        logger.info("Mapping file saved to: Remultiplexed_fastq/mappingFile_barcodesIncluded.txt");
    }

    /**
     * Generate the mapping using the barcodes generated.
     * @param fastQSet
     * @param barcodes
     * @throws IOException
     */
    private static void createMappingFile(String fastQSet, LinkedList<String> barcodes) throws IOException {
        logger.info("Mapping file not given, creating a mapping with minimum information required.");
        BufferedWriter mapWriter = new BufferedWriter(new FileWriter("Remultiplexed_fastq/mappingFile_barcodesIncluded.txt"));

        String header = "#sampleID" + "\t" + "BarcodeSequence" + "\t" + "LibraryNumber" + "\t" + "Direction" + "\t" + "LibraryName" + "\n";
        mapWriter.write(header);

        int counter = 0;
        for (String fastq : fastQSet.split(" ")) {
            String line = "Sample" + String.valueOf(counter+1) + "\t" + barcodes.get(counter) + "\t" + "1" + "\t" + "p" + "\t" + fastq + "\n";
            mapWriter.write(line);
        }
        mapWriter.close();
        logger.info("Mapping file saved to: Remultiplexed_fastq/mappingFile_barcodesIncluded.txt");
    }

    /**
     * Fix the primer from both special character and the regex bracket.
     *
     * @param primer
     * @return
     */
    private static String fixPrimer(String primer) {
        return primer.replaceAll("B", "[CGT]")
                .replaceAll("\\[CGT\\]", "C")
                .replaceAll("D", "[AGT]")
                .replaceAll("\\[AGT\\]", "A")
                .replaceAll("H", "[ACT]")
                .replaceAll("\\[ACT\\]", "A")
                .replaceAll("K", "[GT]")
                .replaceAll("\\[GT\\]", "G")
                .replaceAll("M", "[AC]")
                .replaceAll("\\[AC\\]", "A")
                .replaceAll("N", "[ACGT]")
                .replaceAll("\\[ACGT\\]", "A")
                .replaceAll("R", "[AG]")
                .replaceAll("\\[AG\\]", "A")
                .replaceAll("S", "[GC]")
                .replaceAll("\\[GC\\]", "G")
                .replaceAll("V", "[ACG]")
                .replaceAll("\\[ACG\\]", "A")
                .replaceAll("W", "[AT]")
                .replaceAll("\\[AT\\]", "A")
                .replaceAll("Y", "[CT]")
                .replaceAll("\\[CT\\]", "C");
    }
}
