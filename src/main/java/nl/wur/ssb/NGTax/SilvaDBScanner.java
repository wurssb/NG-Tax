package nl.wur.ssb.NGTax;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

/**
 * Go through content in the given 16S database
 */

public class SilvaDBScanner {

    private LineIterator refdbIn;
    private String header = null;
    private String sequence = null;
    private String[] taxa;
    private String sequenceNext = null;
    private StringBuilder sequenceBuild = new StringBuilder();
    private String[] taxaNext;
    static final Logger logger = LogManager.getLogger(SilvaDBScanner.class);

    SilvaDBScanner(File fileName) throws Exception {
        refdbIn = IOUtils.lineIterator(new GZIPInputStream(new BufferedInputStream(new FileInputStream(fileName))), "UTF-8");
        this.next();
    }

    public void close() throws IOException {
        this.refdbIn.close();
    }

    boolean hasNext() {
        return this.sequenceNext != null;
    }

    String next() throws Exception {
        this.taxa = this.taxaNext;
        this.sequence = this.sequenceNext;
        intNext();
        return this.sequence;
    }

    private void intNext() throws Exception {
        while (refdbIn.hasNext()) {
            String line = refdbIn.nextLine();
            if (line.startsWith(">")) {
                if (header != null) {
                    String tmp[] = header.split(" ", 2);
                    if (tmp.length != 2){
                        throw new Exception("Header must be in the following format: >Identifier Kingdom;Phylum;Class;Order;Family;Genus;Species");
                    }
                    String taxLine = tmp[1].replace(' ', '_');

                    if (taxLine.contains("YM;S32;TM7")){
                        logger.info("You are using SILVA database version 138, adjusted database header from " + taxLine + " to " +  taxLine.replace("YM;S32;TM7", "YM_S32_TM7_50_20"));
                        taxLine = taxLine.replace("YM;S32;TM7", "YM_S32_TM7_50_20");
                    }

                    taxaNext = taxLine.split(";");
                    if (!taxaNext[0].equals("Eukaryota")) { // Gives issues with Silva regarding subfamily etc... only in eukaryotes so far
                        sequenceNext = sequenceBuild.toString();
                        //Do the adding 12613
                        boolean ok = true;
                        for (int i = 0; i < sequenceNext.length(); i++) {
                            if ("ATCG-.".indexOf(sequenceNext.charAt(i)) == -1) {
                                ok = false;
                                break;
                            }
                        }

                        // If the taxon is not complete (6 levels), add "__" to the rest
                        if (ok) {
                            if (taxaNext.length > 7) {
                                logger.error("Higher number of taxonomic lineage than expected, maximum is 7.");
                                throw new Exception("Higher number of taxonomic lineage than expected, maximum is 7. " + taxLine);
                            }
                            if (taxaNext.length != 7) {
                                tmp = taxaNext;
                                taxaNext = new String[7];
                                for (int i = 0; i < 7; i++) {
                                    if (i < tmp.length)
                                        taxaNext[i] = tmp[i];
                                    else
                                        taxaNext[i] = "__";
                                }
                            }
                            this.sequenceBuild = new StringBuilder();
                            header = line;
                            return;
                        }
                    }
                }
                this.sequenceBuild = new StringBuilder();
                header = line;
            } else {
                // Makes sure that U's are T's, TODO need to check performance...
                line = line.replaceAll("U", "T");
                sequenceBuild.append(line);
            }
        }
        taxaNext = null;
        sequenceNext = null;
    }

    public String getSequence() {
        return sequence;
    }

    public String[] getTaxa() {
        return taxa;
    }
}
