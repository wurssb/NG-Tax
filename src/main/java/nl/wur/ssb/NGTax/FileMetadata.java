package nl.wur.ssb.NGTax;

import com.google.gson.annotations.Expose;

public class FileMetadata {
    @Expose
    private String sha256;
    @Expose
    private String fileType;
    @Expose
    private String md5;
    @Expose
    private String localFileName;
    @Expose
    private String localFilePath;
    @Expose
    private Long maxReadLength;
    @Expose
    private Long bases;
    @Expose
    private Long reads;

    public void setSha256(String sha256) {
        this.sha256 = sha256;
    }

    public String getSha256() {
        return sha256;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileType() {
        return fileType;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getMd5() {
        return md5;
    }

    public void setLocalFileName(String localFileName) {
        this.localFileName = localFileName;
    }

    public String getLocalFileName() {
        return localFileName;
    }

    public void setLocalFilePath(String localFilePath) {
        this.localFilePath = localFilePath;
    }

    public String getLocalFilePath() {
        return localFilePath;
    }

    public void setMaxReadLength(Long maxReadLength) {
        this.maxReadLength = maxReadLength;
    }

    public Long getMaxReadLength() {
        return maxReadLength;
    }

    public void setBases(Long bases) {
        this.bases = bases;
    }

    public Long getBases() {
        return bases;
    }

    public void setReads(Long reads) {
        this.reads = reads;
    }

    public Long getReads() {
        return reads;
    }
}
