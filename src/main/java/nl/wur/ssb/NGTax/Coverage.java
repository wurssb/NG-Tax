package nl.wur.ssb.NGTax;

import nl.wur.ssb.NGTax.CommandOptions.CommandOptionsCoverage;
import nl.wur.ssb.NGTax.CommandOptions.CommandOptionsNGTax;

import java.io.File;
import java.io.PrintWriter;
import java.util.*;

public class Coverage {

    public static void start(CommandOptionsCoverage commandOptionsCoverage) throws Exception {

        // Init arguments to trick ngtax into running without too much modifications
        String[] args = {
                "-ngtax",
                "-fS", "./src/test/resources/input/small_1.fastq,./src/test/resources/input/small_2.fastq",
                "-for_p", commandOptionsCoverage.forwardPrimer,
                "-rev_p", commandOptionsCoverage.reversePrimer,
                "-refdb", commandOptionsCoverage.refdb,
                "-for_read_len", String.valueOf(commandOptionsCoverage.forwardReadLength),
                "-rev_read_len", String.valueOf(commandOptionsCoverage.reverseReadLength),
                "-mapFile", "./src/test/resources/input/small_mapping.txt",
                "-minPerT", "0",
                "-identLvl", "60",
                "-errorCorr", "1",
                "-b", "./src/test/resources/output/small_sample2.biom",
                "-t", "./src/test/resources/output/small_sample2.ttl",
                "-classifyRatio", "0.8",
                "-debug"
        };

        App.arguments = new CommandOptionsNGTax(args);
        // Creates the lookup database
        ClassifyASV.loadrefdb();

        // Parse the reference database to have background information
        SilvaDBScanner refdbIn = new SilvaDBScanner(new File(App.arguments.refdb));
        int count = 0;
        HashMap<String, ArrayList<Integer>> taxonCounts = new HashMap<>();
        while (refdbIn.hasNext()) {
            count++;
            if (count % 100000 == 0) {
                System.err.println(count);
            }
            refdbIn.next();
            // Object with taxon information and primer match / mismatch
            String[] taxa = refdbIn.getTaxa();

            // Creating the rank list (Name, Hit, Total)
            ArrayList<String> ranks = createRanks(taxa);
            for (String rank : ranks) {
                if (!taxonCounts.containsKey(rank)) {
                    taxonCounts.put(rank, new ArrayList<>(Arrays.asList(0, 0)));
                }
                ArrayList<Integer> taxonCount = taxonCounts.get(rank);
                // Total count is the second element
                taxonCount.set(1, taxonCount.get(1) + 1);
                taxonCounts.put(rank, taxonCount);
            }
        }

        // Created database based on primers
        count = 0;
        for (EntryDB entryDB : App.refDb) {
            count++;
            if (count % 100000 == 0) {
                System.err.println(count);
            }

            String[] taxa = entryDB.taxon;

            ArrayList<String> ranks = createRanks(taxa);
            for (String rank : ranks) {
                if (!taxonCounts.containsKey(rank)) {
                    taxonCounts.put(rank, new ArrayList<>(Arrays.asList(0, 0)));
                }
                ArrayList<Integer> taxonCount = taxonCounts.get(rank);
                // Hit count is the first element
                taxonCount.set(0, taxonCount.get(0) + entryDB.abundance);
            }
        }

        // Normalise and print to tabular file for kronos
        PrintWriter printWriter = new PrintWriter(new File("coverage.tsv"));
        // Write the taxonCounts sorted alphabetically to the file with the count values
        printWriter.println("Superkingdom\tPhylum\tClass\tOrder\tFamily\tGenus\tSpecies\tHit\tTotal");
        taxonCounts.keySet().stream().sorted().forEach(entry ->
                printWriter.println(entry + "\t" + taxonCounts.get(entry).get(0) + "\t" + taxonCounts.get(entry).get(1))
        );
        printWriter.close();
    }

    private static ArrayList<String> createRanks(String[] segments) {
        // Remove all "--" from the segments
        for (int i = 0; i < segments.length; i++) {
            if (segments[i].matches("^--$") || segments[i].matches("^unidentified$") || segments[i].matches("^$") || segments[i].matches("^__$")) {
                segments[i] = "";
            }
        }
        // Clean the species name
        if (segments[6] != null) {
            String[] species = segments[6].split("[_.]");
            if (species.length > 1)
                segments[6] = species[0] + " " + species[1];
        }

        ArrayList<String> resultList = new ArrayList<>();

        // Build the list with increasing segments
        StringBuilder currentElement = new StringBuilder();
        // The segmet should always contain 6 tabs
        for (String segment : segments) {
            currentElement.append(segment);
            // Count the number of tabs needed to be added to the string
            long tabs = 6 - currentElement.toString().chars().filter(ch -> ch == '\t').count();
            StringBuilder tabString = new StringBuilder();
            for (int i = 0; i < tabs; i++) {
                tabString.append("\t");
            }
            resultList.add(currentElement.toString() + tabString.toString());
            currentElement.append("\t"); // Add a tab for the next segment
        }
        // Remove duplicate entries
        resultList = new ArrayList<>(new LinkedHashSet<>(resultList));
        return resultList;
    }
}
