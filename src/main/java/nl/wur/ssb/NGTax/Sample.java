package nl.wur.ssb.NGTax;

import com.google.gson.annotations.Expose;
import nl.wur.ssb.NGTax.CommandOptions.CommandOptionsNGTax;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Writer;
import java.util.*;

/**
 * Add details about the sample
 */

public class Sample {
    static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(Sample.class);

    @Expose
    int libraryNum;
    @Expose
    String sampleName;
    @Expose
    String fBarcode;
    @Expose
    String rBarcode;
    @Expose
    HashMap<String, String> mappingInfo;
//    @Expose
//    String fastQFiles;
    Writer writer;
    @Expose
    int totalCounts = 0;
    @Expose
    double numAcceptedReadsBeforeChimera = 0;
    @Expose
    double percentAcceptedReadsBeforeChimera = 0.0;
    @Expose
    double numReadsChimera = 0;
    @Expose
    double numAcceptedReadsAfterErrorCorrection = 0;
    @Expose
    double percentAcceptedReadsAfterErrorCorrection = 0.0;
    @Expose
    int numAcceptedOtuBeforeChimera = 0;
    @Expose
    double evenness = 0;
    @Expose
    int numRejectedOtu = 0;
    @Expose
    List<SampleASV> otus = new ArrayList<>();
    //Temporary values before 0.1% filtering is done
    HashMap<String, SampleASV> uniqSeqCount = new HashMap<>();
    @Expose
    LinkedList<SampleASV> rejectedOtus = new LinkedList<>();
    @Expose
    LinkedList<ChimeraRejection> rejectedAsChimera = new LinkedList<>();

    int acceptedReads = 0;

    public Sample(String fileName, int libraryNum, String fBarcode, String rBarcode, HashMap<String, String> mappingInfo) {
        this.libraryNum = libraryNum;
        this.sampleName = fileName;
        this.fBarcode = fBarcode;
        this.rBarcode = rBarcode;
        this.mappingInfo = mappingInfo;
    }

    /**
     * Read library file and create a sample file
     *
     * @param args -- commandline arguments
     * @throws Exception
     */
    void readSample(CommandOptionsNGTax args) throws Exception {
        // Reading library file and creating a sample file
        logger.info("Calculating threshold for each sample..");
        File sampleFile = new File("total_sample_files/", sampleName + ".fasta");
        logger.info("Sample: " + sampleFile);


        // Collect all otus for the given sample
        // this is just done my collecting all unique sequences
        // No instability because of clustering
        BufferedReader sampleBufferedReader = new BufferedReader(new FileReader(sampleFile));
        String line;

        while (sampleBufferedReader.readLine() != null) {
            line = sampleBufferedReader.readLine();
            if (args.singleEnd)
                addSequence(line.substring(0, args.forwardReadLength), StringUtils.repeat("x",args.forwardReadLength));
            else
                addSequence(line.substring(0, args.forwardReadLength), line.substring(args.forwardReadLength));
        }
        sampleBufferedReader.close();
    }

    /**
     * Add sequence to uniqSeqCount and initiate the SampleOTU with forward and reverse sequence
     *
     * @param forward -- forward sequence
     * @param reverse -- reverse sequence
     */
    public void addSequence(String forward, String reverse) {
        String sequence = forward + reverse;
        if (uniqSeqCount.containsKey(sequence)) {
            // Increase the readCount, if the sequence was already found.
            uniqSeqCount.get(sequence).readCount++;
        } else {
            // Otherwise, add a new sequence to sampleOTU
            uniqSeqCount.put(sequence, new SampleASV(forward, reverse));

        }
        this.totalCounts++;
    }

    /**
     * Categorize OTUs into two groups: otus and rejectedOtus
     * Also add other information, percentAcceptedReads, numRejectedOtu etc.
     *
     * @param args -- commandline arguments
     */
    void pickOtu(CommandOptionsNGTax args) {

        // Now filter OTU away which are below the min percentage threshold, which
        // is 0.1 by default
        SampleASV sortedOTU[] = uniqSeqCount.values().toArray(new SampleASV[uniqSeqCount.values().size()]);
        Arrays.sort(sortedOTU);


        /**
         * Evenness calculation (Shannons diversity index)
         */

        double asvRichness = 0.0;
        for (SampleASV otu : sortedOTU) {
            asvRichness += otu.readCount;
        }

        double shannon = 0.0;
        for (SampleASV otu : sortedOTU) {
            double proportion = otu.readCount / asvRichness;
            proportion = proportion * Math.log(proportion);
//            App.logger.info(proportion);
            shannon = shannon + proportion;
//            App.logger.info("Proportion " + proportion);
        }
        shannon = shannon * - 1;

        double evennessX = shannon / Math.log(sortedOTU.length);

        logger.info("ASVRichness " + asvRichness);
        logger.info("Shannon " + shannon);
        logger.info("Evenness " + evennessX);
        logger.info("Estimated Cutoff " + (1 - evennessX));

        // Possible solution to the NaN issue
        if (Double.isNaN(evennessX)) {
            evenness = 0.0;
        } else {
            evenness = (float) evennessX;
        }

        if (args.shannon) {
            args.minPerT = (float) (1 - evennessX);
            logger.info("Using estimated cutoff from evenness " + args.minPerT);
        } else {
            logger.info("Using user's defined cutoff " + args.minPerT);
        }

        /**
         * END HERE
          */

        int totalAcceptedReads = 0;
        double dt = 0.01 * args.minPerT;

        // sort and get the unique sequences.
        // samples with high diversity will have lower number included/number of
        // removed sequences then low diversity samples
        // otu.readCount == lastReadCount prevent any order dependency
        int lastReadCount = 0;

        // add or reject each OTU based on the read count (otus / rejectedOtus)
        for (SampleASV otu : sortedOTU) {
            if ((otu.readCount > 1) && ((otu.readCount == lastReadCount) || ((double) otu.readCount / (double) (totalAcceptedReads + otu.readCount) > dt))) {
                lastReadCount = otu.readCount;
                totalAcceptedReads += otu.readCount;
                otus.add(otu);
            } else {
                rejectedOtus.add(otu);
            }
        }

        numAcceptedReadsBeforeChimera = totalAcceptedReads;
        acceptedReads = totalAcceptedReads;
        percentAcceptedReadsBeforeChimera = ((double) totalAcceptedReads) / ((double) this.totalCounts);
        if (Double.isNaN(percentAcceptedReadsBeforeChimera))
            percentAcceptedReadsBeforeChimera = 0.0;
        int count = 0;
        for (SampleASV otu : sortedOTU) {
            otu.ratio = ((double) otu.readCount) / ((double) totalAcceptedReads);
            otu.id = count++;
        }
        numRejectedOtu = rejectedOtus.size();
        numAcceptedOtuBeforeChimera = otus.size();
        //free mem
        this.uniqSeqCount = null;

        logger.info("Accepted " + otus.size() + " otus");
        logger.info("Rejected " + rejectedOtus.size() + " otus");
    }

    /**
     * Filtered out chimera sequences using Levenshtein distances
     *
     * @param args -- commandline arguments
     */
    void filterChimera(CommandOptionsNGTax args) {
        // Now we do the chimera checking an filtering
        logger.info("Checking for chimeras");
        int chimeraNum = 0;
        // TODO couple to parameters
        for (int i = otus.size() - 1; i >= 0; i--)  // start from the last otus, moving to the beginning of the list
        {
            SampleASV checkMe = otus.get(i);
            SampleASV foundForward = null;
            SampleASV foundReverse = null;
            for (int j = 0; j < i; j++)  // start from the beginning
            {
                SampleASV checkAgainst = otus.get(j);
                if (((double) checkAgainst.readCount) / ((double) checkMe.readCount) < args.chimeraRatio)
                    continue;

                // Calculate Levenshtein distance
                if (StringUtils.getLevenshteinDistance(checkMe.forwardSequence, checkAgainst.forwardSequence, args.maxChemeraDistF) != -1) {
                    if (foundForward == null)
                        foundForward = checkAgainst;
                }
                if (StringUtils.getLevenshteinDistance(checkMe.reverseSequence, checkAgainst.reverseSequence, args.maxChemeraDistR) != -1) {
                    if (foundReverse == null)
                        foundReverse = checkAgainst;
                }
            }

            // If chimera were found, add it to the rejectedAsChimera
            if (foundForward != null && foundReverse != null) {
                rejectedAsChimera.add(new ChimeraRejection(checkMe, foundForward.id, foundForward.ratio, foundReverse.id, foundReverse.ratio));
                chimeraNum++;
            }
        }
        //After selecting the chimera remove them from the otu list
        for (ChimeraRejection item : rejectedAsChimera) {
            item.otu.clusteredReadCount = item.otu.readCount;
            this.otus.remove(item.otu);
            acceptedReads -= item.otu.readCount;
            numReadsChimera += item.otu.readCount;
        }
        logger.info("Found " + chimeraNum + " chimeras..");
    }

    /**
     * re-clustering the rejected reads
     *
     * @param args -- commandline arguments
     * @throws Exception
     */
    void errorCorrect(CommandOptionsNGTax args) throws Exception {
        // Now we calculate the actual ratio count, which include read mapping to
        // any of the removed otu's
        // For non diverse sample this has marginal impact

        // set after clustering read count intially to its own readcount
        // TODO should actually be devide over all that are x distance away

        // Identification of parent OTUs for reads that are below 0.X% abundance
        logger.info("Identification of parent OTUs for reads that are below " + args.minPerT + "% abundance for sample: " + sampleName);
        Levenshtein forwardForOtu[] = new Levenshtein[otus.size()];
        Levenshtein reverseForOtu[] = new Levenshtein[otus.size()];

        int count = 0;

        // Go through all otu in the SampleOTU and add it to the Levenshtein matrix.
        for (SampleASV otu : otus) {
            forwardForOtu[count] = new Levenshtein(otu.forwardSequence.getBytes(), args.maxClusteringMismatchCount);
            reverseForOtu[count] = new Levenshtein(otu.reverseSequence.getBytes(), args.maxClusteringMismatchCount);
            count++;
            // TODO CHECK
            otu.clusteredReadCount = otu.readCount;
        }

        SampleASV rejected[] = rejectedOtus.toArray(new SampleASV[0]);
        Arrays.sort(rejected, new Comparator<SampleASV>() {
            @Override
            public int compare(SampleASV o1, SampleASV o2) {
                return o1.forwardSequence.compareTo(o2.forwardSequence);
            }
        });

        for (SampleASV rejectedOtu : rejected) {
            SampleASV chooseFrom[] = new SampleASV[args.maxClusteringMismatchCount];
            int levelsFilled = 0;
            for (int i = 0; i < otus.size(); i++) {
                int res = forwardForOtu[i].getLevenshteinDistance(rejectedOtu.forwardSequence);
                if (res == -1)
                    continue;

                int res2 = reverseForOtu[i].getLevenshteinDistance(rejectedOtu.reverseSequence);
                if (res2 == -1)
                    continue;

                if (res + res2 == 0)
                    throw new Exception("impossible... ASV should always be unique");

                if (res + res2 <= args.maxClusteringMismatchCount) {
                    if (chooseFrom[res + res2 - 1] == null) {
                        chooseFrom[res + res2 - 1] = this.otus.get(i);
                        levelsFilled++;
                    }
                    if (levelsFilled == args.maxClusteringMismatchCount)
                        break;
                }
            }

            for (SampleASV take : chooseFrom) {
                if (take != null) {
                    // TODO check
                    take.clusteredReadCount += rejectedOtu.readCount;
                    acceptedReads += rejectedOtu.readCount;
                    break;
                }
            }
        }
        numAcceptedReadsAfterErrorCorrection = acceptedReads;
        percentAcceptedReadsAfterErrorCorrection = ((double) numAcceptedReadsAfterErrorCorrection) / ((double) this.totalCounts);
        if (Double.isNaN(percentAcceptedReadsAfterErrorCorrection)){
            percentAcceptedReadsAfterErrorCorrection = 0.0;
        }
        logger.info("Finished clustering of rejected reads for sample: " + sampleName);
    }

    public void cleanRejectedOtu(CommandOptionsNGTax args) {
        logger.info("Keeping rejected OTUs for further consideration, ones that the readcounts are above the threshold: -OTUSizeT, -minimumOTUSize " + args.minOTUsizeT);
        LinkedList<SampleASV> rejectedAgain = new LinkedList<>();
        for (SampleASV reject : rejectedOtus){
            if (reject.readCount < args.minOTUsizeT) {
                rejectedAgain.add(reject);
            }
        }
        rejectedOtus.removeAll(rejectedAgain);
        logger.info("Total number of rejected OTUs remain: " + rejectedOtus.size());
    }
}
