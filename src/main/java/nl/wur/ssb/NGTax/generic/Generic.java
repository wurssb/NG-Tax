package nl.wur.ssb.NGTax.generic;

import nl.wur.ssb.NGTax.App;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Generic {

    static final Logger logger = LogManager.getLogger(App.class);

    /**
     * Extract information out of the mapping file
     * Keep it in Hashmap
     * key: [barcode_f, barcode_r]
     * value: [Sample name, library]
     *
     * @param mappingFile
     * @return Hashmap contains information in the mapping file.
     * @throws Exception
     */
    public static HashMap getMappingInfo(String mappingFile) throws Exception {

        logger.info("Retrieving information from the mapping file.");

        HashMap<Integer, List<HashMap<List<String>, String>>> mappingInfo = new HashMap();

        int hasReverseBarCodes = 0;

        // To check if sampleID already exists.
        HashSet<String> checkSampleID = new HashSet<>();
        // To check if barcode already exists.
        HashSet<String> checkBarcode = new HashSet<>();
        List<String> lines = Files.readAllLines(Paths.get(mappingFile));

        ArrayList<String> header = new ArrayList<>(List.of(lines.get(0).toLowerCase().trim().replaceFirst("^#","").split("\t")));

        // Check if all headers that are needed are present
        if (header.containsAll(List.of(new String[]{"sampleid", "forwardbarcodesequence", "reversebarcodesequence", "librarynumber"}))) {
            // Pass
        } else if (header.containsAll(List.of(new String[]{"sampleid", "barcodesequence", "librarynumber"}))) {
            // Pass
        } else {
            logger.error("Header contains " + StringUtils.join(header, "; "));
            throw new Exception("Missing mapping headers e.g. SampleID, forwardBarcodeSequence, reverseBarcodeSequence, LibraryNumber or sampleID, BarcodeSequence, LibraryNumber");
        }

        for (String line : lines) {//new String(Files.readAllBytes(Paths.get(mappingFile))).replaceAll("\r\n", "\n").replaceAll("\r", "\n").split("\n")) {
            if (line.startsWith("#")) continue;

            String fBarcode = "XXX";
            String rBarcode = "XXX";

            if (line.trim().length() < 1)
                continue;

            String[] parts = line.split("\t"); // parts = [Sample.talarrubias, GAACGTAT, GAACGTAT, 1, p, ...]

            String sampleID = parts[header.indexOf("sampleid")];

            if (header.contains("forwardbarcodesequence")) {
                fBarcode = parts[header.indexOf("forwardbarcodesequence")];
                rBarcode = parts[header.indexOf("reversebarcodesequence")];
            }

            if (header.contains("barcodesequence")) {
                fBarcode = parts[header.indexOf("barcodesequence")];
                rBarcode = parts[header.indexOf("barcodesequence")];
            }

            // Check if the text in barcode column is a sequence consists of ATCG.
            if (!isATCG(fBarcode))
                throw new Exception("Mapping file column 2 should contain forward barcode sequence: " + fBarcode);

            // Check content in mapping file for a reverse barcode, parseInt return -1 if it is a string of characters
            // 1 to classify that the reverse barcode is not in the mapping file
            // 2 to classify that the reverse barcode is in the mappping file.

            // Setting reverse barcode and library number.
            int libraryNum = Integer.parseInt(parts[header.indexOf("librarynumber")]);

            // Check if the the sampleID is unique.
            // If it is in the hashset, it has been used before which is not allowed.
            if (checkSampleID.contains(sampleID))
                throw new Exception("Sample is not unique");

            // Add files to 'check' hashset
            checkSampleID.add(sampleID);

            List<String> barcode = new ArrayList<>();
            barcode.add(fBarcode);
            barcode.add(rBarcode);

            /*
                Check if the library already exists:
                if not, add library and barcode to the mappingInfo parameters
                if yes, check if the barcode is unique
            */

            if (!(mappingInfo.containsKey(libraryNum))) {
                checkBarcode.clear();
                mappingInfo.put(libraryNum, new ArrayList<>());
                mappingInfo.get(libraryNum).add(new HashMap<>() {{
                    put(barcode, sampleID);
                }});
            } else {
                // Check if the barcode is unique.
                // If it is in the hashset, it has been used before which is not allowed.
                if (checkBarcode.contains(barcode.get(0)) || checkBarcode.contains(barcode.get(1)))
                    throw new Exception("Barcode is not unique");
                // Add files to 'check' hashset
                checkBarcode.add(barcode.get(0));
                checkBarcode.add(barcode.get(1));

                mappingInfo.get(libraryNum).add(new HashMap<>() {{
                    put(barcode, sampleID);
                }});
            }
        }
        return mappingInfo;
    }

    /**
     * Check if the barcode only contain ATCG
     *
     * @param test
     * @return
     */
    private static boolean isATCG(String test) {
        return test.matches("^[ATCG]*$");
    }

    /**
     * Check if the given input is a string or integer
     *
     * @param in
     * @return
     */
    private static int parseInt(String in) {
        int toRet = -1;
        try {
            toRet = Integer.parseInt(in);
        } catch (Throwable th) {
        }
        return toRet;
    }

    /**
     * Replace primer character based on IUPAC standard.
     *
     * @param primer
     * @return
     */
    private static String fixPrimer(String primer) {
        return primer.replaceAll("B", "[CGT]")
                .replaceAll("D", "[AGT]")
                .replaceAll("H", "[ACT]")
                .replaceAll("K", "[GT]")
                .replaceAll("M", "[AC]")
                .replaceAll("N", "[ACGT]")
                .replaceAll("R", "[AG]")
                .replaceAll("S", "[GC]")
                .replaceAll("V", "[ACG]")
                .replaceAll("W", "[AT]")
                .replaceAll("Y", "[CT]");
    }
}
