package nl.wur.ssb.NGTax;

import life.gbol.domain.*;
import life.gbol.domain.Taxon;
import nl.wur.ssb.NGTax.CommandOptions.CommandOptionsNGTax;
import nl.wur.ssb.NGTax.CommandOptions.CommandOptionsRef2RDF;
import nl.wur.ssb.RDFSimpleCon.RDFFormat;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

class Database {
    /**
     * Creates a amplicon lookup database from a simple fasta file using the primers provided
     *
     * @param options from NGTax
     * @throws IOException when the database is not found
     */
    static final Logger logger = LogManager.getLogger(Database.class);

    static void creation(CommandOptionsNGTax options) throws Exception {

        // Make list of degenerative primers
        ArrayList<Pattern> forwardPrimers = createDegeneratePrimers(options.forwardPrimer, options.forwardReadLength, options.nomismatch);
        ArrayList<Pattern> reversePrimers = null;
        if (options.reversePrimer != "") {
            reversePrimers = createDegeneratePrimers(options.reversePrimer, options.reverseReadLength, options.nomismatch);
        }

        HashMap<String, Integer> database = new HashMap<>();
        // total number of hits
        long numberOfHits = 0;
        // Forward and reverse the database sequence
        SilvaDBScanner dbScanner = new SilvaDBScanner(new File(options.refdb));
        int count = 0;
        int fwdCount = 0;
        int revCount = 0;
        while (dbScanner.hasNext()) {
            count = count + 1;

            if (count % 10000 == 0 && options.debug && numberOfHits == 0) {
                File cachedFile = new File(options.refdb + "_" + options.forwardPrimer + "_" + options.reversePrimer
                        + "_" + options.forwardReadLength + "_" + options.reverseReadLength + "_nomismatch_" + options.nomismatch + "_" + ClassifyASV.dbVersion + "_full.gz.lock");
                while (cachedFile.exists()) {
                    cachedFile.delete();
                }
                throw new Exception("This primer pair does not give any hits with the first 10.000 entries in the database");
            }

            if (count % 100000 == 0) {
                logger.info("Parsed " + count + " sequences and matched " + numberOfHits + " " + fwdCount + " " + revCount  + " entries resulting in a merged database of " + database.size() + " entries");
            }
            boolean fwdHit = false;
            boolean revHit = false;

            String line = "";
            dbScanner.next();

            String fwdSequence = dbScanner.getSequence();
            String revSequence = ClassifyASV.revComplementRNA(fwdSequence);

            for (Pattern primerPattern : forwardPrimers) {// A U T C G?
                Matcher m = primerPattern.matcher(fwdSequence);
                boolean fwdMatch = m.find();
                if (!fwdMatch) {
                    m = primerPattern.matcher(revSequence);
                    fwdMatch = m.find();
                }
                if (fwdMatch) {
                    fwdHit = true;
                    fwdCount = fwdCount + 1;
                    String sequence = m.group().replaceAll("U", "T");
                    line += sequence + "\t";
                    break;
                }
            }
            // Perform pair primer matching
            if (fwdHit && reversePrimers != null) {
                for (Pattern primerPattern : reversePrimers) {// A U T C G?
                    Matcher m = primerPattern.matcher(fwdSequence);
                    boolean revMatch = m.find();
                    if (!revMatch) {
                        m = primerPattern.matcher(revSequence);
                        revMatch = m.find();
                    }

                    if (revMatch) {
                        revHit = true;
                        revCount = revCount + 1;
                        String sequence = m.group().replaceAll("U", "T");
//                        sequence = sequence.substring(sequence.length() - options.reverseReadLength);
                        line += sequence + "\t";
                        break;
                    }
                }
            } else {
                String sequence = new String(new char[options.forwardReadLength + options.forwardPrimerLength]).replace("\0", "X");
                line += sequence + "\t";
                revHit = true;
            }

            // Generates a count table from fwd / rev / lineage
            if (fwdHit && revHit) {
                line += StringUtils.join(dbScanner.getTaxa(), ";");
                // Forward / Reverse primer added
                if (!database.containsKey(line))
                    database.put(line, 0);
                database.put(line, database.get(line) + 1);
                numberOfHits = numberOfHits + 1;
            }
        }
        File outputdb = new File(options.refdb + "_" + options.forwardPrimer + "_" + options.reversePrimer
                + "_" + options.forwardReadLength + "_" + options.reverseReadLength + "_nomismatch_" + options.nomismatch + "_"+ ClassifyASV.dbVersion+"_full.gz");
        PrintWriter output = new PrintWriter(new OutputStreamWriter(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(outputdb)))));

        for (String key : database.keySet()) {
            output.println(key + "\t" + database.get(key));
        }
        output.close();
    }

    public static ArrayList<Pattern> createDegeneratePrimers(String primer, int regionLength, boolean nomismatch) {
        primer = primer.replaceAll("U", "T");

        ArrayList<Integer> degenerativePositions = new ArrayList<>();
        ArrayList<Pattern> degenerativePrimers = new ArrayList<>();
        degenerativePrimers.add(Pattern.compile(primer + new String(new char[regionLength]).replace("\0", ".")));

        if (nomismatch) {
            return degenerativePrimers;
        }

        // ATG[AT]ATGC
        // A.G[AT]ATGC
        // AT.[AT]ATGC
        // ATG.ATGC
        // ATG[AT].TGC
        // ATG[AT]A.GC
        // ATG[AT]AT.C



        String skip = "[]";
        boolean status = false;
        for (int i = 0; i < primer.length() - 1; i++) {
            char c = primer.charAt(i);
            if (skip.indexOf(c) > -1) {
                status = !status;
            }
            if (!status && skip.indexOf(c) == -1) {
                degenerativePositions.add(i);
            }
        }
        for (Integer position : degenerativePositions) {
            StringBuilder degPrimer = new StringBuilder(primer);
            degPrimer.setCharAt(position, '.');
            String regexPrimer = degPrimer.toString() + new String(new char[regionLength]).replace("\0", ".");

            degenerativePrimers.add(Pattern.compile(regexPrimer));
        }

        // Fix the [] positions
        // ATG[AT]ATGC[AT]GC
        Pattern pattern = Pattern.compile("\\[[A-Z]+\\]");
        Matcher matcher = pattern.matcher(primer);
        while (matcher.find()) {
            degenerativePrimers.add(Pattern.compile(primer.replace(matcher.group(), ".") + new String(new char[regionLength]).replace("\0", ".")));
        }
        return degenerativePrimers;
    }

    public static void toRDF(CommandOptionsRef2RDF argsRef2DB) throws Exception {
        LineIterator refdbIn = IOUtils.lineIterator(new GZIPInputStream(new BufferedInputStream(new FileInputStream(argsRef2DB.input))), "UTF-8");
        Domain domain = new Domain("");

        int fwdLength = CommandOptionsNGTax.makePrimerMask(argsRef2DB.fPrimer).length;
        int revLength = 0;
        if (argsRef2DB.rPrimer != null) {
            revLength = CommandOptionsNGTax.makePrimerMask(argsRef2DB.rPrimer).length;
        }

        int lines = 0;

        while(refdbIn.hasNext()) {
            lines = lines + 1;

            String line = refdbIn.next();
            String[] lineSplit = line.split("\t");
            String fwd = lineSplit[0];
            String rev = lineSplit[1];

            // Foward and reverse primer length removal
            fwd = fwd.substring(fwdLength);
            if (revLength > 0) {
                rev = rev.substring(revLength);
            }

            String tax = lineSplit[2];
            int abundance = Integer.parseInt(lineSplit[3]);

            ASVSet asvSet = domain.make(life.gbol.domain.ASVSet.class, "http://ssb.wur.nl/NG-Tax/0.1/reference/asvset/" + DigestUtils.sha384Hex(fwd + "_" + rev));
            asvSet.setMasterASVId(new File(argsRef2DB.input).getName());
            ASVSequence fASV = domain.make(ASVSequence.class, "http://ssb.wur.nl/NG-Tax/0.1/sequence/" + DigestUtils.sha384Hex(fwd));
            fASV.setSequence(fwd);
            fASV.setLength((long) fwd.length());
            fASV.setSha384(DigestUtils.sha384Hex(fwd));
            asvSet.setForwardASV(fASV);
            if (!rev.startsWith("X")) {
                ASVSequence rASV = domain.make(ASVSequence.class, "http://ssb.wur.nl/NG-Tax/0.1/sequence/" + DigestUtils.sha384Hex(rev));
                rASV.setSequence(rev);
                rASV.setLength((long) rev.length());
                rASV.setSha384(DigestUtils.sha384Hex(rev));
                asvSet.setReverseASV(rASV);
            }

            // Set taxonomy and abundance
            ASVAssignment asvAssignment = domain.make(life.gbol.domain.ASVAssignment.class,"http://ssb.wur.nl/NG-Tax/0.1/reference/assignment/" + tax);
            asvAssignment.setNumberHits(abundance);
            Taxon taxon = domain.make(life.gbol.domain.Taxon.class,"http://ssb.wur.nl/NG-Tax/0.1/reference/taxon/"  + tax);
            taxon.setTaxonName(tax);
            if (tax.split(";").length == 6)
                taxon.setTaxonRank(Rank.RankGenus);
            else
                throw new Exception("Implement other levels for ranking");

            asvAssignment.setTaxon(taxon);
            asvAssignment.setRatio(1f);
            asvAssignment.setType(TaxonAssignmentType.HitsTaxon);
            asvAssignment.setLevelCount(6);
            asvSet.addAsvAssignment(asvAssignment);
        }
        domain.save(argsRef2DB.output, RDFFormat.TURTLE);
    }
}
