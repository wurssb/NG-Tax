package nl.wur.ssb.NGTax;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Paths;

import static nl.wur.ssb.NGTax.ClassifyASV.loadrefdb;
import static nl.wur.ssb.NGTax.CommandOptions.CommandOptionsNGTax.fixPrimer;
import static nl.wur.ssb.NGTax.CommandOptions.CommandOptionsNGTax.makePrimerMask;

public class ReClassify {
    static final Logger logger = LogManager.getLogger(ReClassify.class);

    public void biomReClassify(File biomFile, String refdb, String outputBiomFile, String outputTTLFile) throws Exception {
        new NGTaxResult();
        String data = new String(java.nio.file.Files.readAllBytes(Paths.get(biomFile.getAbsolutePath())));
        NGTaxResult res = NGTaxResult.fromJSon(data);

        if (res == null) {
            throw new Exception("Input file is NOT in the biom json format (Check: http://biom-format.org).");
        }

        if (res.provData != null) {
            logger.info("NGTax biom format file detected.");
            ngTaxBiomReClassify(res, refdb, outputBiomFile, outputTTLFile);

//            if (res.provData.args.turtleFile != null) {
//                if(outputTTLFile != null) {
//                    ngTaxBiomReClassify(res, refdb, outputBiomFile, outputTTLFile);
//                }
//                else {
//                    logger.error("Turtle file location not given, please provide the file location.");
//                }
//            }
//            else {
//                ngTaxBiomReClassify(res, refdb, outputBiomFile, outputTTLFile);
//            }
        } else {
            logger.error("Traditional biom format file detected. Cannot be used for reclassification, use NG-Tax biom format.");
        }
    }

    public void ngTaxBiomReClassify(NGTaxResult res, String refdb, String outputBiomFile, String outputTTLFile) throws Exception {
        App.arguments = res.provData.args;

        if (App.arguments.debug) {
            LogManager.getLogger().atLevel(Level.DEBUG);
        }

        App.arguments.refdb = refdb;

        if (!new File(App.arguments.refdb).exists()) {
            throw new FileNotFoundException("Reference database file not found " + refdb);
        }

        App.arguments.biomFile = outputBiomFile;
        App.arguments.turtleFile = outputTTLFile;

        App.arguments.forwardPrimerMask = makePrimerMask(App.arguments.forwardPrimer);

        if (!App.arguments.singleEnd) {
            App.arguments.reversePrimer = fixPrimer(App.arguments.reversePrimer);
            App.arguments.reversePrimerLength = App.arguments.reversePrimer.replaceAll("\\[.*?\\]", "N").length();
            App.arguments.reversePrimerMask = makePrimerMask(App.arguments.reversePrimer);
        } else {
            App.arguments.reversePrimer = "";
            App.arguments.reversePrimerLength = App.arguments.forwardPrimerLength;
            App.arguments.reverseReadLength = App.arguments.forwardReadLength;
            App.arguments.reversePrimerMask = makePrimerMask(StringUtils.repeat("x", App.arguments.forwardPrimerLength));
        }

        // Load reference database
        loadrefdb();

        App.asvs = res.provData.asvList;

        NGTax ngTax = new NGTax(App.arguments);
        ngTax.samples = res.provData.samples;
        ngTax.libraries = res.provData.libraries;
        ngTax.asvList = res.provData.asvList;

        try {
            loadrefdb();
        } catch (Exception e) {
            logger.info(e);
        }

        logger.info("Starting reclassification");

        if (App.arguments.refdb != null) {
            ClassifyASV.classifyASVs();
        }

        logger.info("Creating biom file");
        ngTax.createBiomFile();
        logger.info("Biom file created");

    }

}
