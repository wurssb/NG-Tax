package nl.wur.ssb.NGTax;

import nl.wur.ssb.NGTax.CommandOptions.CommandOptionsNGTax;
import nl.wur.ssb.NGTax.CommandOptions.CommandOptionsSilvaTree;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.*;
import java.util.zip.GZIPInputStream;

import static nl.wur.ssb.NGTax.ClassifyASV.dbVersion;
import static nl.wur.ssb.NGTax.ClassifyASV.getIndexes;
import static nl.wur.ssb.NGTax.NGTax.getSeqForLength;

public class SilvaTree {
    static final Logger logger = LogManager.getLogger(SilvaTree.class);

    public static void main(CommandOptionsSilvaTree args) throws Exception {
        // Read the file and match against ASVs we have?
        Scanner scanner = new Scanner(args.asv);
        String fwdHeader = null;
        String revHeader = null;

        HashMap<String, String> asvLookup = new HashMap<>();
        HashMap<String, String> headerLookup = new HashMap<>();
        HashSet<String> headers = new HashSet<>();

        HashSet<String> asvSet = new HashSet<>();

        HashMap<String, HashSet<String>> alignment = new HashMap<>();

        logger.info("Parsing ASV input file");

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.startsWith(">")) {
                // Header
                headers.add(line.replace(">",""));
                String[] lineSplit = line.replace(">", "").split("_");
                fwdHeader = lineSplit[0];
                revHeader = lineSplit[1];
            } else {
                // Sequence
                String[] lineSplit = line.split("NNNN");
                String fwdSeq = lineSplit[0];
                String revSeq = lineSplit[1];
                // Asv to header
                asvLookup.put(fwdSeq, fwdHeader);
                asvLookup.put(revSeq, revHeader);
                // Header to asv
                headerLookup.put(fwdHeader, fwdSeq);
                headerLookup.put(revHeader, revSeq);
                // Add to set
                asvSet.add(fwdSeq);
                asvSet.add(revSeq);
            }
        }

        App.arguments = new CommandOptionsNGTax(new String[]{
                "-fS", "x.seq",
                "-b", "x.biom",
                "-for_p", args.forwardPrimer,
                "-rev_p", args.reversePrimer,
                "-for_read_len", String.valueOf(args.forwardReadLength),
                "-rev_read_len", String.valueOf(args.reverseReadLength),
                "-refdb", args.refdb
        });

        // Building the lookup database similar to the original database but now with alignment, alignment, taxonomy
        ClassifyASV.loadrefdb(true);

        // Parse the reference database line by line
        HashMap<Integer, Integer> fTmp = new HashMap<>();
        HashMap<Integer, Integer> rTmp = new HashMap<>();

        CommandOptionsNGTax commandOptionsNGTax = App.arguments;

        List<Integer> integerList = getIndexes(commandOptionsNGTax, new SilvaDBScanner(new File(args.refdb)), fTmp, rTmp);
        Integer forSeqHits = integerList.get(0);
        Integer revSeqHits = integerList.get(1);

        File cachedFile = new File(args.refdb + "_" + forSeqHits + "-" + args.forwardPrimerLength + "_" + revSeqHits + "-" + args.reversePrimerLength + "_"
                + args.forwardReadLength + "-" + args.reverseReadLength + "_" + dbVersion + ".gz");

        BufferedReader buffered = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(cachedFile)), "UTF-8"));

        logger.info("Parsing generated database");

        String content;
        while ((content = buffered.readLine()) != null) {
            String[] array = content.split("\t");
            String fwdAlignment = array[0];
            String revAlignment = array[1];
            String fwdPrimer = getSeqForLength(fwdAlignment, args.forwardPrimerLength);
            String fwdSilva = fwdAlignment.replace(fwdPrimer, "");
            String fwdASV = fwdSilva.replace("-","");

            String revPrimer = getSeqForLength(revAlignment, args.reversePrimerLength);
            String revSilva = revAlignment.replace(revPrimer, "");
            String revASV = revSilva.replace("-","");
            if (asvSet.contains(fwdASV)) {
                if (alignment.containsKey(fwdASV)) {
                    alignment.get(fwdASV).add(fwdSilva);
                } else {
                    alignment.put(fwdASV, new HashSet<>(Arrays.asList(fwdSilva)));
                }
            }
            if (asvSet.contains(revASV)) {
                if (alignment.containsKey(revASV)) {
                    alignment.get(revASV).add(revSilva);
                } else {
                    alignment.put(revASV, new HashSet<>(Arrays.asList(revSilva)));
                }
            }
        }

        logger.info("Parsing headers");
        for (String header : headers) {
            fwdHeader = header.split("_")[0];
            revHeader = header.split("_")[1];

            if (alignment.containsKey(headerLookup.get(fwdHeader))) {
                String fwdAlignment = alignment.get(headerLookup.get(fwdHeader)).iterator().next();
                // System.err.println(fwdAlignment);
            } else {
                logger.info("Missing... forward ASV " + fwdHeader + " " + headerLookup.get(fwdHeader));
            }
            if (alignment.containsKey(headerLookup.get(revHeader))) {
                String revAlignment = alignment.get(headerLookup.get(revHeader)).iterator().next();
                // System.err.println(revAlignment);
            } else {
                logger.info("Missing... reverse ASV " + revHeader + " " + headerLookup.get(revHeader));
            }
        }

//        for (String key : alignment.keySet()) {
//            if (alignment.get(key).size() > 1) {
//                System.err.println(key);
//                for (String seq : alignment.get(key)) {
//                    System.err.println(seq.replaceAll("-","").length() + " " + seq);
//                }
//            }
//        }
    }
//
//        // Load input file as hashmap?
//
//        // Header -> Sequence
//        // Sequence -> alignment
//        HashMap<String, String> fwdHeaderHashMap = new HashMap<>();
//        HashMap<String, String> fwdSeqHashMap = new HashMap<>();
//
//        HashMap<String, String> revHeaderHashMap = new HashMap<>();
//        HashMap<String, String> revSeqHashMap = new HashMap<>();
//
//        // List of headers for writing to output files?
//        HashSet<String> headers = new HashSet<>();
//        // Sequence lookup
//        HashSet<String> sequences = new HashSet<>();
//
//        Scanner scanner = new Scanner(commandOptionsSilvaTree.asv);
//
//        String[] header = {"", ""};
//        while (scanner.hasNextLine()) {
//            String line = scanner.nextLine();
//            if (line.startsWith(">")) {
//                header = line.split("_");
//                headers.add(line);
//            } else {
//                String[] sequence = line.split("NNNN");
//                sequences.addAll(Arrays.asList(sequence.clone()));
//                fwdHeaderHashMap.put(header[0], sequence[0]);
//                revHeaderHashMap.put(header[1], sequence[1]);
//                fwdSeqHashMap.put(sequence[0], "");
//                revSeqHashMap.put(sequence[1], "");
//            }
//        }
//
//        System.err.println(fwdHeaderHashMap.size());
//        System.err.println(revHeaderHashMap.size());
//        System.err.println(headers.size());
//
////        File outputFile = createOutputFile(commandOptionsSilvaTree);
//        createOutputFileTEST2(commandOptionsSilvaTree);
////
////        if (1 == 1) return;
////
////        scanner = new Scanner(outputFile);
////        while (scanner.hasNextLine()) {
////            String line = scanner.nextLine();
////            String[] lineSplit = line.split("\t");
////
////        }
////
//        return;
//    }



//    private static void createOutputFileTEST2(CommandOptionsSilvaTree args) throws Exception {
//        logger.info("File does not exist, creating file from alignment..");
//        SilvaDBScanner refdbIn = new SilvaDBScanner(new File(args.refdb));
//
//        HashMap<Integer, Integer> fTmp = new HashMap<>();
//        HashMap<Integer, Integer> rTmp = new HashMap<>();
//
////        int forSeqHits = 0;
////        int revSeqHits = 0;
//        // TODO Expose if correct
//        int barCodeHit = 0;
//        int silvaTotalDbSize = 0;
//        int silvaPrimerAcceptedDbSize = 0;
//
//        Writer dbPS = null;
//        Writer dbPSFull = null;
//
//        // This is for alignment file
//        File cachedFile = new File(args.refdb + "_" + args.forwardPrimer + "_" + args.reversePrimer
//                + "_" + args.forwardReadLength + "_" + args.reverseReadLength + "_" + dbVersion + "_full.tree.gz");
//        File cachedFileFull = new File(args.refdb + "_" + args.forwardPrimer + "_" + args.reversePrimer
//                + "_" + args.forwardReadLength + "_" + args.reverseReadLength + "_" + dbVersion + "_full.tree.gz");
//        File lockFile = new File(args.refdb + "_" + args.forwardPrimer + "_" + args.reversePrimer
//                + "_" + args.forwardReadLength + "_" + args.reverseReadLength + "_" + dbVersion + ".gz.tree.lock");
//
//        // args
//        String[] ngtaxArgs = {
//                "-fS", "x.seq",
//                "-b", "x.biom",
//                "-for_p", args.forwardPrimer,
//                "-rev_p", args.reversePrimer
//        };
//
//        try {
////            RandomAccessFile randomAccessFile = new RandomAccessFile(
////                    new File(args.refdb + "_" + args.forwardPrimer + "_" + args.reversePrimer + "_"
////                            + args.forwardReadLength + "_" + args.reverseReadLength + "_" + dbVersion + ".tree.gz"), "rw");
//
//            // Function to obtain the index columns
//            List<Integer> integerList = getIndexes(new CommandOptionsNGTax(ngtaxArgs), refdbIn, fTmp, rTmp);
//            Integer forSeqHits = integerList.get(0);
//            Integer revSeqHits = integerList.get(1);
//
//            refdbIn.close();
//
//            // Primers found, creating sub database...
//            logger.info("The locations are: Forward: " + forSeqHits + " and Reverse: " + revSeqHits);
//            logger.info("Writing to file now..");
//
//            dbPS = new OutputStreamWriter(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(cachedFile))));
//            dbPSFull = new OutputStreamWriter(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(cachedFileFull))));
//
//            refdbIn = new SilvaDBScanner(new File(args.refdb));
//
//            int forwardReadLength = args.forwardReadLength + args.forwardPrimerLength;
//            int reverseReadLength = args.reverseReadLength + args.reversePrimerLength;
//            int counter= 0;
//            while (refdbIn.hasNext()) {
//                counter++;
//                if (counter == 20000) {
//                    dbPS.close();
//                    dbPSFull.close();
//                    break;
//                }
//
//                silvaTotalDbSize++;
//                String seq = refdbIn.next();
//                String taxa[] = refdbIn.getTaxa();
//                // String id = refdbIn.getId();
//                String lineage = String.join(";", taxa);
//
//                seq = seq.replace(".", "-");
//
//                String forwardFull = getSeqForLength(seq.substring(forSeqHits), forwardReadLength);
//                String reverseFull = getSeqRevLength(seq.substring(0, seq.length() - revSeqHits), reverseReadLength);
//                if (reverseFull != null && forwardFull != null) {
//                    if (forwardFull != null && reverseFull != null) {
//                        String forwardFinal = forwardFull.replace('U', 'T');
//                        String reverseFinal = revComplementRNA(reverseFull).replace('U', 'T');
//
//                        dbPS.write(forwardFinal + "\t" + reverseFinal.replace('U', 'T') + "\t" + lineage + "\n");
//                        // +
//                        // "\t"
//                        // +
//                        // id
//                        silvaPrimerAcceptedDbSize++;
////                        if (args.genFullDb) {
//                        String forwardFullFinal = forwardFull.replace('U', 'T');
//                        String reverseFullFinal = revComplementRNA(reverseFull).replace('U', 'T');
//                        dbPSFull.write(forwardFullFinal + "\t" + reverseFullFinal + "\t" + lineage + "\n");// +
//                        // "\t"
//                        // +
//                        // id
////                        }
//                    }
//                }
//            }
//            dbPS.close();
//            // Merge redundant sequences and taxonomic assignments TODO sort uniq -c...
//            InputStream fileStream = new FileInputStream(cachedFile);
//            InputStream gzipStream = new GZIPInputStream(fileStream);
//            Reader decoder = new InputStreamReader(gzipStream);
//            BufferedReader buffered = new BufferedReader(decoder);
//
//            String content;
//            LinkedHashMap<String, Integer> dbPSmerged = new LinkedHashMap<>();
//            while ((content = buffered.readLine()) != null) {
//                content = content.trim();
//                if (dbPSmerged.containsKey(content)) {
//                    dbPSmerged.put(content, dbPSmerged.get(content) + 1);
//                } else {
//                    dbPSmerged.put(content, 1);
//                }
//            }
//            // Write to file...
//
//
//            dbPS = new OutputStreamWriter(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(cachedFile))));
//
//            for (String key : dbPSmerged.keySet()) {
//                dbPS.write(key + "\t" + dbPSmerged.get(key) + "\n");
//            }
//            dbPS.close();
//
//            logger.info("Data written to file");
//
//            dbPSFull.close();
//
//            refdbIn.close();
////            randomAccessFile.close();
//
//        } catch (Throwable th) {
//            System.err.println(">" + th.getMessage());
//            System.err.println(">" + StringUtils.join(th.getStackTrace(), "\n"));
//            if (dbPS != null)
//                dbPS.close();
//            if (dbPSFull != null)
//                dbPSFull.close();
//            if (cachedFile.exists())
//                cachedFile.delete();
//            if (cachedFileFull.exists())
//                cachedFileFull.delete();
//        } finally {
//            if (lockFile.exists())
//                lockFile.delete();
//        }
//        if (lockFile.exists())
//            lockFile.delete();
//    }

//    private static File createOutputFileTEST(CommandOptionsSilvaTree commandOptionsSilvaTree) throws Exception {
//        // database
//        SilvaDBScanner refdbIn = new SilvaDBScanner(new File(commandOptionsSilvaTree.refdb));
//
//        // args
//        String[] args = {
//                "-fS","x.seq",
//                "-b","x.biom",
//                "-for_p", commandOptionsSilvaTree.forwardPrimer,
//                "-rev_p", commandOptionsSilvaTree.reversePrimer
//        };
//
//        // Get primer pos
//        CommandOptionsNGTax commandOptionsNGTax = new CommandOptionsNGTax(args);
//
//        HashMap<Integer, Integer> fTmp = new HashMap<>();
//        HashMap<Integer, Integer> rTmp = new HashMap<>();
//        List<Integer> integerList = getIndexes(commandOptionsNGTax, refdbIn, fTmp, rTmp);
////        integerList = integerList.stream().sorted().collect(Collectors.toList());
//
//        Integer forwardPrimerPos = integerList.get(0);
//        Integer reversePrimerPros = integerList.get(1);
//
//        char[] fwdChars = refdbIn.getSequence().substring(forwardPrimerPos).toCharArray();
//        int fwdCharCounter = 0;
//        int fwdIndex = 0;
//        for (int i = 0; i < fwdChars.length; i++) {
//            fwdIndex = i;
//            if (Character.isLetter(fwdChars[i])) {
//                // When finding the readlength + 1 char then stop
//                if (fwdCharCounter == commandOptionsSilvaTree.forwardReadLength) {
//                    break;
//                }
//                fwdCharCounter++;
//            }
//        }
//        // Parse alignment file
//        String line = "TGGTAGTCCTAGCCGTAAACGATGGACACTAGGTGTGGGGGGATGTACCTTCCGTGCCGCAGCTAACGCATTAAGTGTCCCGCCTGGGGAGTACGACCGC";
//        int count = 0;
//
//        while (refdbIn.hasNext()) {
//            refdbIn.next();
//            count++;
//            if (count < 18000) continue;
//            if (count % 10000 == 0) {
//                System.err.println(count);
//            }
//
//            String sequence = refdbIn.getSequence().replaceAll("[\\.-]","");
//            if (sequence.contains(line)) {
//                System.err.println(line);
//
//                // alignment
//                String alignmentString = refdbIn.getSequence().substring(forwardPrimerPos-10, forwardPrimerPos + fwdIndex + 10);
//                String sequenceString = alignmentString.replaceAll("-","");
//                System.err.println(sequenceString);
//                return null;
//            }
//        }
//        return null;
//    }

//    private static File createOutputFile(CommandOptionsSilvaTree commandOptionsSilvaTree) throws Exception {
//        // Create database file based on original code with the alignment
//        String[] args = {
//                "-fS","x.seq",
//                "-b","x.biom",
//                "-for_p", commandOptionsSilvaTree.forwardPrimer,
//                "-rev_p", commandOptionsSilvaTree.reversePrimer
//        };
//        // Reference lookup
//        HashSet<String> parsed = new HashSet<>();
//
//        CommandOptionsNGTax commandOptionsNGTax = new CommandOptionsNGTax(args);
//        SilvaDBScanner refdbIn = new SilvaDBScanner(new File(commandOptionsSilvaTree.refdb));
//
//        HashMap<Integer, Integer> fTmp = new HashMap<>();
//        HashMap<Integer, Integer> rTmp = new HashMap<>();
//        List<Integer> integerList = getIndexes(commandOptionsNGTax, refdbIn, fTmp, rTmp);
//        integerList = integerList.stream().sorted().collect(Collectors.toList());
//
//        Integer forwardPrimerPos = integerList.get(0);
//        Integer reversePrimerPros = integerList.get(1);
//
//        File refdbOutFile = new File(commandOptionsSilvaTree.refdb + "_" + forwardPrimerPos + "-" + reversePrimerPros + "_" + commandOptionsSilvaTree.forwardReadLength + "-" + commandOptionsSilvaTree.reverseReadLength + "_" + dbVersion + "_alignment_lookup.tsv");
//
////        if (refdbOutFile.exists()) return refdbOutFile;
//
//        PrintWriter refdbOut = new PrintWriter(refdbOutFile);
//        int entryCounter = 0;
//        while (refdbIn.hasNext()) {
//            entryCounter++;
//            if (entryCounter % 1000 == 0) {
//                System.err.println(entryCounter + " " + parsed.size());
//            }
//
//            if (entryCounter % 100000 == 0) {
//                refdbOut.close();
//                return refdbOutFile;
//            }
//
//            String line = refdbIn.next();
//
//            // FORWARD PART ....
//            char[] fwdChars = line.substring(forwardPrimerPos).toCharArray();
//            int fwdCharCounter = 0;
//            int fwdIndex = 0;
//            for (int i = 0; i < fwdChars.length; i++) {
//                fwdIndex = i;
//                if (Character.isLetter(fwdChars[i])) {
//                    // When finding the readlength + 1 char then stop
//                    if (fwdCharCounter == commandOptionsSilvaTree.forwardReadLength) {
//                        break;
//                    }
//                    fwdCharCounter++;
//                }
//            }
//
//            // alignment
//            String alignmentString = line.substring(forwardPrimerPos, forwardPrimerPos + fwdIndex);
//            String sequenceString = alignmentString.replaceAll("-","");
//
//            if (!parsed.contains(sequenceString)) {
//                parsed.add(sequenceString);
//                refdbOut.println(sequenceString + "\t" + alignmentString);
//            }
//
////            String temp = sequenceString.replaceAll(commandOptionsSilvaTree.forwardPrimer, "");
////            System.err.println(temp.length());
////            System.err.println(revSeqHashMap.keySet().toArray()[0] + "\t" + fwdSeqHashMap.keySet().toArray()[0] +"\t" +sequenceString);
////
////            if (sequences.contains(sequenceString)) {
////                System.err.println(fwdHeaderHashMap.containsKey(sequenceString) + "\t" + sequenceString + "\t" + alignmentString);
////                System.err.println(revHeaderHashMap.containsKey(sequenceString) + "\t" + sequenceString + "\t" + alignmentString);
////                break;
////            }
//        }
//        refdbOut.close();
//        return refdbOutFile;
//    }
}
