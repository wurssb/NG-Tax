package nl.wur.ssb.NGTax;

import com.google.gson.annotations.Expose;

import java.util.LinkedList;

public class Taxon {
    @Expose
    public String tax;
    @Expose
    public LinkedList<String> otus = new LinkedList<>();

    public Taxon(String tax) {
        this.tax = tax;
    }
}
