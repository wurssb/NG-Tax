package nl.wur.ssb.NGTax;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.HashingInputStream;
import com.google.common.io.Files;
import htsjdk.samtools.fastq.FastqReader;
import htsjdk.samtools.fastq.FastqRecord;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;

public class Generic {
    private static final Logger logger = LogManager.getLogger(Generic.class);

    public static HashMap<String, Long> getSequenceFileInfo(File sequenceFile) throws IOException {
        Reader reader;

        if (sequenceFile.getName().endsWith(".gz")) {
            // GZIP file...
            InputStream gzipStream = new GZIPInputStream(new FileInputStream(sequenceFile));
            reader = new InputStreamReader(gzipStream);
        } else if (sequenceFile.getName().endsWith("bz2")) {
            // BZ2 file
            InputStream gzipStream = new BZip2CompressorInputStream(new FileInputStream(sequenceFile));
            reader = new InputStreamReader(gzipStream);
        } else {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(sequenceFile));
            reader = new InputStreamReader(bufferedInputStream);
        }
        BufferedReader br = new BufferedReader(reader);
        FastqReader fastqReader = new FastqReader(sequenceFile.getAbsoluteFile(), br);

        long maxReadLength = 0;
        long bases = 0;
        long reads = 0;
        while (fastqReader.hasNext()) {
            reads = reads + 1;
            try {
                FastqRecord fastqRecord = fastqReader.next();
                bases = bases + fastqRecord.getReadLength();
                if (maxReadLength < fastqRecord.getReadLength()) {
                    maxReadLength = fastqRecord.getReadLength();
                }
            } catch (htsjdk.samtools.SAMException e) {
                String message = e.getMessage();
                logger.error(message);
                if (message.contains("Unexpected end of ZLIB")) {
                    throw new IOException(message);
                }
            }
        }
        HashMap<String, Long> localSequenceInfo = new HashMap<>();
        localSequenceInfo.put("maxReadLength", maxReadLength);
        localSequenceInfo.put("bases", bases);
        localSequenceInfo.put("reads", reads);
//        sequenceDataSet.setReadLength(maxReadLength);
//        sequenceDataSet.setBases(bases);
//        sequenceDataSet.setReads(reads);
        return localSequenceInfo;
    }

    public static boolean isGzipped(File f) {
        InputStream is = null;
        try {
            is = new FileInputStream(f);
            byte [] signature = new byte[2];
            int nread = is.read( signature ); //read the gzip signature
            return nread == 2 && signature[ 0 ] == (byte) 0x1f && signature[ 1 ] == (byte) 0x8b;
        } catch (IOException e) {
            App.logger.error(e);
            return false;
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    /**
     * Generates the checksum of the given type for the string content.
     *
     * @param checksumType type of checksum ("MD5" or "SHA384")
     * @return checksum string
     */
    public static String checksum(String forCheckSum, String checksumType) {

        try {
            MessageDigest md = MessageDigest.getInstance(checksumType); // "SHA-384"
            md.update(forCheckSum.getBytes());
            byte[] digest = md.digest();
            StringBuffer sb = new StringBuffer();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }

            return (sb.toString());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Generates the MD5 checksum of the file content.
     *
     * @param file         first file to read for checksum
     * @param checksumType type of checksum ("MD5" or "SHA384")
     * @return checksum string
     */
    public static String checksum(File file, HashFunction checksumType) throws IOException {
        HashCode hc = Files.hash(file, checksumType);
        return hc.toString();
    }

    public static String checksum(InputStream stream, HashFunction checksumType) throws IOException {
        HashingInputStream hashingInputStream = new HashingInputStream(checksumType, stream);
        return hashingInputStream.hash().toString();
    }
}
