package nl.wur.ssb.NGTax;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Create ASV object
 */

public class ASV implements Serializable {
    @Expose
    public String id;
    @Expose
    public String forwardSequence;
    @Expose
    public String reverseSequence;
    @Expose
    public ASVMatchStats assignedTaxon;
    @Expose
    MismatchLevel usedMisMatchLevel;
    @Expose
    int usedTaxonLevel;
//    @Expose
    int misMatchLevelCounts[];

    MismatchLevel misMatchLevels[];

    public ASV(String id, String forwardSequence, String reverseSequence) {
        this.id = id;
        this.forwardSequence = forwardSequence;
        this.reverseSequence = reverseSequence;
    }
}
