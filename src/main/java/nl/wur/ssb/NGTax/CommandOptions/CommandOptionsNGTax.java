package nl.wur.ssb.NGTax.CommandOptions;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.google.common.collect.Lists;
import com.google.common.hash.Hashing;
import com.google.gson.annotations.Expose;
import nl.wur.ssb.NGTax.FileMetadata;
import nl.wur.ssb.NGTax.NoneParameterSplitter;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import static nl.wur.ssb.NGTax.Generic.checksum;
import static nl.wur.ssb.NGTax.Generic.getSequenceFileInfo;

@Parameters()
public class CommandOptionsNGTax {
    static final Logger logger = LogManager.getLogger(CommandOptionsNGTax.class);

    @Parameter(names = {"-mock3"}, description = "Name used for the internal mock3 dataset", hidden = true)
    public String mock3 = "REFMOCK3";

    @Parameter(names = {"-mock4"}, description = "Name used for the internal mock4 dataset", hidden = true)
    public String mock4 = "REFMOCK4";

    // Metadata to know what subfragment is being targeted
    @Parameter(names = {"-fragment", "-subfragment"}, description = "Subfragment identifier", hidden = true)
    public String subfragment;

    @Expose
    @Parameter(names = {"-ngtax"}, description = "Runs the NG-Tax pipeline", hidden = true)
    boolean ngtax = true;

    @Expose
    @Parameter(names = {"-fS", "-fastQ"}, description = "Either a set of fastQfiles separated by a space 'fastQF1,fastQR1 fastQF2,fastQR2'", variableArity = true, splitter = NoneParameterSplitter.class)
    public List<String> fastQSet; // or a zip file or a directory

    @Expose
    @Parameter(names = {"-folder"}, description = "Folder location of the fastQfiles, should be a clean folder with only the input (fastq) files")
    public String folder = null; //Use string to be able to write to the output file (biom)

    @Expose
    @Parameter(names = {"-for_p", "-sequence_forward"}, description = "Forward primer sequence (degenerate positions between brackets or use degenerate letters)", required = true)
    public String forwardPrimer;

    @Expose
    @Parameter(names = {"-rev_p", "-sequence_reverse"}, description = "Reverse primer sequence (degenerate positions between brackets or use degenerate letters)")
    public String reversePrimer;

    @Expose
    @Parameter(names = {"-refdb", "-referencedatabase"}, description = "The reference fasta file (Aligned or not aligned)")
    public String refdb; //Use string to be able to write to the output file (biom)

    // Hidden parameter for backwards compatability
    @Parameter(names = {"-16db", "-16database"}, description = "The reference fasta file (Aligned or not aligned)", hidden = true)
    String refdbBackup; //Use string to be able to write to the output file (biom)

    @Expose
    @Parameter(names = {"-nomismatch"}, description = "Primers are not allowed to have a mismatch when a database is created using a simple FASTA file")
    public boolean nomismatch; //Use string to be able to write to the output file (biom)

    @Expose
    @Parameter(names = {"-log"}, description = "Location of the log file")
    public String log = "run.log";

    @Expose
    @Parameter(names = {"-maxCount"}, description = "The maximum number of hits needed with the input database to build the lookup database")
    public int maxCount = 1000;

    @Expose
    @Parameter(names = {"-for_read_len"}, description = "Select the preferred read length for the forward sequence used in the analysis")
    public int forwardReadLength = 70;

    @Expose
    @Parameter(names = {"-rev_read_len"}, description = "Select the preferred read length for the reverse sequence used in the analysis")
    public int reverseReadLength = 70;

    @Expose
    @Parameter(names = {"-mapFile"}, description = "Mapping file containing metadata [txt]")
    public
    String mapFile; //Use string to be able to write to the output file (biom)

    @Expose
    @Parameter(names = {"-minPerT", "-minimumThreshold"}, description = "Minimum threshold detectable, expressed in percentage")
    public
    float minPerT = 0.1f;

    @Expose
    @Parameter(names = {"-shannon", "-shannonEvenness"}, description = "Minimum threshold based upon the Shannons equitability method (dynamically estimated assuming that each ASV represents a species)")
    public boolean shannon;

    @Expose
    @Parameter(names = {"-rja", "-rejectedASVAnnotation"}, description = "The number of rejected ASVs to be classified according to its abundance (-1 to classify everything)")
    public
    int rejectedASVAnnotation = 100;

    @Expose
    @Parameter(names = {"-rjl", "-rejectedASVLimit"}, description = "The number of rejected ASVs to be kept")
    public int rejectedASVLimit = 1000;

    @Expose
    @Parameter(names = {"-OTUSizeT", "-minimumOTUSize"}, description = "Minimum size of an ASV before minimum threshold filtering")
    public
    int minOTUsizeT = 3;

    @Expose
    @Parameter(names = {"-identLvl", "-identityLevel"}, description = "identity level between parents and chimera (recommended 100, no error allowed, chimera as perfect combination of two otus)")
    public
    float identLvl = 100f;

    @Expose
    @Parameter(names = {"-errorCorr", "-errorCorrClusPer"}, description = "Number of mismatch(es) allowed for each ASV clusters (only one mismatch recommended, input: 1)")
    public
    // error correction clustering percentage
    int errorCorr = 1;

    @Expose
    @Parameter(names = {"-cR", "-chimeraRatio"}, description = "ratio otu_parent_abundance/otu_chimera_abundance (recommended 2, both otu parents must be two times more abundant than the otu chimera)")
    public
    float chimeraRatio = 2.0f;

    @Expose
    @Parameter(names = {"-clR", "-classifyRatio"}, description = "the minimum ratio that a taxon needs to be compared to others")
    public
    float classifyRatio = 0.8f; // TODO check for 1.0f

    @Expose
    @Parameter(names = {"-m", "-markIfMoreThen1"}, description = "Mark the classification with *~ if there are more then 1 possible spieces")
    public
    boolean markIfMoreThen1 = false;

    // Biom output file
    @Expose
    @Parameter(names = {"-b", "-biomfile"}, description = "BiomFile location", required = true)
    public String biomFile;

    // RDF turtle output file
    @Expose
    @Parameter(names = {"-t", "-ttl"}, description = "Generate a BIOM RDF file")
    public String turtleFile;

    // Galaxy settings
    @Expose
    @Parameter(names = {"-email"}, description = "User email (Galaxy Only)")
    public String email;

    @Expose
    @Parameter(names = {"-project"}, description = "Project description (Galaxy Only)")
    public String project;

    @Parameter(names = {"-fQF", "-fastQFiles"}, description = "Create fastQ files for each library")
    public boolean fastQfiles = false;

    @Parameter(names = {"-skipFiltering"}, description = "Skip filtering step reuse data already generated")
    public boolean skipFiltering = false;

    @Parameter(names = {"-prefixId"}, description = "Prefix for the id of the otu's")
    public String prefixId = null;

    @Parameter(names = {"-genFullDb"}, hidden = true)
    public boolean genFullDb = false;

    @Parameter(names = {"-includeHitIds"}, hidden = true)
    public boolean includeHitIds = false;

    @Expose
    @Parameter(names = {"-primersRemoved"}, description = "Are the primers already removed?")
    public boolean primersRemoved = false;

    @Parameter(names = {"-single"}, description = "Single end reads")
    public
    boolean singleEnd = false;

    @Expose
    public final String ngtaxversion = getVersion(CommandOptions.class)[0];

    @Expose
    public final String ngtaxcommit = getVersion(CommandOptions.class)[1];

    @Parameter(names = {"-debug"}, description = "Enabling debug mode")
    public boolean debug = false;

    @Parameter(names = {"-compressed"}, description = "Input files are compressed with gzip (gz)")
    boolean compressed = false;

    @Parameter(names = {"-fastaFileLocation"}, description = "output Fasta file location")
    public String fastaFileLocation;

    @Expose
    public int maxChemeraDistF;

    @Expose
    public int maxChemeraDistR;

    @Expose
    public int maxClusteringMismatchCount;

    @Expose
    public int identity97MismatchCount;

    @Expose
    public int identity95MismatchCount;

    @Expose
    public int identity92MismatchCount;

    @Expose
    public int identity90MismatchCount;

    @Expose
    public int identity85MismatchCount;

    @Expose
    public int forwardPrimerLength;

    @Expose
    public int reversePrimerLength;
    public byte[] forwardPrimerMask;
    public byte[] reversePrimerMask;
    public CommandOptionsSingle single;

    // Containing file metadata
    @Expose
    public HashMap<String, FileMetadata> fileMetadataHashMap = new HashMap<>();

    @Parameter(names = "--help", help = true)
    private Boolean help;

    @Expose
    @Parameter(names = {"-threads"}, description = "Number of threads", hidden = true)
    public int threads = 5;

    public CommandOptionsNGTax(String args[]) throws IOException {
        try {
            new JCommander(this, args);

            // Backwards compatibility fix...
            if (this.refdbBackup != null && this.refdb == null) {
                this.refdb = this.refdbBackup;
            }

            if ((this.folder == null) && (this.fastQSet == null)) {
                if (this.mock3 != null || this.mock4 != null) {
                    // no problem
                } else {
                    System.err.println(this.mock3 + " " + this.mock4);
                    throw new Exception("No input files detected, use -fS or -folder.");
                }

            } else if (this.folder != null) {
                if (this.folder != null && new File(this.folder).exists() && new File(this.folder).isDirectory()) {
                    // Obtain all files in the folder
                    List<File> allFiles = Arrays.asList(new File(this.folder).listFiles());
                    List<File> sortedFileName = new ArrayList<>();
                    // Remove hidden ones
                    for (File file : allFiles) {
                        if (!file.isHidden())
                            sortedFileName.add(file);
                    }

                    if (!this.singleEnd && sortedFileName.size() % 2 != 0)
                        throw new Exception("Odd number of files detected");

                    // Sort the files alphabetically
                    Collections.sort(sortedFileName);

                    // Obtain input file metadata
                    for (File file : sortedFileName) {
                        getFileMetadata(file);
                    }

                    if (this.mapFile == null) {
                        logger.warn("Mapping file not given, will create a new one: " + this.mapFile);
                        this.mapFile = File.createTempFile("mapping_tmp", ".txt").getName();
                    } else if (new File(this.mapFile).exists()) {
                        this.mapFile = File.createTempFile("mapping_tmp", ".txt").getName();
                        logger.warn("Mapping file already exists, will not overwrite and create a new one: " + this.mapFile);
                    }

                    logger.info("Creating mapping file...");
                    PrintWriter mapWriter = new PrintWriter(this.mapFile);

                    if (!this.singleEnd)
                        mapWriter.write("#sampleID\tBarcodeSequence\tLibraryNumber\tDirection\tfastQforward\tfastQreverse\n");
                    else
                        mapWriter.write("#sampleID\tBarcodeSequence\tLibraryNumber\tDirection\tfastQforward\n");

                    int sampleCount = 0;
                    fastQSet = new ArrayList<>();

                    // Work in progress
                    List<List<File>> pairs;
                    if (this.singleEnd) {
                        pairs = Lists.partition(sortedFileName, 1);
                    } else {
                        pairs = Lists.partition(sortedFileName, 2);
                    }

                    for (List<File> pair : pairs) {
                        sampleCount++;
                        if (this.singleEnd) {
                            fastQSet.add(pair.get(0).getAbsolutePath());
                            String line = "Sample" + sampleCount + "_" + pair.get(0).getName() + '\t' + "" + "\t" + sampleCount + "\t" + "p" + "\t" + pair.get(0);
                            mapWriter.write(line + "\n");
                        } else {
                            fastQSet.add(pair.get(0) + "," + pair.get(1));
                            String line = "Sample" + sampleCount + "_" + pair.get(0).getName() + '\t' + "" + "\t" + sampleCount + "\t" + "p" + "\t" + pair.get(0) + "\t" + pair.get(1);
                            mapWriter.write(line + "\n");
                        }
                    }
                    mapWriter.close();
                } else {
                    File foldertmp = new File(this.folder);
                    if (!foldertmp.exists())
                        throw new Exception("Folder does not exist: " + foldertmp.getAbsolutePath());
                    if (foldertmp.listFiles().length == 0)
                        throw new Exception("Empty folder: " + foldertmp.getAbsolutePath());
                }
            } else if (this.fastQSet.size() > 0) {
                for (String fastqSet : this.fastQSet) {
                    for (String fastqFile : fastqSet.split(",")) {
                        if (fastqFile.matches("^null$")) break;
                        getFileMetadata(new File(fastqFile));
                    }
                }
            }

            // Primer correction according to https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3174695/
            // If the last nucleotide is a G... then change to R
            if (this.forwardPrimer.endsWith("G")) {
                logger.info("Last nucleotide (G) of the forward primer has been replaced by R, see PMC3174695");
                this.forwardPrimer.replaceAll("G$", "R");
            }

            if (this.reversePrimer != null && this.reversePrimer.endsWith("G")) {
                logger.info("Last nucleotide (G) of the reverse primer has been replaced by R, see PMC3174695");
                this.reversePrimer.replaceAll("G$", "R");
            }

            if (this.help != null) {
                new JCommander(this).usage();
                System.exit(0);
            }
            calcInternalParameters();
        } catch (ParameterException pe) {
            System.err.println(pe.getMessage());
            new JCommander(this).usage();
            System.err.println("  * required parameter");
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private void getFileMetadata(File file) throws IOException {
        logger.info("Obtaining file metadata for " + file);
        FileMetadata fileMetadata = new FileMetadata();
        // Calculate SHA384 of file?, need consistent URIs for file objects
        fileMetadata.setSha256(checksum(file, Hashing.sha256()));
        fileMetadata.setFileType("FASTQ");
        fileMetadata.setMd5(checksum(file, Hashing.md5()));
        fileMetadata.setLocalFileName(file.getName());
        fileMetadata.setLocalFilePath(file.getAbsolutePath());
        HashMap<String, Long> sequenceFileInfo = getSequenceFileInfo(file);
        fileMetadata.setMaxReadLength(sequenceFileInfo.get("maxReadLength"));
        fileMetadata.setBases(sequenceFileInfo.get("bases"));
        fileMetadata.setReads(sequenceFileInfo.get("reads"));

        fileMetadataHashMap.put(file.getAbsolutePath(), fileMetadata);
    }

    public CommandOptionsNGTax(CommandOptionsSingle single) throws IOException {
        this.single = single;
    }

    public static String getMD5(String in) {
        MessageDigest m = null;
        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        m.reset();
        m.update(in.getBytes());
        byte[] digest = m.digest();
        BigInteger bigInt = new BigInteger(1, digest);
        return bigInt.toString(10);
    }

    public static byte[] makePrimerMask(String primer) throws ParameterException {
        primer = primer.replace('T', 'B').replace('G', 'D').replace('C', 'H');
        String res = "";
        boolean opened = false;
        int code = 'A' - 1;
        for ( int i = 0; i < primer.length(); i++ ) {
            char c = primer.charAt(i);
            if (c == '[') {
                if (opened)
                    throw new ParameterException("primer string is wrong: " + primer);
                opened = true;
            } else if (c == ']') {
                if (!opened)
                    throw new ParameterException("primer string is wrong: " + primer);
                opened = false;
                res = res + ((char) code);
                code = 'A' - 1;
            } else if (opened) {
                code = code | c;
            } else
                res = res + c;
        }
        byte toRet[] = res.getBytes();
        for ( int i = 0; i < toRet.length; i++ )
            toRet[i] = (byte) (toRet[i] & 0x0f);
        return toRet;
    }

    public void calcInternalParameters() throws ParameterException {
        //round down
        maxChemeraDistF = (int) (((double) this.forwardReadLength) * (1.0 - (((double) this.identLvl) / 100.0)));
        maxChemeraDistR = (int) (((double) this.reverseReadLength) * (1.0 - (((double) this.identLvl) / 100.0)));

        maxClusteringMismatchCount = errorCorr;
        identity97MismatchCount = (int) (((double) (this.forwardReadLength + this.reverseReadLength)) * 0.03);
        identity95MismatchCount = (int) (((double) (this.forwardReadLength + this.reverseReadLength)) * 0.05);
        identity92MismatchCount = (int) (((double) (this.forwardReadLength + this.reverseReadLength)) * 0.08);
        identity90MismatchCount = (int) (((double) (this.forwardReadLength + this.reverseReadLength)) * 0.1);
        identity85MismatchCount = (int) (((double) (this.forwardReadLength + this.reverseReadLength)) * 0.15);
        this.forwardPrimer = fixPrimer(this.forwardPrimer);
        this.forwardPrimerLength = this.forwardPrimer.replaceAll("\\[.*?\\]", "N").length();
        this.forwardPrimerMask = makePrimerMask(this.forwardPrimer);

        if (!this.singleEnd) {
            this.reversePrimer = fixPrimer(this.reversePrimer);
            this.reversePrimerLength = this.reversePrimer.replaceAll("\\[.*?\\]", "N").length();
            this.reversePrimerMask = makePrimerMask(this.reversePrimer);
        } else {
            this.reversePrimer = "";
            this.reversePrimerLength = this.forwardPrimerLength;
            this.reverseReadLength = this.forwardReadLength;
            this.reversePrimerMask = makePrimerMask(StringUtils.repeat("x",this.forwardPrimerLength));
        }

        if (this.prefixId == null)
            this.prefixId = getMD5(LocalDateTime.now().toString()).substring(0, 6);
        else {
            try {
                Integer.parseInt(this.prefixId);
            } catch (Throwable th) {
                logger.error("Warning id is not integer, this might not work with the Qiime scripts");
            }
        }
    }

    public static String fixPrimer(String primer) {
        return primer.replaceAll("B", "[CGT]")
                .replaceAll("D", "[AGT]")
                .replaceAll("H", "[ACT]")
                .replaceAll("K", "[GT]")
                .replaceAll("M", "[AC]")
                .replaceAll("N", "[ACGT]")
                .replaceAll("R", "[AG]")
                .replaceAll("S", "[GC]")
                .replaceAll("V", "[ACG]")
                .replaceAll("W", "[AT]")
                .replaceAll("Y", "[CT]");
    }

    /**
     * @param clazz of the main program to obtain the right manifest and version
     * @return The {version, and the commit hash} from git in that order
     * @throws MalformedURLException
     * @throws IOException
     */
    public static String[] getVersion(Class<?> clazz) throws MalformedURLException, IOException {
        String className = clazz.getSimpleName() + ".class";
        String classPath = clazz.getResource(className).toString();

        if (classPath.startsWith("jar")) {
            String manifestPath =
                    classPath.substring(0, classPath.lastIndexOf("!") + 1) + "/META-INF/MANIFEST.MF";
            Manifest manifest = new Manifest(new URL(manifestPath).openStream());
            Attributes attr = manifest.getMainAttributes();

            String ImplementationVersion = attr.getValue("Implementation-Version");
            String ImplementationSha = attr.getValue("Implementation-Sha");

            String[] versioning = {ImplementationVersion, ImplementationSha};
            return versioning;
        } else {
            String[] versioning = {"test-run", "test-run"};
            return versioning;
        }
    }
}