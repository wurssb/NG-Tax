package nl.wur.ssb.NGTax.CommandOptions;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.google.gson.annotations.Expose;

import java.io.File;

@Parameters()
public class CommandOptionsSilvaTree {
    public int forwardPrimerLength;
    public int reversePrimerLength;

    @Parameter(names = "--help", help = true)
    private Boolean help;

    @Parameter(names = {"-silvatree"}, description = "Generates a tree based on the silva alignment file")
    public boolean silvaTree;

    @Parameter(names = {"-refdb"}, description = "(Silva) aligned database fasta file", required = true)
    public String refdb;

    @Parameter(names = {"-asv"}, description = "asv fasta file", required = true)
    public File asv;

    @Parameter(names = {"-for_p", "-sequence_forward"}, description = "Forward primer sequence (degenerate positions between brackets or use degenerate letters)", required = true)
    public
    String forwardPrimer;

    @Parameter(names = {"-rev_p", "-sequence_reverse"}, description = "Reverse primer sequence (degenerate positions between brackets or use degenerate letters)")
    public
    String reversePrimer;

    @Parameter(names = {"-for_read_len"}, description = "Select the preferred read length for the forward sequence used in the analysis")
    public
    int forwardReadLength = 100;

    @Parameter(names = {"-rev_read_len"}, description = "Select the preferred read length for the reverse sequence used in the analysis")
    public
    int reverseReadLength = 100;

//    @Parameter(names = {"-f", "-fasta"}, description = "The input fasta file", required = true)
//    String input;
//    @Parameter(names = {"-o"}, description = "The output zip file to write to", required = true)
//    String output;
//    @Parameter(names = {"-t", "-createTree"}, description = "Include trees, created with clustalo")
//    boolean createTree = false;

    public CommandOptionsSilvaTree(String args[]) {
        try {
            new JCommander(this, args);
            if (this.help != null) {
                new JCommander(this).usage();
                System.exit(0);
            }
        } catch (ParameterException pe) {
            System.err.println(pe.getMessage());
            new JCommander(this).usage();
            System.err.println("  * required parameter");
            System.exit(1);
        }

        this.forwardPrimerLength = this.forwardPrimer.replaceAll("\\[.*?\\]", "N").length();
        this.reversePrimerLength = this.reversePrimer.replaceAll("\\[.*?\\]", "N").length();
    }

}
