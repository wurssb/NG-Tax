package nl.wur.ssb.NGTax.CommandOptions;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.google.gson.annotations.Expose;
import nl.wur.ssb.NGTax.NoneParameterSplitter;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.Arrays;

public class CommandOptionsRemultiplex {

    @Expose
    @Parameter(names = {"-remultiplex"}, hidden = true)
    Boolean remultiplex = true;

    @Expose
    @Parameter(names = {"-fS", "-fastQ"}, description = "fastQfiles which forward and reverse read in the same library is separated using comma ',' and each library is separated using space e.g. f1.fastq,r1.fastq f2.fastq,r2.fastq", variableArity = true, splitter = NoneParameterSplitter.class)
    String fastQSet;

    @Expose
    @Parameter(names = {"-folder"}, description = "Folder location of the fastQfiles")
    File folder;

    @Expose
    @Parameter(names = {"-for_p", "-sequence_forward"}, description = "Forward primer sequence (degenerate positions between brackets or use degenerate letters)", required = true)
    String forwardPrimer;

    @Expose
    @Parameter(names = {"-rev_p", "-sequence_reverse"}, description = "Reverse primer sequence (degenerate positions between brackets or use degenerate letters)", required = true)
    String reversePrimer;

    @Expose
    @Parameter(names = {"-mapFile"}, description = "Mapping file containing metadata [txt]")
    String mapFile;

    @Expose
    @Parameter(names = {"-primerRemoved"}, description = "Number of barcodes to be generate.")
    boolean primerRemoved = false;

    public CommandOptionsRemultiplex(String args[]) {
        try {
            new JCommander(this, args);

            if (this.folder != null && this.folder.exists() && this.folder.isDirectory()) {
                File[] sortedFileName = this.folder.listFiles();
                Arrays.sort(sortedFileName);

                fastQSet = "";
                for (int i=0; i<sortedFileName.length ; i++){
                    if (i%2 == 0) {
                        fastQSet += sortedFileName[i] + "," + sortedFileName[i+1] + " ";
                    }
                }
                fastQSet = fastQSet.trim();
            }

        } catch (ParameterException pe) {
            System.err.println(pe.getMessage());
            new JCommander(this).usage();
            System.err.println("  * required parameter");
            System.exit(0);
        }
    }

}
