package nl.wur.ssb.NGTax.CommandOptions;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.google.gson.annotations.Expose;

import java.util.List;

@Parameters()
public class CommandOptionsBiom2RDF {

    @Expose
    @Parameter(names = {"-biom2rdf"}, hidden = true)
    boolean biom2rdf = true;

    @Expose
    @Parameter(names = {"-input", "-i"}, description = "Directly convert existing BIOM file(s) into RDF format, space separated. EX. 'biom1 biom2 biom3", required = true)
    public
    List<String> biomFile;

    @Expose
    @Parameter(names = {"-o", "-output"}, description = "Output directory, also space separated. EX. 'out1 out2 out3", required = true)
    public
    List<String> outFile;

    public CommandOptionsBiom2RDF(String args[]) {
        try {
            new JCommander(this, args);
        } catch (ParameterException pe) {
            System.err.println(pe.getMessage());
            new JCommander(this).usage();
            System.err.println("  * required parameter");
            System.exit(0);
        }
    }
}

