package nl.wur.ssb.NGTax.CommandOptions;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.google.gson.annotations.Expose;

import java.io.IOException;

@Parameters()
public class CommandOptions {

    @Expose
    @Parameter(names = {"-silvatree"}, description = "Generates a tree based on the silva alignment file")
    public boolean silvaTree;

    @Expose
    @Parameter(names = {"-otu2fasta"}, description = "Conversion of ASV's from Biom file to FASTA")
    public
    boolean otu2fasta = false;

    @Expose
    @Parameter(names = {"-demultiplex"}, description = "Demultiplex data for submission")
    public
    boolean demultiplex = false;

    @Expose
    @Parameter(names = {"-remultiplex"}, description = "Remultiplex the data to conveniently analyse the demultiplexed samples.")
    boolean remultiplex = false;

    @Expose
    @Parameter(names = {"-ngtax"}, description = "Runs the NG-Tax pipeline")
    boolean ngtax = false;

    @Parameter(names = {"-mock3"}, description = "Name used for the internal mock3 dataset (e.g., REFMOCK3)", hidden = true)
    public String mock3;

    @Parameter(names = {"-mock4"}, description = "Name used for the internal mock4 dataset (e.g., REFMOCK4)", hidden = true)
    public String mock4;

    @Expose
    @Parameter(names = {"-biom2rdf"}, description = "Directly convert existing BIOM file(s) into RDF format, space separated. EX. 'biom1 biom2 biom3")
    public
    boolean biom2rdf = false;

    @Expose
    @Parameter(names = {"-reClassify"}, description = "ReClassify the existing NG-Tax's BIOM file using a new database.")
    public
    boolean reClassify = false;

    @Expose
    @Parameter(names = {"-db2rdf"}, description = "Convert reference database to an RDF reference database")
    public
    boolean db2rdf = false;

    @Expose
    @Parameter(names = {"-barcodeSplitting"}, description = "Raw data is split into individual fastq files based on the given barcode in the mapping file")
    public
    boolean barcodeSplitting = false;

    @Parameter(names = "-help", help = true)
    private Boolean help = false;

    @Expose
    @Parameter(names = {"-exportFasta"}, description = "Export fasta")
    public
    boolean exportFasta;

    @Expose
    @Parameter(names = {"-coverage"}, description = "Builds the reference database (when not available) and performs the coverage analysis")
    public boolean coverage;


    public CommandOptions(String args[]) throws IOException {
        JCommander jCommander = new JCommander(this);
        jCommander.setAcceptUnknownOptions(true);
        jCommander.parse(args);
        if (this.help) {
            new JCommander(this).usage();
            System.err.println("  * required parameter");
            System.exit(0);
        }
    }



}