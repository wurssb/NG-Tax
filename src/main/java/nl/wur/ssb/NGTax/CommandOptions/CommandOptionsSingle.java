package nl.wur.ssb.NGTax.CommandOptions;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.google.gson.annotations.Expose;

import java.io.File;
import java.util.Scanner;

/**
 * Assign commandline arguments to the corresponding variable
 * "-single" is used for analysing file with only one sample (no maaping file needed)
 */

@Parameters()
public class CommandOptionsSingle {
//    @Expose
//    @Parameter(names = {"-fQ", "-for_fastQ_file"}, description = "forward FASTQ file", required = true)
//    String forwardFastQFiles;
//    @Expose
//    @Parameter(names = {"-rQ", "-rev_fastQ_file"}, description = "reverse FASTQ file", required = true)
//    String reverseFastQFiles;
//    @Expose
//    @Parameter(names = {"-bF", "-barcodeFile"}, description = "barcode File")
//    String barcodeFile = null;
//    @Expose
//    @Parameter(names = {"-refdb_obsolete", "-referencedatabase"}, description = "the reference database")
//    String refdb_obsolete;
//    @Expose
//    @Parameter(names = {"-minPerT", "-minimumThreshold"}, description = "Minimum threshold detectable, expressed in percentage")
//    float minPerT = 0.1f;
//    @Expose
//    @Parameter(names = {"-taxTable"}, description = "Taxonomic table file")
//    String taxTable;
//    @Expose
//    @Parameter(names = {"-identLvl", "-identityLevel"}, description = "identity level between parents and chimera (recommended 100, no error allowed, chimera as perfect combination of two otus)")
//    float identLvl = 100f;
//    @Expose
//    @Parameter(names = {"-errorCorr", "-errorCorrClusPer"}, description = "error correction clustering percentage (only one mismatch recommended)")
//    int errorCorr = 1;
//    @Expose
//    @Parameter(names = {"-clR", "-classifyRatio"}, description = "the minimum ratio that a taxon needs to be compaired to others", required = true)
//    float classifyRatio = 0.5f;
//    @Expose
//    @Parameter(names = {"-cR", "-chimeraRatio"}, description = "ratio otu_parent_abundance/otu_chimera_abundance (recommended 2, both otu parents must be two times more abundant than the otu chimera)")
//    float chimeraRatio = 2.0f;
    // Biom output file
    @Expose
    @Parameter(names = {"-b", "-biomfile"}, description = "BiomFile location", required = true)
    String biomFile;
    // Galaxy settings
//    @Expose
//    @Parameter(names = {"-email"}, description = "User email (Galaxy Only)")
//    String email;
//    @Expose
//    @Parameter(names = {"-project"}, description = "Project description (Galaxy Only)")
//    String project;

    @Parameter(names = "--help", help = true)
    private Boolean help;

    public CommandOptionsSingle(String args[]) {
        try {
            new JCommander(this, args);
            if (this.help != null) {
                new JCommander(this).usage();
                System.exit(0);
            }
        } catch (ParameterException pe) {
            System.err.println(pe.getMessage());
            new JCommander(this).usage();
            System.err.println("  * required parameter");
            System.exit(1);
        }
    }
//
//    CommandOptionsNGTax createCommandOptions() throws Exception {
//        CommandOptionsNGTax commandOptions = new CommandOptionsNGTax(this);
//        Scanner tempFor = new Scanner(new File(this.forwardFastQFiles));
//        Scanner tempRev = new Scanner(new File(this.reverseFastQFiles));
//        tempFor.nextLine();
//        tempRev.nextLine();
//        commandOptions.forwardReadLength = tempFor.nextLine().length() - 1;
//        commandOptions.reverseReadLength = tempRev.nextLine().length() - 1;
//        tempFor.close();
//        tempRev.close();
//        commandOptions.refdb_obsolete = refdb_obsolete;
//        commandOptions.minPerT = minPerT;
////    commandOptions.taxTable = taxTable;
//        commandOptions.identLvl = identLvl;
//        commandOptions.errorCorr = errorCorr;
//        commandOptions.chimeraRatio = chimeraRatio;
//
//        // Biom output file
//        commandOptions.biomFile = biomFile;
//        // Galaxy settings
//        commandOptions.email = email;
//        commandOptions.project = project;
//        return commandOptions;
//    }

}
