package nl.wur.ssb.NGTax.CommandOptions;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.google.gson.annotations.Expose;

import java.io.File;

@Parameters()
public class CommandOptionsReClassify {

    @Expose
    @Parameter(names = {"-reClassify"}, hidden = true)
    public boolean reClassify = true;

    @Expose
    @Parameter(names = {"-input", "-i"}, description = "Directly reclassify the input BIOM file using the new database", required = true)
    public File biomFile;

    @Expose
    @Parameter(names = {"-refdb", "-referencedatabase"}, description = "Reference Database", required = true)
    public String refdb;

    @Expose
    @Parameter(names = {"-o", "-output"}, description = "Output file name for biom file", required = true)
    public String outBiomFile;

    @Expose
    @Parameter(names = {"-ttl", "-turtle"}, description = "Output file name for turtle file")
    public String outTTLFile;

    public CommandOptionsReClassify(String args[]) {
        try {
            new JCommander(this, args);
        } catch (ParameterException pe) {
            System.err.println(pe.getMessage());
            new JCommander(this).usage();
            System.err.println("  * required parameter");
            System.exit(0);
        }
    }
}

