package nl.wur.ssb.NGTax.CommandOptions;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.google.gson.annotations.Expose;

@Parameters()
public class CommandOptionsBiom2OTUfasta {

    @Expose
    @Parameter(names = {"-otu2fasta"}, hidden = true)
    boolean otu2fasta = true;

    @Expose
    @Parameter(names = {"-input", "-i"}, description = "Extract ASV sequences from the existing BIOM file and exported into fasta files", required = true)
    public
    String biomFile;

    @Expose
    @Parameter(names = {"-o", "-output"}, description = "Output directory", required = true)
    public
    String outFile;

    public CommandOptionsBiom2OTUfasta(String args[]) {
        try {
            new JCommander(this, args);
        } catch (ParameterException pe) {
            System.err.println(pe.getMessage());
            new JCommander(this).usage();
            System.err.println("  * required parameter");
            System.exit(0);
        }
    }
}

