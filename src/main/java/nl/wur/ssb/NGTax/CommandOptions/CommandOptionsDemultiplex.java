package nl.wur.ssb.NGTax.CommandOptions;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.google.gson.annotations.Expose;
import nl.wur.ssb.NGTax.NoneParameterSplitter;

import java.io.File;
import java.util.UUID;

@Parameters()
public class CommandOptionsDemultiplex {

    @Expose
    @Parameter(names = {"-demultiplex"}, hidden = true)
    public
    Boolean demultiplex = true;

    @Expose
    @Parameter(names = {"-o", "-output"}, description = "Output folder for demultiplexed files")
    public
    File output = new File("Demultiplex_fastq_" + UUID.randomUUID().toString() + "/");

    @Expose
    @Parameter(names = {"-fS", "-fastQ"}, description = "fastQfiles which forward and reverse read in the same library is separated using comma ',' and each library is separated using space e.g. f1.fastq,r1.fastq f2.fastq,r2.fastq", required = true, variableArity = true, splitter = NoneParameterSplitter.class)
    public
    String fastQSet;

    @Expose
    @Parameter(names = {"-for_p", "-sequence_forward"}, description = "Forward primer sequence (degenerate positions between brackets or use degenerate letters)", required = true)
    public
    String forwardPrimer;

    @Expose
    @Parameter(names = {"-rev_p", "-sequence_reverse"}, description = "Reverse primer sequence (degenerate positions between brackets or use degenerate letters)")
    public
    String reversePrimer;

    @Expose
    @Parameter(names = {"-mapFile"}, description = "Mapping file containing metadata [txt]", required = true)
    public
    String mapFile;

    @Expose
    @Parameter(names = {"-zip"}, description = "Zip the output folder")
    public
    File zip;

    @Expose
    @Parameter(names = {"-untrim"}, description = "Specify untrim option to only separate sample without removing barcode and primer.")
    public
    Boolean untrim = false;

    @Parameter(names = {"-debug"}, description = "Debug mode")
    public Boolean debug = false;

    public CommandOptionsDemultiplex(String args[]) {
        try {
            new JCommander(this, args);
        } catch (ParameterException pe) {
            System.err.println(pe.getMessage());
            new JCommander(this).usage();
            System.err.println("  * required parameter");
            System.exit(0);
        }
    }

}
