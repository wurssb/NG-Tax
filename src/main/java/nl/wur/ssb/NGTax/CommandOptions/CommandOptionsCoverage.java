package nl.wur.ssb.NGTax.CommandOptions;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.google.gson.annotations.Expose;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

@Parameters()
public class CommandOptionsCoverage {
    static final Logger logger = LogManager.getLogger(CommandOptionsCoverage.class);

    @Parameter(names = {"-refdb", "-referencedatabase"}, description = "The reference fasta file (Aligned or not aligned)")
    public String refdb; //Use string to be able to write to the output file (biom)

    @Parameter(names = {"-for_p", "-sequence_forward"}, description = "Forward primer sequence (degenerate positions between brackets or use degenerate letters)", required = true)
    public String forwardPrimer;

    @Parameter(names = {"-rev_p", "-sequence_reverse"}, description = "Reverse primer sequence (degenerate positions between brackets or use degenerate letters)")
    public String reversePrimer;

    @Parameter(names = {"-for_read_len"}, description = "Select the preferred read length for the forward sequence used in the analysis")
    public int forwardReadLength = 70;

    @Parameter(names = {"-rev_read_len"}, description = "Select the preferred read length for the reverse sequence used in the analysis")
    public int reverseReadLength = 70;

    @Parameter(names = {"-debug"}, description = "Enabling debug mode")
    public boolean debug = false;

    @Parameter(names = {"-reference"}, description = "Reference output landscape")
    public String reference = "reference.tsv";

    @Parameter(names = {"-coverage"}, description = "Coverage output landscape", required = true)
    public boolean coverage;


    @Parameter(names = "--help", help = true)
    private Boolean help;

    public CommandOptionsCoverage(String args[]) throws IOException {
        try {
            new JCommander(this, args);

            if (this.help != null) {
                new JCommander(this).usage();
                System.exit(0);
            }
        } catch (ParameterException pe) {
            System.err.println(pe.getMessage());
            new JCommander(this).usage();
            System.err.println("  * required parameter");
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}