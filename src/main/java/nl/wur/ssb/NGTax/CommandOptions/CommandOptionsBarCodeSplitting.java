package nl.wur.ssb.NGTax.CommandOptions;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.google.gson.annotations.Expose;

import java.io.File;
import java.util.List;

@Parameters()
public class CommandOptionsBarCodeSplitting {

    @Parameter(names = {"-barcodeSplitting"}, hidden = true)
    boolean barcodeSplit = true;

    @Parameter(names = {"-forward", "-fwd"})
    public
    File forward;

    @Parameter(names = {"-reverse", "-rev"})
    public
    File reverse;


    @Parameter(names = {"-mapFile"}, description = "Mapping file containing metadata [txt]")
    public
    String mapFile; //Use string to be able to write to the output file (biom)

    @Parameter(names = {"-o", "-output"}, description = "Output file for the barcode files (will append .fwd.fastq.gz / .rev.fastq.gz)", required = true)
    public
    File libraryFile;

    public CommandOptionsBarCodeSplitting(String args[]) {
        try {
            new JCommander(this, args);
        } catch (ParameterException pe) {
            System.err.println(pe.getMessage());
            new JCommander(this).usage();
            System.err.println("  * required parameter");
            System.exit(0);
        }
    }
}

