#!/bin/bash
#============================================================================
#title          :NG-Tax
#description    :NG-Tax, a highly accurate and validated pipeline for analysis of 16S rRNA amplicons from complex biomes
#authors        :Javier Ramiro-Garcia, Wiebe Kooistra, Jasper Koehorst, Jesse van Dam
#date           :2016
#version        :0.0.3
#============================================================================

planemo test --test_data .
